Milestones
==========

   1. 4 data structures (tracker, peer, source, monitor)
   2. protocol buffer description file(s)
   3. message processing between source & tracker
   4. message processing between peer & tracker
   5. message processing between peer & peer / source
        assumption regarding optimization: max of X children per peer
   6. message processing between monitor & tracker
   7. message processing between monitor & peer / source
   8. RTP stream
   9. data structures for local knowledge / local monitoring
  10. local optimizations
        => drop assumption regarding number of children
  11. monitor: kill / leave

