#!/bin/bash

echo '~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~'
echo '~ ~ LSO'
echo '~ ~ 2. Playground'
echo '~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~'
echo ''
echo ''

streams=(Playground)
stripes=(1)
video_rates=(100)
bandwidths=(100)
peers=(3)
peer_bandwidths=(100)
mode="OverlayOnly"

cd ../start/
source start.sh
