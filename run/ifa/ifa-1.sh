#!/bin/bash

echo '~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~'
echo '~ ~ LSO'
echo '~ ~ 1. Single Tree'
echo '~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~'
echo ''
echo ''

streams=(SingleTree)
stripes=(1)
video_rates=(100)
bandwidths=(200)
peers=(5)
peer_bandwidths=(200)
mode="OverlayOnly"

cd ../start/
source start.sh
