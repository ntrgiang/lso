#!/bin/bash

echo '~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~'
echo '~ ~ LSO'
echo '~ ~ 3. Multi Tree'
echo '~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~'
echo ''
echo ''

streams=(MultiTree)
stripes=(4)
video_rates=(100)
bandwidths=(300)
peers=(10)
peer_bandwidths=(200)
mode="OverlayOnly"

cd ../start/
source start.sh
