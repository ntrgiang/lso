#!/bin/bash

echo '~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~'
echo '~ ~ LSO / all scenarios'
echo '~ ~ 1. Single Tree'
echo '~ ~ 2. Playground'
echo '~ ~ 3. Multi Tree'
echo '~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~'
echo ''
echo ''

streams=(SingleTree Playground MultiTree)
stripes=(1 1 4)
video_rates=(100 100 100)
bandwidths=(200 100 300)
peers=(5 3 10)
peer_bandwidths=(200 100 200)
mode="OverlayOnly"

cd ../start/
source start.sh
