#!/bin/bash

# generates the different installations as described in README.txt

if [ -e "./installation" ]; then rm -r installation; fi

mkdir installation

types=("tracker" "monitor" "source" "peer")

for type in "${types[@]}"; do
	echo $type
	mkdir installation/${type}
	mkdir installation/${type}/build
	mkdir installation/${type}/run
	cp ../build/lso.jar installation/${type}/build/
	cp ../build/${type}.jar installation/${type}/build/
	cp ${type}.sh installation/${type}/run/
	cp addresses.ini installation/${type}/run/
	cp demo* installation/${type}/run/
	cp start.sh installation/${type}/run/
	cp stop.sh installation/${type}/run/
done

cp ../build/peer.jar installation/monitor/build/
cp peer.sh installation/monitor/run/

cp -r img installation/monitor/run/
cp config.cfg installation/monitor/run/


mkdir installation/complete
mkdir installation/complete/build
mkdir installation/complete/run

cp ../build/* installation/complete/build/
cp -r img installation/complete/run/
cp *.sh installation/complete/run/
cp config.cfg installation/complete/run/
cp addresses.ini installation/complete/run/
cp README.txt installation/complete/run/
cp lso.apk installation/