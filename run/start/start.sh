if [ "$#" -gt "0" ]; then
	if [ "$1" == "-h" ]; then exit; fi
	if [ "$1" == "--help" ]; then exit; fi
	type=$1
else
	type="all"
fi

echo "starting ${type}"
echo 
echo

source addresses.ini

if [[ $type == "all" || $type == "tracker" ]]; then
	./tracker.sh $tracker &
	sleep 2
fi


i=0
while [ "$i" -lt "${#streams[@]}" ]; do
	stream=${streams[$i]}
	stripe=${stripes[$i]}
	video_rate=${video_rates[$i]}
	bandwidth=${bandwidths[$i]}
	
	if [[ $type == "all" || $type == "source" ]]; then
		./source.sh $source_host $source_port_maintenance $source_port_data $tracker $stream $stripe $video_rate $bandwidth 5 $mode &
		sleep 2
	fi
	
	if [[ $type == "all" || $type == "peer" ]]; then
		j=0
		while [ "$j" -lt "${peers[$i]}" ]; do
			peer_bandwidth=${peer_bandwidths[$i]}
			./peer.sh $peer_host $peer_port_maintenance $peer_port_data $tracker $stream $peer_bandwidth 5 $mode &
			sleep 2
			let "j++"
		done
	fi
	
	let "i++"
done

if [[ $type == "all" || $type == "monitor" ]]; then
	./monitor.sh $monitor $tracker ${peer_bandwidths[0]}
	./stop.sh
fi
