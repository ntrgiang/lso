----------------------------------------------------------
-- Complete installation
----------------------------------------------------------

A complete installation contains:

- build
  - lso.jar
  - monitor.jar
  - peer.jar
  - source.jar
  - tracker.jar

- run
  - addresses.ini
  - config.cfg
  - demo{1..5}.sh
  - installation.sh
  - monitor.sh
  - peer.sh
  - source.sh
  - start.sh
  - tracker.sh

- run/img
  - android.png
  - linux.png
  - mac.png
  - phone.png
  - source.png
  - tablet.png
  - unix.png
  - windows.png


----------------------------------------------------------
-- Tracker installation
----------------------------------------------------------

A tracker installation contains:

- build
  - lso.jar
  - tracker.jar

- run
  - addresses.ini
  - demo{1..5}.sh
  - tracker.sh


----------------------------------------------------------
-- Source installation
----------------------------------------------------------

A source installation contains:

- build
  - lso.jar
  - source.jar

- run
  - addresses.ini
  - demo{1..5}.sh
  - source.sh


----------------------------------------------------------
-- Peer installation
----------------------------------------------------------

A peer installation contains:

- build
  - lso.jar
  - peer.jar

- run
  - addresses.ini
  - demo{1..5}.sh
  - peer.sh


----------------------------------------------------------
-- Monitor installation
----------------------------------------------------------

A monitor installation contains:

- build
  - lso.jar
  - monitor.jar

- run
  - addresses.ini
  - config.cfg
  - demo{1..5}.sh
  - monitor.sh

- run/img
  - android.png
  - linux.png
  - mac.png
  - phone.png
  - source.png
  - tablet.png
  - unix.png
  - windows.png


----------------------------------------------------------
-- Requirements
----------------------------------------------------------

- Java runtime environment 1.6
- Network connection between components


----------------------------------------------------------
-- Demos
----------------------------------------------------------

*** Demo 0
  * 1 source with 1 stripe
  * source bandwidth: 2
  * peer bandwidth: 2
  * 2 peers

*** Demo 1
  * 5 sources with 1-9 stripes
  * source bandwidth: 2
  * peer bandwidth: 2
  * 2-5 peers per source

*** Demo 2
  * 9 sources with 1-9 stripes
  * source bandwidth: 2
  * peer bandwidth: 2
  * 0 peers

*** Demo 3
  * 1 source with 5 stripes
  * source bandwidth: 4
  * peer bandwidth: 2
  * 5 peers

*** Demo 4
  * 1 source with 1 stripe
  * source bandwidth: 4
  * peer bandwidth: 2
  * 5 peers

*** Demo 5
  * 2 sources with 1-2 stripes
  * source bandwidth: 2
  * peer bandwidth: 2
  * 1-2 peers per source

*** Demo 6
  * 1 source with 1 stripe
  * source bandwidth: 2
  * peer bandwidth: 2
  * 2 peers

*** Demo 7
  * 1 source with 3 stripes
  * source bandwidth: 2
  * peer bandwidth: 2
  * 7 peers

This information is also displayed when starting a script.
In order to only display this information, run "./demo*.sh -h" or "./demo*.sh --help".


----------------------------------------------------------
-- Running a demo on a single machine
----------------------------------------------------------

- Perform ping tests to mobile devices
- Adapt IP addresses in addresses.ini
- Adapt tracker / peer IP addresses on Android client
- Start demo
  - cd run; ./demo{1..5}.sh
- Stop demo:
  - quit monitor (or Ctrl-c in terminal)


----------------------------------------------------------
-- Running a distributed demo
----------------------------------------------------------

- Perform ping test between all components
- Adapt IP addresses in addresses.ini
- Generate installations './installation.sh'
- Copy installation folders to respective machine
- Adapt tracker / peer IP addresses on Android client
- Start demo on each machine:
  - tracker: cd run; ./demo{1..5}.sh tracker
  - source: cd run; ./demo{1..5}.sh source
  - peer: cd run; ./demo{1..5}.sh peer
  - monitor: cd run; ./demo{1..5}.sh monitor
- Stop demo
  - monitor: quit application or Ctrl-c in cmd-line
  - tracker, source, peer: ./stop.sh