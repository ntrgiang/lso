Live Streaming Overlay (lso)
============================

An overlay for the distribution of live video streams.

Installation
------------

To install lso clone the git repository:

    git clone https://github.com/BenjaminSchiller/LiveStreamingOverlay.git

Then you need to generate the protocol buffer java files:

    protoc --java_out=src -I ./res/protocolbuffers ./res/protocolbuffers/*.proto


Eclipse Plugins
--------------

### Markdown

[Homepage](http://www.winterwell.com/software/markdown-editor.php)

[Update Site](http://winterstein.me.uk/projects/tt-update-site/)

### Protocol Buffers

[Homepage](http://code.google.com/p/protoclipse/)
[Update Site](http://protoclipse.googlecode.com/svn/trunk/site/)
