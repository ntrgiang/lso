package lso;

import java.util.HashSet;
import java.util.Set;

public class MStream {
	private String identifier;

	private int stripes;

	private Address tracker;

	private Set<Address> peers;

	private Address source;

	private int videoRate;

	public Address getTrackerMaintenanceAddress() {
		return this.tracker;
	}

	public String getIdentifier() {
		return this.identifier;
	}

	public int getStripes() {
		return this.stripes;
	}

	public MStream(String streamIdentifier, int stripes, int videoRate,
			Address trackerAddress, Address sourceAddress) {
		this.identifier = streamIdentifier;
		this.stripes = stripes;
		this.videoRate = videoRate;
		this.tracker = trackerAddress;
		this.source = sourceAddress;
		this.peers = new HashSet<Address>();
	}

	public String toString() {
		return this.source.toString() + ":'" + this.identifier + "' ("
				+ this.stripes + ")";
	}

	public void setStripes(int stripes) {
		this.stripes = stripes;
	}

	public void setVideoRate(int videoRate) {
		this.videoRate = videoRate;
	}

	/**
	 * @return the tracker
	 */
	public Address getTracker() {
		return tracker;
	}

	public Address getSource() {
		return source;
	}

	/**
	 * @return the peers
	 */
	public Set<Address> getPeers() {
		return peers;
	}

	/**
	 * @return the videoRate
	 */
	public int getVideoRate() {
		return videoRate;
	}
}
