package lso;

import java.util.Random;

public class Location {
	private int number;

	public int getNumber() {
		return this.number;
	}

	public Location(int number) {
		this(number + "");
	}

	public Location(String number) {
		try {
			this.number = Integer.parseInt(number);
		} catch (NumberFormatException e) {
			this.number = (int) Double.parseDouble(number);
		}
		if (this.number <= 0 || this.number > Common.LOCATION_NUMBER_OF) {
			throw new IllegalArgumentException("Location number cnnot be '"
					+ number + "', must be between 1 and "
					+ Common.LOCATION_NUMBER_OF);
		}
	}

	public String getStringRepresentation() {
		return this.number + "";
	}

	@Override
	public String toString() {
		return "Location: " + this.number;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Location)) {
			return false;
		}
		Location loc = (Location) obj;
		return this.number == loc.getNumber();
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

	public static Location getRandomLocation(Random random) {
		return new Location(random.nextInt(Common.LOCATION_NUMBER_OF) + 1);
	}

	public static Location getDefaultLocation() {
		return new Location(1);
	}
}
