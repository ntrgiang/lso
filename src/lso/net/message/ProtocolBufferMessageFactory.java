package lso.net.message;

import java.util.Map;

import lso.net.message.CommonProtocolBuffers.Address;
import lso.net.message.CommonProtocolBuffers.ChildAddress;
import lso.net.message.CommonProtocolBuffers.Property;
import lso.net.message.MessageProtocolBuffers.Message;
import lso.net.message.MessageProtocolBuffers.Message.Type;
import lso.net.message.maintenance.Alive;
import lso.net.message.maintenance.ChangeLocation;
import lso.net.message.maintenance.ChangeParent;
import lso.net.message.maintenance.Connect;
import lso.net.message.maintenance.ForwardChild;
import lso.net.message.maintenance.GetChildren;
import lso.net.message.maintenance.GetInfo;
import lso.net.message.maintenance.GetParents;
import lso.net.message.maintenance.GetPeerList;
import lso.net.message.maintenance.GetStreams;
import lso.net.message.maintenance.Kill;
import lso.net.message.maintenance.Leave;
import lso.net.message.maintenance.LeaveStripe;
import lso.net.message.maintenance.MGetAllPeers;
import lso.net.message.maintenance.MGetAllPeersForStream;
import lso.net.message.maintenance.MGetStreams;
import lso.net.message.maintenance.MaintenanceMessage;
import lso.net.message.maintenance.PAlive;
import lso.net.message.maintenance.RegisterStream;
import lso.net.message.maintenance.RequestChild;
import lso.net.message.maintenance.SAlive;
import lso.net.message.maintenance.SetProperties;
import lso.net.message.maintenance.Term;
import lso.net.message.maintenance.UnregisterStream;
import lso.net.message.maintenance.WhichConnections;

public class ProtocolBufferMessageFactory {
	public static Message fromMaintenanceMessage(
			MaintenanceMessage maintenanceMessage) {
		switch (maintenanceMessage.getType()) {
		case ALIVE:
			return fromAlive((Alive) maintenanceMessage);

		case CHANGE_PARENT:
			return fromChangeParent((ChangeParent) maintenanceMessage);

		case CONNECT:
			return fromConnect((Connect) maintenanceMessage);

		case FORWARD_CHILD:
			return fromForwardChild((ForwardChild) maintenanceMessage);

		case GET_CHILDREN:
			return fromGetChildren((GetChildren) maintenanceMessage);

		case GET_INFO:
			return fromGetInfo((GetInfo) maintenanceMessage);

		case GET_PARENTS:
			return fromGetParents((GetParents) maintenanceMessage);

		case GET_PEER_LIST:
			return fromGetPeerList((GetPeerList) maintenanceMessage);

		case GET_STREAMS:
			return fromGetStreams((GetStreams) maintenanceMessage);

		case KILL:
			return fromKill((Kill) maintenanceMessage);

		case LEAVE:
			return fromLeave((Leave) maintenanceMessage);

		case LEAVE_STRIPE:
			return fromLeaveStripe((LeaveStripe) maintenanceMessage);

		case REGISTER_STREAM:
			return fromRegisterStream((RegisterStream) maintenanceMessage);

		case REQUEST_CHILD:
			return fromRequestChild((RequestChild) maintenanceMessage);

		case SET_PROPERTIES:
			return fromSetProperties((SetProperties) maintenanceMessage);

		case TERM:
			return fromTerm((Term) maintenanceMessage);

		case UNREGISTER_STREAM:
			return fromUnregisterStream((UnregisterStream) maintenanceMessage);

		case S_ALIVE:
			return fromSAlive((SAlive) maintenanceMessage);

		case P_ALIVE:
			return fromPAlive((PAlive) maintenanceMessage);

		case M_GET_STREAMS:
			return fromMGetStreams((MGetStreams) maintenanceMessage);

		case M_GET_ALL_PEERS:
			return fromMGetAllPeers((MGetAllPeers) maintenanceMessage);

		case M_GET_ALL_PEERS_FOR_STREAM:
			return fromMGetAllPeersForStream((MGetAllPeersForStream) maintenanceMessage);

		case WHICH_CONNECTIONS:
			return fromWhichConnections((WhichConnections) maintenanceMessage);

		case CHANGE_LOCATION:
			return fromChangeLocation((ChangeLocation) maintenanceMessage);
		}

		return null;
	}

	private static Message fromAlive(Alive alive) {
		Address address = lso.net.message.CommonProtocolBuffers.Address
				.newBuilder()
				.setHostname(alive.getMaintenanceAddress().getHostname())
				.setPort(alive.getMaintenanceAddress().getPort()).build();

		Message.Alive pb = Message.Alive.newBuilder()
				.setMaintenanceAddress(address).build();

		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						alive.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						alive.getUuid().getMostSignificantBits())
				.setTimeStamp(alive.getTimestamp().getTime())
				.setType(Message.Type.ALIVE).setAlive(pb).build();
	}

	private static Message fromChangeParent(ChangeParent changeParent) {
		Address maintenanceAddress = Address
				.newBuilder()
				.setHostname(changeParent.getMaintenanceAddress().getHostname())
				.setPort(changeParent.getMaintenanceAddress().getPort())
				.build();

		Message.ChangeParent pb = Message.ChangeParent.newBuilder()
				.setStripe(changeParent.getStripeIdentifier())
				.setMaintenanceAddress(maintenanceAddress).build();

		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						changeParent.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						changeParent.getUuid().getMostSignificantBits())
				.setTimeStamp(changeParent.getTimestamp().getTime())
				.setType(Message.Type.CHANGE_PARENT).setChangeParent(pb)
				.build();
	}

	private static Message fromConnect(Connect connect) {
		Address maintenanceAddress = Address.newBuilder()
				.setHostname(connect.getMaintenanceAddress().getHostname())
				.setPort(connect.getMaintenanceAddress().getPort()).build();
		Address dataAddress = Address.newBuilder()
				.setHostname(connect.getDataAddress().getHostname())
				.setPort(connect.getDataAddress().getPort()).build();
		ChildAddress childAddres = ChildAddress.newBuilder()
				.setMaintenanceAddress(maintenanceAddress)
				.setDataAddress(dataAddress).build();

		Message.Connect pb = Message.Connect.newBuilder()
				.setStripe(connect.getStripeIdentifier())
				.setChildAddress(childAddres).build();

		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						connect.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						connect.getUuid().getMostSignificantBits())
				.setTimeStamp(connect.getTimestamp().getTime())
				.setType(Message.Type.CONNECT).setConnect(pb).build();
	}

	private static Message fromForwardChild(ForwardChild forwardChild) {
		Address maintenanceAddress = Address
				.newBuilder()
				.setHostname(forwardChild.getMaintenanceAddress().getHostname())
				.setPort(forwardChild.getMaintenanceAddress().getPort())
				.build();
		Address dataAddress = Address.newBuilder()
				.setHostname(forwardChild.getDataAddress().getHostname())
				.setPort(forwardChild.getDataAddress().getPort()).build();
		ChildAddress childAddress = ChildAddress.newBuilder()
				.setMaintenanceAddress(maintenanceAddress)
				.setDataAddress(dataAddress).build();

		Message.ForwardChild pb = Message.ForwardChild.newBuilder()
				.setStripe(forwardChild.getStripeIdentifier())
				.setChildAddress(childAddress).build();

		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						forwardChild.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						forwardChild.getUuid().getMostSignificantBits())
				.setTimeStamp(forwardChild.getTimestamp().getTime())
				.setType(Message.Type.FORWARD_CHILD).setForwardChild(pb)
				.build();
	}

	private static Message fromGetInfo(GetInfo getInfo) {
		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						getInfo.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						getInfo.getUuid().getMostSignificantBits())
				.setTimeStamp(getInfo.getTimestamp().getTime())
				.setType(Message.Type.GET_INFO)
				.setGetInfo(Message.GetInfo.newBuilder().build()).build();
	}

	private static Message fromGetParents(GetParents getParents) {
		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						getParents.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						getParents.getUuid().getMostSignificantBits())
				.setTimeStamp(getParents.getTimestamp().getTime())
				.setType(Message.Type.GET_PARENTS)
				.setGetParents(Message.GetParents.newBuilder().build()).build();
	}

	private static Message fromGetPeerList(GetPeerList getPeerList) {
		Address address = lso.net.message.CommonProtocolBuffers.Address
				.newBuilder()
				.setHostname(getPeerList.getMaintenanceAddress().getHostname())
				.setPort(getPeerList.getMaintenanceAddress().getPort()).build();

		Message.GetPeerList pb = Message.GetPeerList.newBuilder()
				.setStreamIdentifier(getPeerList.getStreamIdentifier())
				.setMaintenanceAddress(address)
				.setLocation(getPeerList.getLocation()).build();

		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						getPeerList.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						getPeerList.getUuid().getMostSignificantBits())
				.setTimeStamp(getPeerList.getTimestamp().getTime())
				.setType(Message.Type.GET_PEER_LIST).setGetPeerList(pb).build();
	}

	private static Message fromGetStreams(GetStreams getStreams) {
		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						getStreams.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						getStreams.getUuid().getMostSignificantBits())
				.setTimeStamp(getStreams.getTimestamp().getTime())
				.setType(Message.Type.GET_STREAMS)
				.setGetStreams(Message.GetStreams.newBuilder().build()).build();
	}

	private static Message fromLeave(Leave leave) {
		Address address = lso.net.message.CommonProtocolBuffers.Address
				.newBuilder()
				.setHostname(leave.getMaintenanceAddress().getHostname())
				.setPort(leave.getMaintenanceAddress().getPort()).build();
		Message.Leave pb = Message.Leave.newBuilder()
				.setMaintenanceAddress(address).build();

		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						leave.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						leave.getUuid().getMostSignificantBits())
				.setTimeStamp(leave.getTimestamp().getTime())
				.setType(Message.Type.LEAVE).setLeave(pb).build();
	}

	private static Message fromLeaveStripe(LeaveStripe leaveStripe) {
		Address maintenanceAddress = Address.newBuilder()
				.setHostname(leaveStripe.getMaintenanceAddress().getHostname())
				.setPort(leaveStripe.getMaintenanceAddress().getPort()).build();

		Address newParent = Address.newBuilder()
				.setHostname(leaveStripe.getNewParent().getHostname())
				.setPort(leaveStripe.getNewParent().getPort()).build();

		Message.LeaveStripe ls = Message.LeaveStripe.newBuilder()
				.setStripe(leaveStripe.getStripe())
				.setMaintenanceAddress(maintenanceAddress)
				.setNewParent(newParent).build();

		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						leaveStripe.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						leaveStripe.getUuid().getMostSignificantBits())
				.setTimeStamp(leaveStripe.getTimestamp().getTime())
				.setType(Message.Type.LEAVE_STRIPE).setLeaveStripe(ls).build();
	}

	private static Message fromRegisterStream(RegisterStream registerStream) {
		Address address = lso.net.message.CommonProtocolBuffers.Address
				.newBuilder()
				.setHostname(
						registerStream.getMaintenanceAddress().getHostname())
				.setPort(registerStream.getMaintenanceAddress().getPort())
				.build();

		Message.RegisterStream pb = Message.RegisterStream.newBuilder()
				.setStreamIdentifier(registerStream.getStreamIdentifier())
				.setStripes(registerStream.getStripes())
				.setVideoRate(registerStream.getVideoRate())
				.setStreamDescriptor(registerStream.getStreamDescriptor())
				.setMaintenanceAddress(address).build();

		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						registerStream.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						registerStream.getUuid().getMostSignificantBits())
				.setTimeStamp(registerStream.getTimestamp().getTime())
				.setType(Message.Type.REGISTER_STREAM).setRegisterStream(pb)
				.build();
	}

	private static Message fromRequestChild(RequestChild requestChild) {
		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						requestChild.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						requestChild.getUuid().getMostSignificantBits())
				.setTimeStamp(requestChild.getTimestamp().getTime())
				.setType(Message.Type.REQUEST_CHILD)
				.setRequestChild(
						Message.RequestChild.newBuilder()
								.setStripe(requestChild.getStripe()).build())
				.build();
	}

	private static Message fromSetProperties(SetProperties setProperties) {
		Message.SetProperties.Builder spmb = Message.SetProperties.newBuilder();

		Map<String, String> properties = setProperties.getProperties();

		for (String key : properties.keySet()) {
			spmb.addProperty(Property.newBuilder().setKey(key)
					.setValue(properties.get(key)).build());
		}

		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						setProperties.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						setProperties.getUuid().getMostSignificantBits())
				.setTimeStamp(setProperties.getTimestamp().getTime())
				.setType(Message.Type.SET_PROPERTIES)
				.setSetProperties(spmb.build()).build();
	}

	private static Message fromUnregisterStream(
			UnregisterStream unregisterStream) {
		Address address = lso.net.message.CommonProtocolBuffers.Address
				.newBuilder()
				.setHostname(
						unregisterStream.getMaintenanceAddress().getHostname())
				.setPort(unregisterStream.getMaintenanceAddress().getPort())
				.build();

		Message.UnregisterStream pb = Message.UnregisterStream.newBuilder()
				.setStreamIdentifier(unregisterStream.getStreamIdentifier())
				.setMaintenanceAddress(address).build();

		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						unregisterStream.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						unregisterStream.getUuid().getMostSignificantBits())
				.setTimeStamp(unregisterStream.getTimestamp().getTime())
				.setType(Message.Type.UNREGISTER_STREAM)
				.setUnregisterStream(pb).build();
	}

	private static Message fromGetChildren(GetChildren getChildren) {
		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						getChildren.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						getChildren.getUuid().getMostSignificantBits())
				.setTimeStamp(getChildren.getTimestamp().getTime())
				.setType(Type.GET_CHILDREN)
				.setGetChildren(Message.GetChildren.newBuilder().build())
				.build();
	}

	private static Message fromTerm(Term term) {
		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						term.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(term.getUuid().getMostSignificantBits())
				.setTimeStamp(term.getTimestamp().getTime()).setType(Type.TERM)
				.setTerm(Message.Term.newBuilder().build()).build();
	}

	private static Message fromKill(Kill kill) {
		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						kill.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(kill.getUuid().getMostSignificantBits())
				.setTimeStamp(kill.getTimestamp().getTime()).setType(Type.KILL)
				.setKill(Message.Kill.newBuilder().build()).build();
	}

	private static Message fromSAlive(SAlive sAlive) {
		Address address = lso.net.message.CommonProtocolBuffers.Address
				.newBuilder()
				.setHostname(sAlive.getMaintenanceAddress().getHostname())
				.setPort(sAlive.getMaintenanceAddress().getPort()).build();

		Message.SAlive pb = Message.SAlive.newBuilder()
				.setMaintenanceAddress(address).build();

		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						sAlive.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						sAlive.getUuid().getMostSignificantBits())
				.setTimeStamp(sAlive.getTimestamp().getTime())
				.setType(Message.Type.S_ALIVE).setSAlive(pb).build();
	}

	private static Message fromPAlive(PAlive pAlive) {
		Address address = lso.net.message.CommonProtocolBuffers.Address
				.newBuilder()
				.setHostname(pAlive.getMaintenanceAddress().getHostname())
				.setPort(pAlive.getMaintenanceAddress().getPort()).build();

		Message.PAlive pb = Message.PAlive.newBuilder()
				.setMaintenanceAddress(address).build();

		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						pAlive.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						pAlive.getUuid().getMostSignificantBits())
				.setTimeStamp(pAlive.getTimestamp().getTime())
				.setType(Message.Type.P_ALIVE).setPAlive(pb).build();
	}

	private static Message fromMGetStreams(MGetStreams mGetStreams) {
		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						mGetStreams.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						mGetStreams.getUuid().getMostSignificantBits())
				.setTimeStamp(mGetStreams.getTimestamp().getTime())
				.setType(Type.M_GET_STREAMS)
				.setMGetStreams(Message.MGetStreams.newBuilder().build())
				.build();
	}

	private static Message fromMGetAllPeers(MGetAllPeers mGetAllPeers) {
		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						mGetAllPeers.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						mGetAllPeers.getUuid().getMostSignificantBits())
				.setTimeStamp(mGetAllPeers.getTimestamp().getTime())
				.setType(Type.M_GET_ALL_PEERS)
				.setMGetAllPeers(Message.MGetAllPeers.newBuilder().build())
				.build();
	}

	private static Message fromMGetAllPeersForStream(
			MGetAllPeersForStream mGetAllPeersForStream) {
		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						mGetAllPeersForStream.getUuid()
								.getLeastSignificantBits())
				.setUuidMostSignificant(
						mGetAllPeersForStream.getUuid()
								.getMostSignificantBits())
				.setTimeStamp(mGetAllPeersForStream.getTimestamp().getTime())
				.setType(Type.M_GET_ALL_PEERS_FOR_STREAM)
				.setMGetAllPeersForStream(
						Message.MGetAllPeersForStream
								.newBuilder()
								.setStreamIdentifier(
										mGetAllPeersForStream
												.getStreamIdentifier()).build())
				.build();
	}

	private static Message fromWhichConnections(
			WhichConnections whichConnections) {
		Address address = lso.net.message.CommonProtocolBuffers.Address
				.newBuilder()
				.setHostname(
						whichConnections.getMaintenanceAddress().getHostname())
				.setPort(whichConnections.getMaintenanceAddress().getPort())
				.build();
		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						whichConnections.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						whichConnections.getUuid().getMostSignificantBits())
				.setTimeStamp(whichConnections.getTimestamp().getTime())
				.setType(Type.WHICH_CONNECTIONS)
				.setWhichConnections(
						Message.WhichConnections.newBuilder()
								.setMaintenanceAddress(address)).build();
	}

	private static Message fromChangeLocation(ChangeLocation changeLocation) {
		Address maintenanceAddress = Address
				.newBuilder()
				.setHostname(
						changeLocation.getMaintenanceAddress().getHostname())
				.setPort(changeLocation.getMaintenanceAddress().getPort())
				.build();
		return Message
				.newBuilder()
				.setUuidLeastSignificant(
						changeLocation.getUuid().getLeastSignificantBits())
				.setUuidMostSignificant(
						changeLocation.getUuid().getMostSignificantBits())
				.setTimeStamp(changeLocation.getTimestamp().getTime())
				.setType(Type.CHANGE_LOCATION)
				.setChangeLocation(
						Message.ChangeLocation.newBuilder()
								.setLocation(changeLocation.getLocation())
								.setMaintenanceAddress(maintenanceAddress))
				.build();
	}
}
