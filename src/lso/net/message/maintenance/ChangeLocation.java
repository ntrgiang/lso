package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

import lso.Address;

public class ChangeLocation extends MaintenanceMessage {

	protected String location;

	protected Address maintenanceAddress;

	public ChangeLocation(String location, Address maintenanceAddress) {
		super();
		this.location = location;
		this.maintenanceAddress = maintenanceAddress;
	}

	public ChangeLocation(UUID uuid, Date timestamp, String location,
			Address maintenanceAddress) {
		super(uuid, timestamp);
		this.location = location;
		this.maintenanceAddress = maintenanceAddress;
	}

	@Override
	public Type getType() {
		return Type.CHANGE_LOCATION;
	}

	public String getLocation() {
		return this.location;
	}

	public Address getMaintenanceAddress() {
		return this.maintenanceAddress;
	}

}
