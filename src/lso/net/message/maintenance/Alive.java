package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

import lso.Address;

public class Alive extends MaintenanceMessage {
	protected Address maintenanceAddress;

	public Alive(Address maintenanceAddress) {
		super();
		this.maintenanceAddress = maintenanceAddress;
	}

	public Alive(UUID uuid, Date timestamp, Address maintenanceAddress) {
		this.uuid = uuid;
		this.timestamp = timestamp;
		this.maintenanceAddress = maintenanceAddress;
	}

	@Override
	public Type getType() {
		return Type.ALIVE;
	}

	public Address getMaintenanceAddress() {
		return this.maintenanceAddress;
	}
}
