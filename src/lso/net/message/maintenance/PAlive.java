package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

import lso.Address;

public class PAlive extends MaintenanceMessage {
	protected Address maintenanceAddress;

	public PAlive(Address maintenanceAddress) {
		super();
		this.maintenanceAddress = maintenanceAddress;
	}

	public PAlive(UUID uuid, Date timestamp, Address maintenanceAddress) {
		super(uuid, timestamp);
		this.maintenanceAddress = maintenanceAddress;
	}

	@Override
	public Type getType() {
		return Type.P_ALIVE;
	}

	public Address getMaintenanceAddress() {
		return this.maintenanceAddress;
	}
}
