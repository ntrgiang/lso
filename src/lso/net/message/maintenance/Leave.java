package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

import lso.Address;

public class Leave extends MaintenanceMessage {
	protected Address maintenanceAddress;

	public Leave(Address maintenanceAddress) {
		super();
		this.maintenanceAddress = maintenanceAddress;
	}

	public Leave(UUID uuid, Date timestamp, Address maintenanceAddress) {
		this.uuid = uuid;
		this.timestamp = timestamp;
		this.maintenanceAddress = maintenanceAddress;
	}

	@Override
	public Type getType() {
		return Type.LEAVE;
	}

	public Address getMaintenanceAddress() {
		return this.maintenanceAddress;
	}
}
