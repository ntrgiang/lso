package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

import lso.Address;

public class LeaveStripe extends MaintenanceMessage {
	protected int stripe;

	protected Address maintenanceAddress;

	protected Address newParent;

	public LeaveStripe(int stripe, Address maintenanceAddress, Address newParent) {
		super();
		this.stripe = stripe;
		this.maintenanceAddress = maintenanceAddress;
		this.newParent = newParent;
	}

	public LeaveStripe(UUID uuid, Date timestamp, int stripe,
			Address maintenanceAddress, Address newParent) {
		this(stripe, maintenanceAddress, newParent);
		this.uuid = uuid;
		this.timestamp = timestamp;
	}

	@Override
	public Type getType() {
		return Type.LEAVE_STRIPE;
	}

	public Address getMaintenanceAddress() {
		return this.maintenanceAddress;
	}

	public int getStripe() {
		return this.stripe;
	}

	public Address getNewParent() {
		return this.newParent;
	}

}
