package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

public class MGetAllPeers extends MaintenanceMessage {

	public MGetAllPeers(UUID uuid, Date timestamp) {
		super(uuid, timestamp);
	}

	@Override
	public Type getType() {
		return Type.M_GET_ALL_PEERS;
	}

}
