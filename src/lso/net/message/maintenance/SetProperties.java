package lso.net.message.maintenance;

import java.sql.Date;
import java.util.Map;
import java.util.UUID;

public class SetProperties extends MaintenanceMessage {

	protected Map<String, String> properties;

	public SetProperties(Map<String, String> properties) {
		super();
		this.properties = properties;
	}

	public SetProperties(UUID uuid, Date timestamp,
			Map<String, String> properties) {
		this.uuid = uuid;
		this.timestamp = timestamp;
		this.properties = properties;
	}

	@Override
	public Type getType() {
		return Type.SET_PROPERTIES;
	}

	public Map<String, String> getProperties() {
		return this.properties;
	}
}
