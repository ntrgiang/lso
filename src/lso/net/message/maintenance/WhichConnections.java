package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

import lso.Address;

public class WhichConnections extends MaintenanceMessage {

	protected Address maintenanceAddress;

	public WhichConnections(Address maintenanceAddress) {
		super();
		this.maintenanceAddress = maintenanceAddress;
	}

	public WhichConnections(UUID uuid, Date timestamp,
			Address maintenanceAddress) {
		this.uuid = uuid;
		this.timestamp = timestamp;
		this.maintenanceAddress = maintenanceAddress;
	}

	@Override
	public Type getType() {
		return Type.WHICH_CONNECTIONS;
	}

	public Address getMaintenanceAddress() {
		return this.maintenanceAddress;
	}

}
