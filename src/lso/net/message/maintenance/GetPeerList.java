package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

import lso.Address;

public class GetPeerList extends MaintenanceMessage {

	protected String streamIdentifier;

	protected Address maintenanceAddress;

	protected String location;

	public GetPeerList(String streamIdentifier, Address maintenanceAddress,
			String location) {
		super();
		this.streamIdentifier = streamIdentifier;
		this.maintenanceAddress = maintenanceAddress;
		this.location = location;
	}

	public GetPeerList(UUID uuid, Date timestamp, String streamIdentifier,
			Address maintenanceAddress, String location) {
		this.uuid = uuid;
		this.timestamp = timestamp;
		this.streamIdentifier = streamIdentifier;
		this.maintenanceAddress = maintenanceAddress;
		this.location = location;
	}

	@Override
	public Type getType() {
		return Type.GET_PEER_LIST;
	}

	public String getStreamIdentifier() {
		return this.streamIdentifier;
	}

	public Address getMaintenanceAddress() {
		return this.maintenanceAddress;
	}

	public String getLocation() {
		return this.location;
	}
}
