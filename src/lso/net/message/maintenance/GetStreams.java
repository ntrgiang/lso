package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

import lso.Address;

public class GetStreams extends MaintenanceMessage {

	protected Address maintenanceAddress;

	public GetStreams() {
		super();
	}

	public GetStreams(UUID uuid, Date timestamp) {
		super(uuid, timestamp);
	}

	@Override
	public Type getType() {
		return Type.GET_STREAMS;
	}
}
