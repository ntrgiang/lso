package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

import lso.Address;

public class ForwardChild extends MaintenanceMessage {

	protected int stripeIdentifier;

	protected Address maintenanceAddress;

	protected Address dataAddress;

	public ForwardChild(int stripeIdentifier, Address maintenanceAddress,
			Address dataAddress) {
		super();
		this.stripeIdentifier = stripeIdentifier;
		this.maintenanceAddress = maintenanceAddress;
		this.dataAddress = dataAddress;
	}

	public ForwardChild(UUID uuid, Date timestamp, int stripeIdentifier,
			Address maintenanceAddress, Address dataAddress) {
		super(uuid, timestamp);
		this.stripeIdentifier = stripeIdentifier;
		this.maintenanceAddress = maintenanceAddress;
		this.dataAddress = dataAddress;
	}

	@Override
	public Type getType() {
		return Type.FORWARD_CHILD;
	}

	public int getStripeIdentifier() {
		return stripeIdentifier;
	}

	public Address getMaintenanceAddress() {
		return maintenanceAddress;
	}

	public Address getDataAddress() {
		return dataAddress;
	}
}
