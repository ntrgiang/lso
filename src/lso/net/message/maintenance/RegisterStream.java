package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

import lso.Address;

public class RegisterStream extends MaintenanceMessage {
	protected UUID uuid;

	protected Date timestamp;

	protected String streamIdentifier;

	protected Address maintenanceAddress;

	protected int stripes;

	protected int videoRate;

	protected String streamDescriptor;

	public RegisterStream(String streamIdentifer, int stripes, int videoRate,
			String streamDescriptor, Address maintenanceAddress) {
		super();
		this.streamIdentifier = streamIdentifer;
		this.stripes = stripes;
		this.videoRate = videoRate;
		this.streamDescriptor = streamDescriptor;
		this.maintenanceAddress = maintenanceAddress;
	}

	public RegisterStream(UUID uuid, Date timestamp, String streamIdentifer,
			int stripes, int videoRate, String streamDescriptor,
			Address maintenanceAddress) {
		this.uuid = uuid;
		this.timestamp = timestamp;
		this.streamIdentifier = streamIdentifer;
		this.stripes = stripes;
		this.videoRate = videoRate;
		this.streamDescriptor = streamDescriptor;
		this.maintenanceAddress = maintenanceAddress;
	}

	public String getStreamIdentifier() {
		return this.streamIdentifier;
	}

	public Address getMaintenanceAddress() {
		return this.maintenanceAddress;
	}

	public int getStripes() {
		return this.stripes;
	}

	public int getVideoRate() {
		return this.videoRate;
	}

	@Override
	public Type getType() {
		return Type.REGISTER_STREAM;
	}

	public String getStreamDescriptor() {
		return streamDescriptor;
	}
}
