package lso.net.message.maintenance;

import lso.net.response.ReturnCode;
import lso.net.response.maintenance.MaintenanceResponse;
import lso.net.response.maintenance.Null;

public abstract class MaintenanceMessageHandler {

	public MaintenanceResponse handleMaintenanceMessage(
			MaintenanceMessage maintenanceMessage) {

		MaintenanceResponse response = new Null(
				ReturnCode.UNKNOWN_MESSAGE_TYPE_ERROR);

		switch (maintenanceMessage.getType()) {
		case ALIVE:
			response = handleAlive((Alive) maintenanceMessage);
			break;

		case CHANGE_PARENT:
			response = handleChangeParent((ChangeParent) maintenanceMessage);
			break;

		case CONNECT:
			response = handleConnect((Connect) maintenanceMessage);
			break;

		case FORWARD_CHILD:
			response = handleForwardChild((ForwardChild) maintenanceMessage);
			break;

		case GET_CHILDREN:
			response = handleGetChildren((GetChildren) maintenanceMessage);
			break;

		case GET_INFO:
			response = handleGetInfo((GetInfo) maintenanceMessage);
			break;

		case GET_PARENTS:
			response = handleGetParents((GetParents) maintenanceMessage);
			break;

		case GET_PEER_LIST:
			response = handleGetPeerList((GetPeerList) maintenanceMessage);
			break;

		case GET_STREAMS:
			response = handleGetStreams((GetStreams) maintenanceMessage);
			break;

		case KILL:
			response = handleKill((Kill) maintenanceMessage);
			break;

		case LEAVE:
			response = handleLeave((Leave) maintenanceMessage);
			break;

		case LEAVE_STRIPE:
			response = handleLeaveStripe((LeaveStripe) maintenanceMessage);
			break;

		case M_GET_ALL_PEERS:
			response = handleMGetAllPeers((MGetAllPeers) maintenanceMessage);
			break;

		case M_GET_ALL_PEERS_FOR_STREAM:
			response = handleMGetAllPeersForStream((MGetAllPeersForStream) maintenanceMessage);
			break;

		case M_GET_STREAMS:
			response = handleMGetStreams((MGetStreams) maintenanceMessage);
			break;

		case P_ALIVE:
			response = handlePAlive((PAlive) maintenanceMessage);
			break;

		case REGISTER_STREAM:
			response = handleRegisterStream((RegisterStream) maintenanceMessage);
			break;

		case REQUEST_CHILD:
			response = handleRequestChild((RequestChild) maintenanceMessage);
			break;

		case S_ALIVE:
			response = handleSAlive((SAlive) maintenanceMessage);
			break;

		case SET_PROPERTIES:
			response = handleSetProperties((SetProperties) maintenanceMessage);
			break;

		case TERM:
			response = handleTerm((Term) maintenanceMessage);
			break;

		case UNREGISTER_STREAM:
			response = handleUnregisterStream((UnregisterStream) maintenanceMessage);
			break;

		case WHICH_CONNECTIONS:
			response = handleWhichConnections((WhichConnections) maintenanceMessage);
			break;

		case CHANGE_LOCATION:
			response = handleChangeLocation((ChangeLocation) maintenanceMessage);
			break;

		default:
			System.err.println("Unkown Message Type ("
					+ maintenanceMessage.getType() + ")!");
		}

		return response;
	}

	protected MaintenanceResponse createUnknownMessageResponse() {
		return new Null(ReturnCode.UNKNOWN_MESSAGE_TYPE_ERROR);
	}

	protected abstract MaintenanceResponse handleAlive(Alive alive);

	protected abstract MaintenanceResponse handleChangeParent(
			ChangeParent changeParent);

	protected abstract MaintenanceResponse handleConnect(Connect connect);

	protected abstract MaintenanceResponse handleForwardChild(
			ForwardChild forwardChild);

	protected abstract MaintenanceResponse handleGetChildren(
			GetChildren getChildren);

	protected abstract MaintenanceResponse handleGetInfo(GetInfo getInfo);

	protected abstract MaintenanceResponse handleGetParents(
			GetParents getParents);

	protected abstract MaintenanceResponse handleGetPeerList(
			GetPeerList getPeerList);

	protected abstract MaintenanceResponse handleGetStreams(
			GetStreams getStreams);

	protected abstract MaintenanceResponse handleKill(Kill kill);

	protected abstract MaintenanceResponse handleLeave(Leave leave);

	protected abstract MaintenanceResponse handleLeaveStripe(
			LeaveStripe leaveStripe);

	protected abstract MaintenanceResponse handleMGetAllPeers(
			MGetAllPeers mGetAllPeers);

	protected abstract MaintenanceResponse handleMGetAllPeersForStream(
			MGetAllPeersForStream mGetAllPeersForStream);

	protected abstract MaintenanceResponse handleMGetStreams(
			MGetStreams mGetStreams);

	protected abstract MaintenanceResponse handlePAlive(PAlive pAlive);

	protected abstract MaintenanceResponse handleRegisterStream(
			RegisterStream registerStream);

	protected abstract MaintenanceResponse handleRequestChild(
			RequestChild requestChild);

	protected abstract MaintenanceResponse handleSAlive(SAlive sAlive);

	protected abstract MaintenanceResponse handleSetProperties(
			SetProperties setProperties);

	protected abstract MaintenanceResponse handleTerm(Term term);

	protected abstract MaintenanceResponse handleUnregisterStream(
			UnregisterStream unregisterStream);

	protected abstract MaintenanceResponse handleWhichConnections(
			WhichConnections whichConnections);

	protected abstract MaintenanceResponse handleChangeLocation(
			ChangeLocation changeLocation);

}
