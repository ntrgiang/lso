package lso.net.message.maintenance;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import lso.Address;
import lso.net.message.CommonProtocolBuffers.Property;
import lso.net.message.MessageProtocolBuffers.Message;

public class MaintenanceMessageFactory {

	public static MaintenanceMessage fromMessage(Message message) {
		UUID uuid = new UUID(message.getUuidMostSignificant(),
				message.getUuidLeastSignificant());
		Date timestamp = new Date(message.getTimeStamp());

		Address maintenanceAddress = null;
		Address dataAddress = null;

		String streamIdentifier = "";
		int stripeIdentifier = 0;

		switch (message.getType()) {
		case ALIVE:
			Message.Alive a = message.getAlive();
			maintenanceAddress = new Address(a.getMaintenanceAddress()
					.getHostname(), a.getMaintenanceAddress().getPort());
			return new Alive(uuid, timestamp, maintenanceAddress);

		case REGISTER_STREAM:
			Message.RegisterStream rs = message.getRegisterStream();
			maintenanceAddress = new Address(rs.getMaintenanceAddress()
					.getHostname(), rs.getMaintenanceAddress().getPort());
			return new RegisterStream(uuid, timestamp,
					rs.getStreamIdentifier(), rs.getStripes(),
					rs.getVideoRate(), rs.getStreamDescriptor(),
					maintenanceAddress);

		case UNREGISTER_STREAM:
			Message.UnregisterStream urs = message.getUnregisterStream();
			maintenanceAddress = new Address(urs.getMaintenanceAddress()
					.getHostname(), urs.getMaintenanceAddress().getPort());
			return new UnregisterStream(uuid, timestamp,
					urs.getStreamIdentifier(), maintenanceAddress);

		case GET_PEER_LIST:
			Message.GetPeerList gpl = message.getGetPeerList();
			streamIdentifier = gpl.getStreamIdentifier();
			maintenanceAddress = new Address(gpl.getMaintenanceAddress()
					.getHostname(), gpl.getMaintenanceAddress().getPort());
			String location = gpl.getLocation();
			return new GetPeerList(uuid, timestamp, streamIdentifier,
					maintenanceAddress, location);

		case GET_STREAMS:
			return new GetStreams(uuid, timestamp);

		case GET_CHILDREN:
			return new GetChildren(uuid, timestamp);

		case GET_PARENTS:
			return new GetParents(uuid, timestamp);

		case CONNECT:
			Message.Connect connect = message.getConnect();
			stripeIdentifier = connect.getStripe();
			maintenanceAddress = new Address(connect.getChildAddress()
					.getMaintenanceAddress().getHostname(), connect
					.getChildAddress().getMaintenanceAddress().getPort());
			dataAddress = new Address(connect.getChildAddress()
					.getDataAddress().getHostname(), connect.getChildAddress()
					.getDataAddress().getPort());
			return new Connect(uuid, timestamp, stripeIdentifier,
					maintenanceAddress, dataAddress);

		case FORWARD_CHILD:
			Message.ForwardChild forwardChild = message.getForwardChild();
			stripeIdentifier = forwardChild.getStripe();
			maintenanceAddress = new Address(forwardChild.getChildAddress()
					.getMaintenanceAddress().getHostname(), forwardChild
					.getChildAddress().getMaintenanceAddress().getPort());
			dataAddress = new Address(forwardChild.getChildAddress()
					.getDataAddress().getHostname(), forwardChild
					.getChildAddress().getDataAddress().getPort());
			return new ForwardChild(uuid, timestamp, stripeIdentifier,
					maintenanceAddress, dataAddress);

		case CHANGE_PARENT:
			Message.ChangeParent changeParent = message.getChangeParent();
			stripeIdentifier = changeParent.getStripe();
			maintenanceAddress = new Address(changeParent
					.getMaintenanceAddress().getHostname(), changeParent
					.getMaintenanceAddress().getPort());
			return new ChangeParent(uuid, timestamp, stripeIdentifier,
					maintenanceAddress);

		case LEAVE:
			Message.Leave leave = message.getLeave();
			maintenanceAddress = new Address(leave.getMaintenanceAddress()
					.getHostname(), leave.getMaintenanceAddress().getPort());
			return new Leave(uuid, timestamp, maintenanceAddress);

		case LEAVE_STRIPE:
			Message.LeaveStripe leaveStripe = message.getLeaveStripe();
			stripeIdentifier = leaveStripe.getStripe();
			maintenanceAddress = new Address(leaveStripe
					.getMaintenanceAddress().getHostname(), leaveStripe
					.getMaintenanceAddress().getPort());
			Address newParent = new Address(leaveStripe.getNewParent()
					.getHostname(), leaveStripe.getNewParent().getPort());
			return new LeaveStripe(uuid, timestamp, stripeIdentifier,
					maintenanceAddress, newParent);

		case GET_INFO:
			return new GetInfo(uuid, timestamp);

		case SET_PROPERTIES:
			Message.SetProperties setProperties = message.getSetProperties();

			Map<String, String> properties = new HashMap<String, String>();

			for (Property property : setProperties.getPropertyList()) {
				properties.put(property.getKey(), property.getValue());
			}

			return new SetProperties(uuid, timestamp, properties);

		case REQUEST_CHILD:
			Message.RequestChild rc = message.getRequestChild();

			return new RequestChild(uuid, timestamp, rc.getStripe());

		case S_ALIVE:
			Message.SAlive sa = message.getSAlive();
			maintenanceAddress = new Address(sa.getMaintenanceAddress()
					.getHostname(), sa.getMaintenanceAddress().getPort());
			return new SAlive(uuid, timestamp, maintenanceAddress);

		case P_ALIVE:
			Message.PAlive pa = message.getPAlive();
			maintenanceAddress = new Address(pa.getMaintenanceAddress()
					.getHostname(), pa.getMaintenanceAddress().getPort());
			return new PAlive(uuid, timestamp, maintenanceAddress);

		case TERM:
			return new Term(uuid, timestamp);

		case KILL:
			return new Kill(uuid, timestamp);

		case M_GET_STREAMS:
			return new MGetStreams(uuid, timestamp);

		case M_GET_ALL_PEERS:
			return new MGetAllPeers(uuid, timestamp);

		case M_GET_ALL_PEERS_FOR_STREAM:
			Message.MGetAllPeersForStream mgapfs = message
					.getMGetAllPeersForStream();
			return new MGetAllPeersForStream(uuid, timestamp,
					mgapfs.getStreamIdentifier());

		case WHICH_CONNECTIONS:
			Message.WhichConnections wc = message.getWhichConnections();
			return new WhichConnections(uuid, timestamp, new Address(wc
					.getMaintenanceAddress().getHostname(), wc
					.getMaintenanceAddress().getPort()));

		case CHANGE_LOCATION:
			Message.ChangeLocation cl = message.getChangeLocation();
			maintenanceAddress = new Address(cl.getMaintenanceAddress()
					.getHostname(), cl.getMaintenanceAddress().getPort());
			return new ChangeLocation(uuid, timestamp, cl.getLocation(),
					maintenanceAddress);
		}

		return null;
	}
}
