package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

import lso.Address;

public class UnregisterStream extends MaintenanceMessage {

	protected UUID uuid;

	protected Date timestamp;

	protected String streamIdentifier;

	protected Address maintenanceAddress;

	public UnregisterStream(String streamIdentifer, Address maintenanceAddress) {
		super();
		this.streamIdentifier = streamIdentifer;
		this.maintenanceAddress = maintenanceAddress;
	}

	public UnregisterStream(UUID uuid, Date timestamp, String streamIdentifer,
			Address maintenanceAddress) {
		this.uuid = uuid;
		this.timestamp = timestamp;
		this.streamIdentifier = streamIdentifer;
		this.maintenanceAddress = maintenanceAddress;
	}

	public String getStreamIdentifier() {
		return this.streamIdentifier;
	}

	public Address getMaintenanceAddress() {
		return this.maintenanceAddress;
	}

	@Override
	public Type getType() {
		return Type.UNREGISTER_STREAM;
	}
}
