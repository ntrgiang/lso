package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

public class MGetAllPeersForStream extends MaintenanceMessage {

	protected String streamIdentifier;

	public MGetAllPeersForStream(String streamIdentifier) {
		super();
		this.streamIdentifier = streamIdentifier;
	}

	public MGetAllPeersForStream(UUID uuid, Date timestamp,
			String streamIdentifier) {
		super(uuid, timestamp);
		this.streamIdentifier = streamIdentifier;
	}

	@Override
	public Type getType() {
		return Type.M_GET_ALL_PEERS_FOR_STREAM;
	}

	public String getStreamIdentifier() {
		return this.streamIdentifier;
	}

}
