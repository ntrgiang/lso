package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

import lso.Address;

public class ChangeParent extends MaintenanceMessage {

	protected int stripeIdentifier;

	protected Address maintenanceAddress;

	public ChangeParent(int stripeIdentifier, Address maintenanceAddress) {
		super();
		this.stripeIdentifier = stripeIdentifier;
		this.maintenanceAddress = maintenanceAddress;
	}

	public ChangeParent(UUID uuid, Date timestamp, int stripeIdentifier,
			Address maintenanceAddress) {
		super(uuid, timestamp);
		this.stripeIdentifier = stripeIdentifier;
		this.maintenanceAddress = maintenanceAddress;
	}

	@Override
	public Type getType() {
		return Type.CHANGE_PARENT;
	}

	public int getStripeIdentifier() {
		return stripeIdentifier;
	}

	public Address getMaintenanceAddress() {
		return maintenanceAddress;
	}
}
