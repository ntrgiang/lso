package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

import lso.Address;

public class SAlive extends MaintenanceMessage {
	protected Address maintenanceAddress;

	public SAlive(Address maintenanceAddress) {
		super();
		this.maintenanceAddress = maintenanceAddress;
	}

	public SAlive(UUID uuid, Date timestamp, Address maintenanceAddress) {
		super(uuid, timestamp);
		this.maintenanceAddress = maintenanceAddress;
	}

	@Override
	public Type getType() {
		return Type.S_ALIVE;
	}

	public Address getMaintenanceAddress() {
		return this.maintenanceAddress;
	}
}
