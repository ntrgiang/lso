package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

public class Kill extends MaintenanceMessage {

	public Kill(UUID uuid, Date timestamp) {
		super(uuid, timestamp);
	}

	@Override
	public Type getType() {
		return Type.KILL;
	}

}
