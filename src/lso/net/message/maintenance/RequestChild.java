package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

public class RequestChild extends MaintenanceMessage {

	protected int stripe;

	public RequestChild(int stripe) {
		super();
		this.stripe = stripe;
	}

	public RequestChild(UUID uuid, Date timestamp, int stripe) {
		super(uuid, timestamp);
		this.stripe = stripe;
	}

	@Override
	public Type getType() {
		return Type.REQUEST_CHILD;
	}

	public int getStripe() {
		return this.stripe;
	}
}
