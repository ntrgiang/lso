package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

public class GetParents extends MaintenanceMessage {

	public GetParents(UUID uuid, Date timestamp) {
		super(uuid, timestamp);
	}

	@Override
	public Type getType() {
		return Type.GET_PARENTS;
	}
}
