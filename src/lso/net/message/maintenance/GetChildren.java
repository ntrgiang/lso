package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

public class GetChildren extends MaintenanceMessage {

	public GetChildren(UUID uuid, Date timestamp) {
		super(uuid, timestamp);
	}

	@Override
	public Type getType() {
		return Type.GET_CHILDREN;
	}
}
