package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

public class GetInfo extends MaintenanceMessage {

	public GetInfo() {
		super();
	}

	public GetInfo(UUID uuid, Date timestamp) {
		this.uuid = uuid;
		this.timestamp = timestamp;
	}

	@Override
	public Type getType() {
		return Type.GET_INFO;
	}
}
