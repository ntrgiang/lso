package lso.net.message.maintenance;

import java.sql.Date;
import java.util.UUID;

public class Term extends MaintenanceMessage {

	public Term(UUID uuid, Date timestamp) {
		super(uuid, timestamp);
	}

	@Override
	public Type getType() {
		return Type.TERM;
	}

}
