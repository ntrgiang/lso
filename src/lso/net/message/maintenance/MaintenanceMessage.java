package lso.net.message.maintenance;

import java.util.Date;
import java.util.UUID;

public abstract class MaintenanceMessage {

	protected UUID uuid;

	protected Date timestamp;

	public abstract Type getType();

	public UUID getUuid() {
		return this.uuid;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public MaintenanceMessage() {
		this.uuid = UUID.randomUUID();
		this.timestamp = new Date();
	}

	public MaintenanceMessage(UUID uuid, Date timestamp) {
		this.uuid = uuid;
		this.timestamp = timestamp;
	}
}
