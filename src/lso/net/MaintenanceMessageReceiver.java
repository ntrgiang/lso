package lso.net;

import java.io.IOException;

public interface MaintenanceMessageReceiver {
	void listen() throws IOException;

	void quit();
}
