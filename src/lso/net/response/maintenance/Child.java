package lso.net.response.maintenance;

import lso.ChildAddress;
import lso.net.response.ReturnCode;
import lso.net.response.Type;

public class Child extends MaintenanceResponse {

	protected ChildAddress childAddress;

	public Child(ReturnCode returnCode, ChildAddress childAddress) {
		super(returnCode);
		this.childAddress = childAddress;
	}

	@Override
	public Type getType() {
		return Type.CHILD;
	}

	public ChildAddress getChildAddress() {
		return this.childAddress;
	}
}
