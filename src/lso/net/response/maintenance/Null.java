package lso.net.response.maintenance;

import lso.net.response.ReturnCode;
import lso.net.response.Type;

public class Null extends MaintenanceResponse {

	public Null(ReturnCode returnCode) {
		super(returnCode);
	}

	@Override
	public Type getType() {
		return Type.NULL;
	}
}
