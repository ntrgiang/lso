package lso.net.response.maintenance;

import lso.Parent;
import lso.net.response.ReturnCode;
import lso.net.response.Type;

public class Parents extends MaintenanceResponse {

	protected Parent[] parents;

	public Parents(ReturnCode returnCode, Parent[] parents) {
		super(returnCode);
		this.parents = parents;
	}

	@Override
	public Type getType() {
		return Type.PARENTS;
	}

	public Parent[] getParents() {
		return this.parents;
	}
}
