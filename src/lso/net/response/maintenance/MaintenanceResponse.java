package lso.net.response.maintenance;

import lso.net.response.ReturnCode;
import lso.net.response.Type;

public abstract class MaintenanceResponse {

	protected ReturnCode returnCode;

	public MaintenanceResponse(ReturnCode returnCode) {
		this.returnCode = returnCode;
	}

	public abstract Type getType();

	public ReturnCode getReturnCode() {
		return this.returnCode;
	}
}
