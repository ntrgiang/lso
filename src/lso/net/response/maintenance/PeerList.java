package lso.net.response.maintenance;

import lso.Address;
import lso.net.response.ReturnCode;
import lso.net.response.Type;

public class PeerList extends MaintenanceResponse {

	protected Address[] peers;

	public PeerList(ReturnCode returnCode, Address[] peers) {
		super(returnCode);
		this.peers = peers;
	}

	@Override
	public Type getType() {
		return Type.PEER_LIST;
	}

	public Address[] getPeers() {
		return this.peers;
	}
}
