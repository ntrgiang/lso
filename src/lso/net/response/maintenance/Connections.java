package lso.net.response.maintenance;

import lso.Child;
import lso.Parent;
import lso.net.response.ReturnCode;
import lso.net.response.Type;

public class Connections extends MaintenanceResponse {

	protected Parent[] parent;

	protected Child[] child;

	protected int[] heights;

	public Connections(ReturnCode returnCode, Parent[] parent, Child[] child,
			int[] heights) {
		super(returnCode);
		this.parent = parent;
		this.child = child;
		this.heights = heights;
	}

	@Override
	public Type getType() {
		return Type.CONNECTIONS;
	}

	public Parent[] getParent() {
		return parent;
	}

	public Child[] getChild() {
		return child;
	}

	public int[] getHeights() {
		return this.heights;
	}

}
