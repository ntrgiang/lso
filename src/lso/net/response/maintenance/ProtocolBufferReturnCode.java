package lso.net.response.maintenance;

import lso.net.message.ResponseProtocolBuffers.Response.ReturnCode;

public class ProtocolBufferReturnCode {
	public static ReturnCode fromLsoReturnCode(lso.net.response.ReturnCode rc) {
		lso.net.message.ResponseProtocolBuffers.Response.ReturnCode returnCode = lso.net.message.ResponseProtocolBuffers.Response.ReturnCode.OK;
		switch (rc) {
		case OK:
			returnCode = lso.net.message.ResponseProtocolBuffers.Response.ReturnCode.OK;
			break;

		case UNKNOWN_MESSAGE_TYPE_ERROR:
			returnCode = lso.net.message.ResponseProtocolBuffers.Response.ReturnCode.UNKNONW_MESSAGE_TYPE_ERROR;
			break;

		case NOT_FOUND:
			returnCode = lso.net.message.ResponseProtocolBuffers.Response.ReturnCode.NOT_FOUND;
			break;
		}
		return returnCode;
	}
}
