package lso.net.response.maintenance;

import lso.MStream;
import lso.net.response.ReturnCode;
import lso.net.response.Type;

public class MStreams extends MaintenanceResponse {

	protected MStream[] mstreams;

	public MStreams(ReturnCode returnCode, MStream[] mstreams) {
		super(returnCode);
		this.mstreams = mstreams;
	}

	@Override
	public Type getType() {
		return Type.MSTREAMS;
	}

	public MStream[] getMStreams() {
		return this.mstreams;
	}
}
