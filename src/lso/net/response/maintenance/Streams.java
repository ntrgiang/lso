package lso.net.response.maintenance;

import lso.Stream;
import lso.net.response.ReturnCode;
import lso.net.response.Type;

public class Streams extends MaintenanceResponse {

	protected Stream[] streams;

	public Streams(ReturnCode returnCode, Stream[] streams) {
		super(returnCode);
		this.streams = streams;
	}

	@Override
	public Type getType() {
		return Type.STREAMS;
	}

	public Stream[] getStreams() {
		return this.streams;
	}
}
