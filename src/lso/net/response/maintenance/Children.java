package lso.net.response.maintenance;

import lso.Child;
import lso.net.response.ReturnCode;
import lso.net.response.Type;

public class Children extends MaintenanceResponse {
	protected int stripe;
	protected Child[] children;

	public Children(ReturnCode returnCode, Child[] children) {
		super(returnCode);
		this.children = children;
	}

	@Override
	public Type getType() {
		return Type.CHILDREN;
	}

	public int getStripe() {
		return this.stripe;
	}

	public Child[] getChildren() {
		return this.children;
	}
}
