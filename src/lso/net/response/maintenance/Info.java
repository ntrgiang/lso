package lso.net.response.maintenance;

import java.util.Map;

import lso.net.response.ReturnCode;
import lso.net.response.Type;

public class Info extends MaintenanceResponse {

	protected Map<String, String> properties;

	public Info(ReturnCode returnCode, Map<String, String> properties) {
		super(returnCode);
		this.properties = properties;
	}

	@Override
	public Type getType() {
		return Type.INFO;
	}

	public Map<String, String> getProperties() {
		return this.properties;
	}
}
