package lso.net.response.maintenance;

import java.util.HashMap;
import java.util.List;

import lso.Address;
import lso.Parent;
import lso.net.message.CommonProtocolBuffers;
import lso.net.message.CommonProtocolBuffers.MStream;
import lso.net.message.CommonProtocolBuffers.Stream;
import lso.net.message.NotFoundException;
import lso.net.message.ResponseProtocolBuffers.Response;
import lso.net.message.UnknownMessageException;

public class MaintenanceResponseFactory {

	public static MaintenanceResponse fromResponse(Response response)
			throws UnknownMessageException, NotFoundException {
		switch (response.getReturnCode()) {
		case NOT_FOUND:
			throw new NotFoundException();
		case UNKNONW_MESSAGE_TYPE_ERROR:
			throw new UnknownMessageException();
		}

		MaintenanceResponse result = null;

		int i;

		switch (response.getType()) {
		case NULL:
			result = new Null(lso.net.response.ReturnCode.OK);
			break;

		case PEER_LIST:
			List<CommonProtocolBuffers.Address> peerList = response
					.getPeerList().getMaintenanceAddressList();
			Address[] peers = new Address[peerList.size()];

			i = 0;
			for (CommonProtocolBuffers.Address a : peerList) {
				Address address = new Address(a.getHostname(), a.getPort());
				peers[i] = address;
				i++;
			}

			result = new PeerList(lso.net.response.ReturnCode.OK, peers);
			break;

		case STREAMS:
			List<Stream> streamList = response.getStreams().getStreamList();
			lso.Stream[] streams = new lso.Stream[streamList.size()];

			i = 0;
			for (CommonProtocolBuffers.Stream s : streamList) {
				// FIXME setting the tracker maintenance address to null isn't
				// good
				streams[i] = new lso.Stream(s.getStreamIdentifier(),
						s.getStripes(), s.getVideoRate(),
						s.getStreamDescriptor(), null, new Address(s
								.getSource().getHostname(), s.getSource()
								.getPort()));
				i++;
			}

			result = new Streams(lso.net.response.ReturnCode.OK, streams);
			break;

		case MSTREAMS:
			List<MStream> mstreamList = response.getMstreams().getMstreamList();
			lso.MStream[] mstreams = new lso.MStream[mstreamList.size()];

			i = 0;
			for (CommonProtocolBuffers.MStream s : mstreamList) {
				// FIXME setting the tracker maintenance address to null isn't
				// good
				mstreams[i] = new lso.MStream(s.getStreamIdentifier(),
						s.getStripes(), s.getVideoRate(), null, new Address(s
								.getSource().getHostname(), s.getSource()
								.getPort()));
				i++;
			}

			result = new MStreams(lso.net.response.ReturnCode.OK, mstreams);
			break;

		case PARENTS:
			List<CommonProtocolBuffers.Parent> parentList = response
					.getParents().getParentList();
			Parent[] parents = new Parent[parentList.size()];

			i = 0;
			for (CommonProtocolBuffers.Parent p : parentList) {
				Parent parent = new Parent(p.getStripe(), new Address(p
						.getAddress().getHostname(), p.getAddress().getPort()));
				parents[i] = parent;
				i++;
			}

			result = new Parents(lso.net.response.ReturnCode.OK, parents);
			break;

		case CHILDREN:
			List<CommonProtocolBuffers.CommonChild> childList = response
					.getChildren().getChildList();
			lso.Child[] children = new lso.Child[childList.size()];
			i = 0;
			for (CommonProtocolBuffers.CommonChild c : childList) {
				Address ma = new Address(c.getAddress().getMaintenanceAddress()
						.getHostname(), c.getAddress().getMaintenanceAddress()
						.getPort());
				Address da = new Address(c.getAddress().getDataAddress()
						.getHostname(), c.getAddress().getDataAddress()
						.getPort());

				lso.Child child = new lso.Child(c.getStripe(),
						new lso.ChildAddress(ma, da));
				children[i++] = child;
			}

			result = new Children(lso.net.response.ReturnCode.OK, children);

			break;

		case CHILD:
			CommonProtocolBuffers.ChildAddress c = response.getChild()
					.getChildAddress();

			Address ma = new Address(c.getMaintenanceAddress().getHostname(), c
					.getMaintenanceAddress().getPort());
			Address da = new Address(c.getDataAddress().getHostname(), c
					.getDataAddress().getPort());

			lso.ChildAddress ca = new lso.ChildAddress(ma, da);

			result = new Child(lso.net.response.ReturnCode.OK, ca);
			break;

		case INFO:
			List<CommonProtocolBuffers.Property> propertyList = response
					.getInfo().getPropertyList();

			HashMap<String, String> properties = new HashMap<String, String>();
			for (CommonProtocolBuffers.Property p : propertyList) {
				properties.put(p.getKey(), p.getValue());
			}

			result = new Info(lso.net.response.ReturnCode.OK, properties);
			break;

		case CONNECTIONS:
			List<CommonProtocolBuffers.Parent> pList = response
					.getConnections().getParentList();
			List<CommonProtocolBuffers.CommonChild> cList = response
					.getConnections().getChildList();
			List<Integer> hList = response.getConnections().getHeightList();

			lso.Parent[] p = new lso.Parent[pList.size()];
			int index = 0;
			for (CommonProtocolBuffers.Parent parent : pList) {
				Address address = new Address(
						parent.getAddress().getHostname(), parent.getAddress()
								.getPort());
				p[index++] = new lso.Parent(parent.getStripe(), address);
			}

			lso.Child[] C = new lso.Child[cList.size()];
			index = 0;
			for (CommonProtocolBuffers.CommonChild child : cList) {
				ma = new Address(child.getAddress().getMaintenanceAddress()
						.getHostname(), child.getAddress()
						.getMaintenanceAddress().getPort());
				da = new Address(child.getAddress().getDataAddress()
						.getHostname(), child.getAddress().getDataAddress()
						.getPort());
				C[index++] = new lso.Child(child.getStripe(),
						new lso.ChildAddress(ma, da));
			}

			int[] heights = new int[hList.size()];
			index = 0;
			for (int height : hList) {
				heights[index++] = height;
			}

			result = new Connections(lso.net.response.ReturnCode.OK, p, C,
					heights);
			break;

		}
		return result;
	}
}
