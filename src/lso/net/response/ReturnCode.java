package lso.net.response;

public enum ReturnCode {
	NOT_FOUND, UNKNOWN_MESSAGE_TYPE_ERROR, OK, CANNOT_CONNECT
}
