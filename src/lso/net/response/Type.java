package lso.net.response;

public enum Type {
	NULL, PEER_LIST, CHILD, STREAMS, MSTREAMS, PARENTS, CHILDREN, INFO, CONNECTIONS
}
