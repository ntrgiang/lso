package lso.net.response;

import lso.Address;
import lso.MStream;
import lso.Parent;
import lso.Stream;
import lso.net.message.CommonProtocolBuffers;
import lso.net.message.CommonProtocolBuffers.ChildAddress;
import lso.net.message.CommonProtocolBuffers.Property;
import lso.net.message.ResponseProtocolBuffers.Response;
import lso.net.message.ResponseProtocolBuffers.Response.Type;
import lso.net.response.maintenance.Child;
import lso.net.response.maintenance.Children;
import lso.net.response.maintenance.Connections;
import lso.net.response.maintenance.Info;
import lso.net.response.maintenance.MStreams;
import lso.net.response.maintenance.MaintenanceResponse;
import lso.net.response.maintenance.Null;
import lso.net.response.maintenance.Parents;
import lso.net.response.maintenance.PeerList;
import lso.net.response.maintenance.ProtocolBufferReturnCode;
import lso.net.response.maintenance.Streams;

public class ProtocolBufferResponseFactory {
	public static Response fromMaintenanceResponse(
			MaintenanceResponse maintenanceResponse) {
		switch (maintenanceResponse.getType()) {
		case NULL:
			return fromNull((Null) maintenanceResponse);

		case PEER_LIST:
			return fromPeerList((PeerList) maintenanceResponse);

		case CHILD:
			return fromChild((Child) maintenanceResponse);

		case STREAMS:
			return fromStreams((Streams) maintenanceResponse);

		case MSTREAMS:
			return fromMStreams((MStreams) maintenanceResponse);

		case PARENTS:
			return fromParents((Parents) maintenanceResponse);

		case CHILDREN:
			return fromChildren((Children) maintenanceResponse);

		case INFO:
			return fromInfo((Info) maintenanceResponse);

		case CONNECTIONS:
			return fromConnections((Connections) maintenanceResponse);
		}

		return null;
	}

	private static Response fromChild(Child child) {
		lso.net.message.CommonProtocolBuffers.Address maintenanceAddress = lso.net.message.CommonProtocolBuffers.Address
				.newBuilder()
				.setHostname(
						child.getChildAddress().getMaintenance().getHostname())
				.setPort(child.getChildAddress().getMaintenance().getPort())
				.build();
		lso.net.message.CommonProtocolBuffers.Address dataAddress = lso.net.message.CommonProtocolBuffers.Address
				.newBuilder()
				.setHostname(child.getChildAddress().getData().getHostname())
				.setPort(child.getChildAddress().getData().getPort()).build();

		ChildAddress ca = ChildAddress.newBuilder()
				.setMaintenanceAddress(maintenanceAddress)
				.setDataAddress(dataAddress).build();

		Response.Child c = Response.Child.newBuilder().setChildAddress(ca)
				.build();

		return Response
				.newBuilder()
				.setReturnCode(
						ProtocolBufferReturnCode.fromLsoReturnCode(child
								.getReturnCode())).setType(Type.CHILD)
				.setChild(c).build();
	}

	private static Response fromChildren(Children children) {
		Response.Children.Builder cb = Response.Children.newBuilder();

		lso.Child[] childrenList = children.getChildren();

		for (lso.Child c : childrenList) {
			lso.ChildAddress ca = c.getChildAddress();
			lso.net.message.CommonProtocolBuffers.Address maintenanceAddress = lso.net.message.CommonProtocolBuffers.Address
					.newBuilder()
					.setHostname(ca.getMaintenance().getHostname())
					.setPort(ca.getMaintenance().getPort()).build();
			lso.net.message.CommonProtocolBuffers.Address dataAddress = lso.net.message.CommonProtocolBuffers.Address
					.newBuilder().setHostname(ca.getData().getHostname())
					.setPort(ca.getData().getPort()).build();

			ChildAddress pbca = ChildAddress.newBuilder()
					.setMaintenanceAddress(maintenanceAddress)
					.setDataAddress(dataAddress).build();

			CommonProtocolBuffers.CommonChild cc = CommonProtocolBuffers.CommonChild
					.newBuilder().setStripe(c.getStripe()).setAddress(pbca)
					.build();

			cb.addChild(cc);
		}

		return Response
				.newBuilder()
				.setReturnCode(
						ProtocolBufferReturnCode.fromLsoReturnCode(children
								.getReturnCode())).setType(Type.CHILDREN)
				.setChildren(cb.build()).build();
	}

	private static Response fromInfo(Info info) {
		Response.Info.Builder gimb = Response.Info.newBuilder();

		for (String key : info.getProperties().keySet()) {
			String value = info.getProperties().get(key);
			gimb.addProperty(Property.newBuilder().setKey(key).setValue(value)
					.build());
		}

		return Response
				.newBuilder()
				.setReturnCode(
						ProtocolBufferReturnCode.fromLsoReturnCode(info
								.getReturnCode())).setType(Type.INFO)
				.setInfo(gimb.build()).build();
	}

	private static Response fromNull(Null n) {
		return Response
				.newBuilder()
				.setReturnCode(
						ProtocolBufferReturnCode.fromLsoReturnCode(n
								.getReturnCode())).setType(Type.NULL)
				.setNull(Response.Null.newBuilder().build()).build();
	}

	private static Response fromParents(Parents parents) {
		Response.Parents.Builder gpmb = Response.Parents.newBuilder();
		for (Parent p : parents.getParents()) {
			CommonProtocolBuffers.Address address = CommonProtocolBuffers.Address
					.newBuilder().setHostname(p.getAddress().getHostname())
					.setPort(p.getAddress().getPort()).build();

			CommonProtocolBuffers.Parent.Builder parentBuilder = CommonProtocolBuffers.Parent
					.newBuilder();
			gpmb.addParent(parentBuilder.setStripe(p.getStripe())
					.setAddress(address).build());
		}

		return Response
				.newBuilder()
				.setReturnCode(
						ProtocolBufferReturnCode.fromLsoReturnCode(parents
								.getReturnCode())).setType(Type.PARENTS)
				.setParents(gpmb.build()).build();
	}

	private static Response fromPeerList(PeerList peerList) {
		Response.PeerList.Builder gplmb = Response.PeerList.newBuilder();
		for (Address a : peerList.getPeers()) {
			lso.net.message.CommonProtocolBuffers.Address address = lso.net.message.CommonProtocolBuffers.Address
					.newBuilder().setHostname(a.getHostname())
					.setPort(a.getPort()).build();
			gplmb.addMaintenanceAddress(address);
		}

		return Response
				.newBuilder()
				.setReturnCode(
						ProtocolBufferReturnCode.fromLsoReturnCode(peerList
								.getReturnCode())).setType(Type.PEER_LIST)
				.setPeerList(gplmb.build()).build();
	}

	private static Response fromStreams(Streams streams) {
		Response.Streams.Builder smb = Response.Streams.newBuilder();
		for (Stream s : streams.getStreams()) {
			lso.net.message.CommonProtocolBuffers.Address a = lso.net.message.CommonProtocolBuffers.Address
					.newBuilder().setHostname(s.getSource().getHostname())
					.setPort(s.getSource().getPort()).build();
			smb.addStream(CommonProtocolBuffers.Stream.newBuilder()
					.setStreamIdentifier(s.getIdentifier())
					.setStripes(s.getStripes()).setVideoRate(s.getVideoRate())
					.setStreamDescriptor(s.getStreamDescriptor()).setSource(a)
					.build());
		}

		return Response
				.newBuilder()
				.setReturnCode(
						ProtocolBufferReturnCode.fromLsoReturnCode(streams
								.getReturnCode())).setType(Type.STREAMS)
				.setStreams(smb.build()).build();
	}

	private static Response fromMStreams(MStreams mstreams) {
		Response.MStreams.Builder msmb = Response.MStreams.newBuilder();
		for (MStream s : mstreams.getMStreams()) {
			lso.net.message.CommonProtocolBuffers.Address a = lso.net.message.CommonProtocolBuffers.Address
					.newBuilder().setHostname(s.getSource().getHostname())
					.setPort(s.getSource().getPort()).build();
			msmb.addMstream(CommonProtocolBuffers.MStream.newBuilder()
					.setStreamIdentifier(s.getIdentifier())
					.setStripes(s.getStripes()).setVideoRate(s.getVideoRate())
					.setSource(a).build());
		}

		return Response
				.newBuilder()
				.setReturnCode(
						ProtocolBufferReturnCode.fromLsoReturnCode(mstreams
								.getReturnCode())).setType(Type.MSTREAMS)
				.setMstreams(msmb.build()).build();
	}

	private static Response fromConnections(Connections connections) {
		Response.Connections.Builder cmb = Response.Connections.newBuilder();

		for (lso.Child c : connections.getChild()) {
			lso.ChildAddress ca = c.getChildAddress();
			lso.net.message.CommonProtocolBuffers.Address maintenanceAddress = lso.net.message.CommonProtocolBuffers.Address
					.newBuilder()
					.setHostname(ca.getMaintenance().getHostname())
					.setPort(ca.getMaintenance().getPort()).build();
			lso.net.message.CommonProtocolBuffers.Address dataAddress = lso.net.message.CommonProtocolBuffers.Address
					.newBuilder().setHostname(ca.getData().getHostname())
					.setPort(ca.getData().getPort()).build();

			ChildAddress pbca = ChildAddress.newBuilder()
					.setMaintenanceAddress(maintenanceAddress)
					.setDataAddress(dataAddress).build();

			CommonProtocolBuffers.CommonChild cc = CommonProtocolBuffers.CommonChild
					.newBuilder().setStripe(c.getStripe()).setAddress(pbca)
					.build();

			cmb.addChild(cc);
		}

		for (Parent p : connections.getParent()) {
			CommonProtocolBuffers.Address address = CommonProtocolBuffers.Address
					.newBuilder().setHostname(p.getAddress().getHostname())
					.setPort(p.getAddress().getPort()).build();

			CommonProtocolBuffers.Parent.Builder parentBuilder = CommonProtocolBuffers.Parent
					.newBuilder();
			cmb.addParent(parentBuilder.setStripe(p.getStripe())
					.setAddress(address).build());
		}

		for (int height : connections.getHeights()) {
			cmb.addHeight(height);
		}

		return Response
				.newBuilder()
				.setReturnCode(
						ProtocolBufferReturnCode.fromLsoReturnCode(connections
								.getReturnCode())).setType(Type.CONNECTIONS)
				.setConnections(cmb.build()).build();
	}
}
