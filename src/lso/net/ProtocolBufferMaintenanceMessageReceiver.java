package lso.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import lso.Address;
import lso.log.Logger;
import lso.net.message.MessageProtocolBuffers.Message;
import lso.net.message.ResponseProtocolBuffers.Response;
import lso.net.message.maintenance.MaintenanceMessage;
import lso.net.message.maintenance.MaintenanceMessageFactory;
import lso.net.message.maintenance.MaintenanceMessageHandler;
import lso.net.response.ProtocolBufferResponseFactory;
import lso.net.response.maintenance.MaintenanceResponse;

public class ProtocolBufferMaintenanceMessageReceiver implements
		MaintenanceMessageReceiver {

	int BACKLOG = 23;

	Logger logger;

	ServerSocket serverSocket;

	MaintenanceMessageHandler messageHandler;

	public ProtocolBufferMaintenanceMessageReceiver(Address address,
			MaintenanceMessageHandler messageHandler) throws IOException {
		System.out.println("Binding to " + address + "...");
		this.serverSocket = this.createServerSocket(address);
		this.messageHandler = messageHandler;
	}

	private MaintenanceMessage receive(InputStream inputStream)
			throws IOException {
		Message pb = Message.parseDelimitedFrom(inputStream);

		return MaintenanceMessageFactory.fromMessage(pb);
	}

	private void respond(MaintenanceResponse maintenanceResponse,
			OutputStream outputStream) throws IOException {
		Response pbResponse = ProtocolBufferResponseFactory
				.fromMaintenanceResponse(maintenanceResponse);

		pbResponse.writeDelimitedTo(outputStream);
	}

	@Override
	public void listen() throws IOException {
		try {
			while (true) {
				Socket socket = this.serverSocket.accept();

				InputStream inputStream = socket.getInputStream();
				OutputStream outputStream = socket.getOutputStream();

				MaintenanceMessage maintenanceMessage = this
						.receive(inputStream);

				MaintenanceResponse maintenanceResponse = this.messageHandler
						.handleMaintenanceMessage(maintenanceMessage);

				this.respond(maintenanceResponse, outputStream);

				inputStream.close();
				outputStream.close();
				socket.close();
			}
		} catch (IOException e) {
		}
	}

	@Override
	public void quit() {
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected ServerSocket createServerSocket(Address address)
			throws UnknownHostException, IOException {
		InetAddress inetAddress = InetAddress.getByName(address.getHostname());
		return new ServerSocket(address.getPort(), BACKLOG, inetAddress);
	}

}
