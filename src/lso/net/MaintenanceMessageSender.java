package lso.net;

import java.io.IOException;

import lso.Address;
import lso.net.message.NotFoundException;
import lso.net.message.UnknownMessageException;
import lso.net.message.maintenance.MaintenanceMessage;
import lso.net.response.maintenance.MaintenanceResponse;

public interface MaintenanceMessageSender {

	MaintenanceResponse send(Address address, MaintenanceMessage message)
			throws IOException, IllegalArgumentException,
			UnknownMessageException, NotFoundException;
}
