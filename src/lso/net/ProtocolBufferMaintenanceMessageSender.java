package lso.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import lso.Address;
import lso.net.message.MessageProtocolBuffers.Message;
import lso.net.message.NotFoundException;
import lso.net.message.ProtocolBufferMessageFactory;
import lso.net.message.ResponseProtocolBuffers.Response;
import lso.net.message.UnknownMessageException;
import lso.net.message.maintenance.MaintenanceMessage;
import lso.net.response.maintenance.MaintenanceResponse;
import lso.net.response.maintenance.MaintenanceResponseFactory;

public class ProtocolBufferMaintenanceMessageSender implements
		MaintenanceMessageSender {

	@Override
	public MaintenanceResponse send(Address address,
			MaintenanceMessage maintenanceMessage)
			throws IllegalArgumentException, IOException,
			UnknownMessageException, NotFoundException {
		Message message = ProtocolBufferMessageFactory
				.fromMaintenanceMessage(maintenanceMessage);

		Socket socket = new Socket(address.getHostname(), address.getPort());

		OutputStream outputStream = socket.getOutputStream();
		InputStream inputStream = socket.getInputStream();

		message.writeDelimitedTo(outputStream);

		Response response = Response.parseDelimitedFrom(inputStream);

		MaintenanceResponse maintenanceResponse = MaintenanceResponseFactory
				.fromResponse(response);

		outputStream.close();
		inputStream.close();
		socket.close();

		return maintenanceResponse;
	}

}
