package lso;

import java.util.HashSet;
import java.util.Set;

public class Stream {
	private String identifier;

	private int stripes;

	private Address tracker;

	private Set<Address> peers;

	private Address source;

	private int videoRate;

	private String streamDescriptor;

	public Address getTrackerMaintenanceAddress() {
		return this.tracker;
	}

	public String getIdentifier() {
		return this.identifier;
	}

	public int getStripes() {
		return this.stripes;
	}

	public Stream(String streamIdentifier, int stripes, int videoRate,
			String streamDescriptor, Address trackerAddress, Address source) {
		this.identifier = streamIdentifier;
		this.stripes = stripes;
		this.videoRate = videoRate;
		this.streamDescriptor = streamDescriptor;
		this.tracker = trackerAddress;
		this.source = source;
		this.peers = new HashSet<Address>();
	}

	public String toString() {
		return "'" + this.identifier + "' (" + this.stripes + ") "
				+ this.videoRate + " kb/s";
	}

	public void setStripes(int stripes) {
		this.stripes = stripes;
	}

	public void setVideoRate(int videoRate) {
		this.videoRate = videoRate;
	}

	public void setSource(Address source) {
		this.source = source;
	}

	/**
	 * @return the tracker
	 */
	public Address getTracker() {
		return tracker;
	}

	/**
	 * @return the peers
	 */
	public Set<Address> getPeers() {
		return peers;
	}

	/**
	 * @return the videoRate
	 */
	public int getVideoRate() {
		return videoRate;
	}

	public int getVideoRatePerStripe() {
		return (int) Math.ceil((double) this.videoRate / (double) this.stripes);
	}

	/**
	 * @return the source
	 */
	public Address getSource() {
		return source;
	}

	/**
	 * @return the streamDescriptor
	 */
	public String getStreamDescriptor() {
		return streamDescriptor;
	}

	/**
	 * @param streamDescriptor the streamDescriptor to set
	 */
	public void setStreamDescriptor(String streamDescriptor) {
		this.streamDescriptor = streamDescriptor;
	}
}
