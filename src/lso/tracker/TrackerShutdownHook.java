package lso.tracker;

public class TrackerShutdownHook extends Thread {
	@Override
	public void run() {
		System.err.println("Shutting down...");
	}
}
