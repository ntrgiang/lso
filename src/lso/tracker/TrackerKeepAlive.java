package lso.tracker;

import java.sql.Date;

import lso.Address;
import lso.MaintenanceNode;
import lso.Stream;

public class TrackerKeepAlive extends Thread {
	private MaintenanceNode maintenanceNode;

	private Tracker tracker;

	private long wait;

	private long sourceThreshold;
	private long peerThreshold;

	public TrackerKeepAlive(MaintenanceNode maintenanceNode, Tracker tracker,
			long wait, long sourceThreshold, long peerThreshold) {
		this.maintenanceNode = maintenanceNode;
		this.tracker = tracker;
		this.wait = wait;
		this.peerThreshold = peerThreshold;
		this.sourceThreshold = sourceThreshold;
	}

	@Override
	public void run() {
		try {
			while (true) {
				Stream[] streams = this.tracker.getStreams();
				for (Stream stream : streams) {
					String sid = stream.getIdentifier();
					long lastSeen, current, diff;

					Address source = this.tracker.getSourceForStream(sid);
					Date date = this.tracker.getLastSeenSource(source);
					if (date != null) {
						lastSeen = date.getTime();
					} else {
						System.out.println("Source " + source + " for " + sid
								+ " has never been seen!");
						lastSeen = 0;
					}
					current = System.currentTimeMillis();
					diff = current - lastSeen;
					if (diff > this.sourceThreshold) {
						System.out.println("Source '" + source + "' for " + sid
								+ " has not been seen for " + diff + " ms");
						this.tracker.removeStream(sid, source);
						continue;
					}

					Address[] peers = this.tracker.getAllPeersForStream(sid);
					for (Address peer : peers) {
						date = this.tracker.getLastSeenPeer(peer);
						if (date == null) {
							System.out.println("Peer " + peer + " in " + sid
									+ " has never been seen!");
							lastSeen = 0;
						} else {
							lastSeen = date.getTime();
						}
						current = System.currentTimeMillis();
						diff = current - lastSeen;
						if (diff > this.peerThreshold) {
							System.out.println("Peer '" + peer + "' in " + sid
									+ " has not been seen for " + diff + " ms");
							this.tracker.removePeer(peer);
						}
					}
				}
				Thread.sleep(this.wait);
			}
		} catch (InterruptedException e) {
			System.out.println("terminating KeepAlive");
		}
	}
}
