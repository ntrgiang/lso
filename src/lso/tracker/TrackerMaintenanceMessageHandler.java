package lso.tracker;

import java.io.IOException;

import lso.Address;
import lso.Common;
import lso.Location;
import lso.MaintenanceNode;
import lso.Stream;
import lso.net.message.NotFoundException;
import lso.net.message.UnknownMessageException;
import lso.net.message.maintenance.Alive;
import lso.net.message.maintenance.ChangeLocation;
import lso.net.message.maintenance.ChangeParent;
import lso.net.message.maintenance.Connect;
import lso.net.message.maintenance.ForwardChild;
import lso.net.message.maintenance.GetChildren;
import lso.net.message.maintenance.GetInfo;
import lso.net.message.maintenance.GetParents;
import lso.net.message.maintenance.GetPeerList;
import lso.net.message.maintenance.GetStreams;
import lso.net.message.maintenance.Kill;
import lso.net.message.maintenance.Leave;
import lso.net.message.maintenance.LeaveStripe;
import lso.net.message.maintenance.MGetAllPeers;
import lso.net.message.maintenance.MGetAllPeersForStream;
import lso.net.message.maintenance.MGetStreams;
import lso.net.message.maintenance.MaintenanceMessage;
import lso.net.message.maintenance.MaintenanceMessageHandler;
import lso.net.message.maintenance.PAlive;
import lso.net.message.maintenance.RegisterStream;
import lso.net.message.maintenance.RequestChild;
import lso.net.message.maintenance.SAlive;
import lso.net.message.maintenance.SetProperties;
import lso.net.message.maintenance.Term;
import lso.net.message.maintenance.UnregisterStream;
import lso.net.message.maintenance.WhichConnections;
import lso.net.response.ReturnCode;
import lso.net.response.maintenance.MStreams;
import lso.net.response.maintenance.MaintenanceResponse;
import lso.net.response.maintenance.Null;
import lso.net.response.maintenance.PeerList;
import lso.net.response.maintenance.Streams;

public class TrackerMaintenanceMessageHandler extends MaintenanceMessageHandler {

	private Tracker tracker;

	private MaintenanceNode maintenanceNode;

	public TrackerMaintenanceMessageHandler(Tracker tracker,
			MaintenanceNode maintenanceNode) {
		this.tracker = tracker;
		this.maintenanceNode = maintenanceNode;
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// source / peer
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleAlive(Alive alive) {
		MaintenanceResponse response = new Null(ReturnCode.NOT_FOUND);

		Address address = alive.getMaintenanceAddress();

		if (this.tracker.knowsSource(address)) {
			this.tracker.seeSource(address);
			response = new Null(ReturnCode.OK);
		} else if (this.tracker.knowsPeer(address)) {
			this.tracker.seePeer(address);
			response = new Null(ReturnCode.OK);
		}
		return response;
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// source
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleRegisterStream(
			RegisterStream registerStream) {
		String sid = registerStream.getStreamIdentifier();
		Address source = registerStream.getMaintenanceAddress();
		int stripes = registerStream.getStripes();
		int videoRate = registerStream.getVideoRate();
		String streamDescriptor = registerStream.getStreamDescriptor();

		Stream stream = new Stream(sid, stripes, videoRate, streamDescriptor,
				this.tracker.getMaintenanceAddress(), source);

		System.out.println("RegisterStream received for stream '" + sid + "'");

		this.tracker.addStream(stream, source);

		return new Null(ReturnCode.OK);
	}

	@Override
	protected MaintenanceResponse handleUnregisterStream(
			UnregisterStream unregisterStream) {
		MaintenanceResponse response = new Null(ReturnCode.OK);

		String sid = unregisterStream.getStreamIdentifier();
		Address source = unregisterStream.getMaintenanceAddress();

		System.out
				.println("UnregisterStream received for stream '" + sid + "'");

		if (this.tracker.hasSource(sid, source)) {
			this.tracker.removeStream(sid, source);
		} else {
			response = new Null(ReturnCode.NOT_FOUND);
		}

		return response;
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// peer
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleGetStreams(GetStreams getStreams) {
		System.out.println("GetStreams received");

		Stream[] streams = this.tracker.getStreams();

		return new Streams(ReturnCode.OK, streams);
	}

	@Override
	protected MaintenanceResponse handleGetPeerList(GetPeerList getPeerList) {
		MaintenanceResponse response = new PeerList(ReturnCode.NOT_FOUND,
				new Address[0]);

		Address address = getPeerList.getMaintenanceAddress();
		String sid = getPeerList.getStreamIdentifier();
		Location location = new Location(getPeerList.getLocation());

		this.tracker.changeLocation(sid, address, location);

		// this.checkPeersAlive(sid);

		System.out.println("PeerListRequest received from " + address
				+ " for stream '" + sid + "'");

		if (this.tracker.hasStream(sid)) {
			Address[] peers = this.tracker.getPeersForStream(sid, address,
					Common.TRACKER_MAXIMUM_PEER_LIST_SIZE);

			response = new PeerList(ReturnCode.OK, peers);

			if (!this.tracker.knowsPeer(address)) {
				this.tracker.addPeerToStream(sid, address);
			}
		} else {
			response = new PeerList(ReturnCode.NOT_FOUND, new Address[0]);

			System.err.println("Couldn't find " + sid);
		}
		return response;
	}

	@Override
	protected MaintenanceResponse handleLeave(Leave leave) {
		Address address = leave.getMaintenanceAddress();

		System.out.println("Leave received from peer '" + address + "'");
		this.tracker.removePeer(address);

		return new Null(ReturnCode.OK);
	}

	@Override
	protected MaintenanceResponse handleChangeLocation(
			ChangeLocation changeLocation) {
		Location location = new Location(changeLocation.getLocation());
		Address peer = changeLocation.getMaintenanceAddress();
		String sid = this.tracker.getStreamOfPeer(peer);
		this.tracker.changeLocation(sid, peer, location);
		return new Null(ReturnCode.OK);
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// monitor
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleMGetAllPeers(MGetAllPeers mGetAllPeers) {
		return new PeerList(ReturnCode.OK, this.tracker.getAllPeers());
	}

	@Override
	protected MaintenanceResponse handleMGetAllPeersForStream(
			MGetAllPeersForStream mGetAllPeersForStream) {
		return new PeerList(ReturnCode.OK, this.tracker.getAllPeersForStream(
				mGetAllPeersForStream.getStreamIdentifier(), null));
	}

	@Override
	protected MaintenanceResponse handleMGetStreams(MGetStreams mGetStreams) {
		return new MStreams(ReturnCode.OK, this.tracker.getMStreams());
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// unknown messages
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleChangeParent(ChangeParent changeParent) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleConnect(Connect connect) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleForwardChild(ForwardChild forwardChild) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleGetInfo(GetInfo getInfo) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleGetParents(GetParents getParents) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handlePAlive(PAlive pAlive) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleRequestChild(RequestChild requestChild) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleSAlive(SAlive sAlive) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleLeaveStripe(LeaveStripe leaveStripe) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleTerm(Term term) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleSetProperties(
			SetProperties setProperties) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleGetChildren(GetChildren getChildren) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleKill(Kill kill) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleWhichConnections(
			WhichConnections whichConnections) {
		return this.createUnknownMessageResponse();
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// HELPER
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	protected void checkPeersAlive(String sid) {
		Address[] peers = this.tracker.getAllPeersForStream(sid);
		for (Address peer : peers) {
			if (!this.isAlive(peer)) {
				this.tracker.removePeer(peer);
			}
		}
	}

	/**
	 * Sends a PAlive message to the peer with the given address to check if it
	 * is still alive or has gone offline without notifying the tracker.
	 * 
	 * @param address
	 *            peer's address to check
	 * @return true, if the peer replies with ReturnCode.OK; false otherwise
	 */
	protected boolean isAlive(Address address) {
		MaintenanceMessage msg = new PAlive(
				this.tracker.getMaintenanceAddress());
		try {
			MaintenanceResponse r = this.maintenanceNode.send(address, msg);
			if (r != null && r.getReturnCode() == ReturnCode.OK) {
				return true;
			}
		} catch (IllegalArgumentException e) {
		} catch (IOException e) {
		} catch (UnknownMessageException e) {
		} catch (NotFoundException e) {
		}
		return false;
	}
}
