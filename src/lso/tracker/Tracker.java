package lso.tracker;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lso.Address;
import lso.Common;
import lso.Location;
import lso.MStream;
import lso.Stream;

public class Tracker {

	private Address maintenanceAddress;

	private Map<String, Stream> streams;
	private Map<String, Address> sources;

	private Map<String, Set<Address>> peerSets;

	private Map<Address, Date> sourceLastSeen;
	private Map<Address, Date> peerLastSeen;

	private Map<Address, Location> peerLocations;
	private Map<String, Map<Location, Set<Address>>> locationMaps;

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// CONSTRUCTOR
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public Tracker(Address maintenanceAddress) throws IOException {
		this.maintenanceAddress = maintenanceAddress;
		this.streams = new HashMap<String, Stream>();
		this.sources = new HashMap<String, Address>();
		this.peerSets = new HashMap<String, Set<Address>>();
		this.sourceLastSeen = new HashMap<Address, Date>();
		this.peerLastSeen = new HashMap<Address, Date>();
		this.peerLocations = new HashMap<Address, Location>();
		this.locationMaps = new HashMap<String, Map<Location, Set<Address>>>();
	}

	public Address getMaintenanceAddress() {
		return this.maintenanceAddress;
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// STREAMS
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public boolean addStream(Stream stream, Address source) {
		if (this.streams.containsKey(stream.getIdentifier())) {
			return false;
		} else {
			String sid = stream.getIdentifier();

			this.streams.put(sid, stream);
			this.sources.put(sid, source);
			this.peerSets.put(sid, new HashSet<Address>());
			this.seeSource(source);

			System.out.println("  => adding stream " + sid);

			return true;
		}
	}

	public boolean removeStream(String sid, Address sourceMaintenanceAddress) {
		for (Address peer : this.peerSets.get(sid)) {
			this.peerLastSeen.remove(peer);
		}
		this.sourceLastSeen.remove(sourceMaintenanceAddress);

		this.streams.remove(sid);
		this.sources.remove(sid);
		this.peerSets.remove(sid);

		System.out.println("  => removing stream " + sid);

		return true;
	}

	public boolean hasStream(String streamIdentifier) {
		return this.streams.containsKey(streamIdentifier);
	}

	public void printStreams() {
		for (String id : this.streams.keySet()) {
			Address a = this.sources.get(id);
			Stream s = this.streams.get(id);
			System.out.println("  - " + s + " @ " + a);
		}
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// SOURCE
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public boolean knowsSource(Address sourceMaintenanceAddress) {
		return this.sources.values().contains(sourceMaintenanceAddress);
	}

	public void seeSource(Address sourceMaintenanceAddress) {
		this.sourceLastSeen.put(sourceMaintenanceAddress,
				new Date(System.currentTimeMillis()));
	}

	public boolean hasSource(String sid, Address source) {
		return this.sources.containsKey(sid)
				&& this.sources.get(sid).equals(source);
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// PEER
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public boolean knowsPeer(Address peerMaintenanceAddress) {
		for (Set<Address> s : this.peerSets.values()) {
			if (s.contains(peerMaintenanceAddress)) {
				return true;
			}
		}
		return false;
	}

	public void seePeer(Address peerMaintenanceAddress) {
		this.peerLastSeen.put(peerMaintenanceAddress,
				new Date(System.currentTimeMillis()));
	}

	public boolean hasPeer(String sid, Address peer) {
		return this.peerSets.containsKey(sid)
				&& this.peerSets.get(sid).contains(peer);
	}

	public void removePeer(Address address) {
		// remove peer from lastSeen
		this.peerLastSeen.remove(address);

		// remove peer from peerSet of stream
		for (Set<Address> peers : this.peerSets.values()) {
			if (peers.contains(address)) {
				peers.remove(address);
				System.out.println("  => removing peer '" + address + "'");
				break;
			}
		}

		// remove peer from locations
		this.peerLocations.remove(address);

		// remove peer from locationSet
		for (Map<Location, Set<Address>> map : this.locationMaps.values()) {
			for (Set<Address> set : map.values()) {
				set.remove(address);
			}
		}
	}

	public void addPeerToStream(String sid, Address peer) {
		this.peerSets.get(sid).add(peer);
		this.seePeer(peer);
		System.out.println("  => adding peer '" + peer + "' to stream " + sid);
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// GETTER STREAM
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public Stream[] getStreams() {
		Stream[] streams = new Stream[this.streams.size()];

		ArrayList<String> ids = new ArrayList<String>(this.streams.keySet());
		Collections.sort(ids);

		int i = 0;
		for (String id : ids) {
			streams[i++] = this.streams.get(id);
		}

		return streams;
	}

	public MStream[] getMStreams() {
		MStream[] mstreams = new MStream[this.streams.size()];

		int i = 0;
		for (Stream s : this.streams.values()) {
			mstreams[i++] = new MStream(s.getIdentifier(), s.getStripes(),
					s.getVideoRate(), this.getMaintenanceAddress(),
					this.sources.get(s.getIdentifier()));
		}

		return mstreams;
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// GETTER SOURCE
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public Address getSourceForStream(String stream) {
		return this.sources.get(stream);
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// GETTER PEER
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public Address[] getPeersForStream(String sid, Address requester,
			int maxCount) {
		/**
		 * return empty list if stream unkown
		 */
		if (!this.peerSets.containsKey(sid)) {
			return new Address[0];
		}

		HashSet<Address> peers = new HashSet<Address>();

		/**
		 * initialize peer list based on location
		 */
		if (Common.TRACKER_SELECT_PEER_LIST_BASED_ON_LOCATION) {
			Location location = this.peerLocations.get(requester);
			peers.addAll(this.getLocationSet(sid, location));
			peers.remove(requester);
		}

		/**
		 * if no peers known for location or selection not based on location,
		 * initialize peer list with list of all peers from stream including
		 * source
		 */
		if (peers.size() == 0) {
			for (Address peer : this.getAllPeersForStream(sid)) {
				if (!peer.equals(requester)) {
					peers.add(peer);
				}
			}
			peers.add(this.sources.get(sid));
		}

		/**
		 * randomize order of peers in list
		 */
		ArrayList<Address> list = new ArrayList<Address>();
		for (Address peer : peers) {
			list.add(peer);
		}
		Collections.shuffle(list);

		/**
		 * return subset of peers with maximum size maxCount
		 */
		Address[] result = new Address[Math.min(maxCount, list.size())];
		for (int i = 0; i < result.length; i++) {
			result[i] = list.get(i);
		}
		return result;
	}

	public Address[] getAllPeersForStream(String sid) {
		return this.getAllPeersForStream(sid, null);
	}

	public Address[] getAllPeersForStream(String sid, Address requester) {
		HashSet<Address> peers = new HashSet<Address>();

		if (this.peerSets.containsKey(sid)) {
			peers = (HashSet<Address>) this.peerSets.get(sid);

			if (requester != null) {
				peers.remove(requester);
			}
			peers.remove(this.sources.get(sid));

			Address[] result = new Address[peers.size()];
			int i = 0;
			for (Address peer : peers) {
				result[i] = peer;
				i++;
			}
			return result;
		} else {
			return new Address[0];
		}
	}

	public Address[] getAllPeers() {
		HashSet<Address> peers = new HashSet<Address>();

		for (Set<Address> addresses : this.peerSets.values()) {
			for (Address a : addresses) {
				peers.add(a);
			}
		}

		return (Address[]) peers.toArray();
	}

	public String getStreamOfPeer(Address peer) {
		for (String sid : this.peerSets.keySet()) {
			if (this.peerSets.get(sid).contains(peer)) {
				return sid;
			}
		}
		return null;
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// GETTER LAST_SEEN
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public Date getLastSeenPeer(Address address) {
		return this.peerLastSeen.get(address);
	}

	public Date getLastSeenSource(Address address) {
		return this.sourceLastSeen.get(address);
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// LOCATION
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public Map<Location, Set<Address>> getLocationMap(String sid) {
		if (!this.locationMaps.containsKey(sid)) {
			this.locationMaps.put(sid, new HashMap<Location, Set<Address>>());
		}
		return this.locationMaps.get(sid);
	}

	public Set<Address> getLocationSet(String sid, Location location) {
		Map<Location, Set<Address>> map = this.getLocationMap(sid);
		if (!map.containsKey(location)) {
			map.put(location, new HashSet<Address>());
		}
		return map.get(location);
	}

	public void changeLocation(String sid, Address peer, Location location) {
		if (this.peerLocations.containsKey(peer)
				&& location.equals(this.peerLocations.get(peer))) {
			return;
		}

		System.out.println("Change location of peer '" + peer + "' in " + sid
				+ " to " + location);
		if (this.peerLocations.containsKey(peer)) {
			Location oldLocation = this.peerLocations.get(peer);
			this.getLocationSet(sid, oldLocation).remove(peer);
		}
		this.peerLocations.put(peer, location);
		this.getLocationSet(sid, location).add(peer);
	}

	public void printLocations() {
		System.out.println(".............................");

		for (Address peer : this.peerLocations.keySet()) {
			System.out.println(peer + " @ " + this.peerLocations.get(peer));
		}

		for (String sid : this.locationMaps.keySet()) {
			System.out.println(".............................");
			System.out.println(sid);
			for (Location location : this.locationMaps.get(sid).keySet()) {
				System.out.println("  " + location);
				for (Address peer : this.locationMaps.get(sid).get(location)) {
					System.out.println("    - " + peer);
				}
			}
		}

		System.out.println(".............................");
	}

}
