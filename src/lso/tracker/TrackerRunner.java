package lso.tracker;

import java.io.IOException;

import lso.Address;
import lso.Common;
import lso.MaintenanceNode;
import lso.Option;
import lso.OptionParser;
import lso.net.message.maintenance.MaintenanceMessageHandler;

public class TrackerRunner implements Runnable {
	public static final String DEFAULT_MAINTENANCE_HOSTNAME = "localhost";
	public static final int DEFAULT_MAINTENANCE_PORT = 8888;

	private Tracker tracker;

	private MaintenanceNode maintenanceNode;

	private Thread keepAliveThread;
	private Thread maintenanceNodeThread;

	public TrackerRunner(Address address) throws IOException {
		this.tracker = new Tracker(address);
		this.maintenanceNode = new MaintenanceNode(address);
		MaintenanceMessageHandler mmh = new TrackerMaintenanceMessageHandler(
				this.tracker, this.maintenanceNode);
		this.maintenanceNode.setMessageHandler(mmh);

		this.maintenanceNodeThread = new Thread(this.maintenanceNode);

		this.keepAliveThread = new TrackerKeepAlive(maintenanceNode, tracker,
				Common.TRACKER_KEEP_ALIVE_WAIT,
				Common.TRACKER_SOURCE_ALIVE_THRESHOLD,
				Common.TRACKER_PEER_ALIVE_THRESHOLD);

		TrackerShutdownHook trsh = new TrackerShutdownHook();
		Runtime.getRuntime().addShutdownHook(trsh);
	}

	@Override
	public void run() {
		this.startMaintenanceNode();
	}

	private void startMaintenanceNode() {
		this.maintenanceNodeThread.start();
	}

	public static void main(String[] args) {
		/**
		 * define parameters
		 */
		Option bindOption = new Option("b", "bind", "Address to bind to", true,
				false);

		Option[] options = { bindOption };

		/**
		 * create option parser
		 */
		OptionParser optionParser = new OptionParser(options, "tracker.jar "
				+ "--bind hostname:port");
		try {
			optionParser.parse(args);
		} catch (IllegalArgumentException e) {
			System.err.println(e.getMessage());
			System.exit(-1);
		}

		/**
		 * get parameters from option parser
		 */
		Option bindArgument = optionParser.getByLongOption("bind");
		Address address = bindArgument.asAddress(-1);

		/**
		 * start tracker
		 */
		try {
			TrackerRunner trackerRunner = new TrackerRunner(address);
			trackerRunner.startMaintenanceNode();
			trackerRunner.keepAliveThread.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
