package lso;

public class ChildAddress {
	private Address maintenance;
	private Address data;

	public ChildAddress(Address maintenance, Address data) {
		this.setMaintenance(maintenance);
		this.setData(data);
	}

	public Address getMaintenance() {
		return maintenance;
	}

	private void setMaintenance(Address maintenance) {
		this.maintenance = maintenance;
	}

	public Address getData() {
		return data;
	}

	private void setData(Address data) {
		this.data = data;
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof ChildAddress)) {
			return false;
		}
		ChildAddress ca = (ChildAddress) obj;
		return ca.getMaintenance().equals(this.maintenance)
				&& ca.getData().equals(this.data);
	}

}
