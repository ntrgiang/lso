package lso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Common {
	public static final String STREAM_DESCRIPTOR_DEFAULT = "DEFAULT_SDF";
	
	/**
	 * TOPOLOGY CHECK TURNS
	 */
	public static final int TURN_BANDWIDTH_FREE = 1;
	public static final int TURN_BANDWIDTH_INSUFFICIENT = 1;
	
	public static final int TURN_CHILDREN_ALIVE = 6;
	public static final int TURN_CHILDREN_CONNECTIONS = 6;
	
	public static final int TURN_LOCATION = -1;
	
	public static final int TURN_LOOPS = -1;

	public static final int TURN_PARENTS_ALIVE = 2;
	public static final int TURN_PARENTS_CONNECTIONS = 6;
	public static final int TURN_PARENTS_REJOIN = 1;
	
	public static final int TURN_HEIGHTS = 1;
	
	public static final int TURN_UPDATE_STREAMER = 1;

	/**
	 * TOPOLOGY CHECK WAIT
	 */
	public final static long PEER_TOPOLOGY_CHECK_WAIT = 2000;
	public final static long SOURCE_TOPOLOGY_CHECK_WAIT = 2000;
	
	/**
	 * TOPOLOGY CHECK BEHAVIOR
	 */
	public static final boolean pushDownMultiple = true;
	public static final boolean pullUpMultiple = true;
	
	/**
	 * PEER
	 */
	public final static int PEER_DEFAULT_PORT_MAINTENANCE = 6000;
	public final static int PEER_DEFAULT_PORT_DATA = 7000;
	public final static int PEER_DEFAULT_NUMBER_OF_DATA_PORTS = 4;

	/**
	 * MONITOR
	 */
	public final static int MONITOR_MAX_IMAGE_HEIGHT = 35;
	public final static int MONITOR_MAX_IMAGE_WIDTH = 35;

	public static final String MONITOR_DEFAULT_MAINTENANCE_HOSTNAME = "localhost";
	public static final int MONITOR_DEFAULT_MAINTENANCE_PORT = 4444;

	/**
	 * BANDWIDTH
	 */
	public final static int PEER_DEFAULT_BANDWIDTH = 1;
	public static final int SOURCE_DEFAULT_AVAILABLE_BANDWIDTH = 3;

	/**
	 * KEEP_ALIVE
	 */
	public final static long PEER_KEEP_ALIVE_WAIT = 2000;
	public final static long SOURCE_KEEP_ALIVE_WAIT = 2000;
	public final static long TRACKER_KEEP_ALIVE_WAIT = 2000;
	public final static long TRACKER_PEER_ALIVE_THRESHOLD = 10000;
	public final static long TRACKER_SOURCE_ALIVE_THRESHOLD = 10000;

	/**
	 * SOURCE
	 */
	public static final int SOURCE_DEFAULT_STRIPES = 8;
	public static final String SOURCE_DEFAULT_MAINTENANCE_HOSTNAME = "localhost";
	public static final int SOURCE_DEFAULT_MAINTENANCE_PORT = 3000;

	/**
	 * LOCATION
	 */

	public static final int LOCATION_NUMBER_OF = 3;
	public static final boolean LOCATION_CHOOSE_RANDOM_AT_STARTUP = false;

	/**
	 * TRACKER
	 */

	public static final int TRACKER_MAXIMUM_PEER_LIST_SIZE = Integer.MAX_VALUE;
	public static final boolean TRACKER_SELECT_PEER_LIST_BASED_ON_LOCATION = true;

	public static List<Integer> getRandomOrder(int number, Random random) {
		List<Integer> order = new ArrayList<Integer>(number);
		for (int i = 0; i < number; i++) {
			order.add(i);
		}
		Collections.shuffle(order);
		return order;
	}

	public static boolean isWindows() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("win") >= 0);
	}

	public static boolean isMac() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("mac") >= 0);
	}

	public static boolean isLinux() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("nux") >= 0);
	}

	public static boolean isUnix() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("nix") >= 0);
	}

	public static boolean isAndroid() {
		String url = System.getProperty("java.vendor.url").toLowerCase();
		return (url.indexOf("android") >= 0);
	}
}
