package lso.monitor.config;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import lso.monitor.extern.XMLFacade;
import edu.uci.ics.jung.visualization.layout.PersistentLayout.Point;

public class Layouts {

	private static HashMap<Integer, HashSet<int[]>> mLayouts = new HashMap<Integer, HashSet<int[]>>();
	private static HashMap<Integer, int[]> mSessionFavorites = new HashMap<Integer, int[]>();

	// XML TAGS
	public static String XML_TAG_LAYOUTS = "LAYOUTS";
	private static String XML_TAG_LAYOUT = "LAYOUT";

	public static HashSet<int[]> getLayouts(Integer number) {
		if (mLayouts.containsKey(number) && (mLayouts.get(number).size() > 0))
			return mLayouts.get(number);

		HashSet<int[]> layouts = new HashSet<int[]>();
		layouts.add(getSquareLayout(number));
		mLayouts.put(number, layouts);

		return layouts;
	}

	public static int[] getSquareLayout(Integer number) {
		// Default square layout
		int root = (int) Math.ceil(Math.sqrt(number));
		int[] ret = new int[root];
		for (int i = 0; i < root; i++)
			ret[i] = root;

		return ret;
	}

	public static XMLFacade toXML() {
		XMLFacade ret = new XMLFacade(XML_TAG_LAYOUTS);

		addLayout(3, new int[] { 1, 2 });
		addLayout(3, new int[] { 1, 1, 1 });

		for (Integer key : mLayouts.keySet()) {
			XMLFacade child = new XMLFacade(XML_TAG_LAYOUT + key);
			for (int[] arr : mLayouts.get(key))
				child.addChild(XML_TAG_LAYOUT, convertArrayToString(arr));
			ret.addChild(child);
		}

		return ret;
	}

	public static void fromXML(XMLFacade layout) {
		if ((layout == null) || (layout.getName() == null))
			return;
		if (!layout.getName().toLowerCase()
				.equals(XML_TAG_LAYOUTS.toLowerCase()))
			return;

		for (XMLFacade child : layout.getChildren()) {
			try {
				int number = Integer.parseInt(child.getName().substring(
						XML_TAG_LAYOUT.length()));
				for (XMLFacade layouts : child.getChildren())
					if (layouts.getName().toLowerCase()
							.equals(XML_TAG_LAYOUT.toLowerCase())) {
						int arr[] = convertStringToArray(layouts.getText());
						addLayout(number, arr);
					}
			} catch (Exception ex) {
			}
		}

	}

	public static void addLayout(Integer number, int[] arr) {
		ensureCorrectLayout(arr, number);

		HashSet<int[]> layouts = mLayouts.get(number);
		if (layouts == null) {
			layouts = new HashSet<int[]>();
			layouts.add(getSquareLayout(number));
			mLayouts.put(number, layouts);
		}

		for (int[] content : layouts)
			if (equals(arr, content))
				return;

		layouts.add(arr);
	}

	public static void ensureCorrectLayout(int arr[], int size) {
		if ((arr == null) || (arr.length == 0))
			arr = new int[] { size };
		int count = 0;
		for (int i : arr)
			count += i;

		if (count < size)
			arr[arr.length - 1] += size - count;
	}

	public static String convertArrayToString(int[] arr) {
		String ret = null;

		for (int i = 0; i < arr.length; i++)
			if (ret == null)
				ret = arr[i] + "";
			else
				ret += "," + arr[i];

		return ret;
	}

	public static int[] convertStringToArray(String arr) {
		LinkedList<Integer> mReturn = new LinkedList<Integer>();

		for (String S : arr.split(",")) {
			try {
				mReturn.add(Integer.parseInt(S));
			} catch (Exception ex) {
			}
		}

		int[] ret = new int[mReturn.size()];
		for (int i = 0; i < mReturn.size(); i++)
			ret[i] = mReturn.get(i);
		return ret;
	}

	private static boolean equals(int[] arrA, int[] arrB) {
		if (arrA.length != arrB.length)
			return false;

		for (int i = 0; i < arrA.length; i++)
			if (arrA[i] != arrB[i])
				return false;

		return true;
	}

	public static void setSessionFavorite(int stripes, int[] arr) {
		mSessionFavorites.put(stripes, arr);
	}

	public static int[] getSessionFavorite(int stripes) {
		if (mSessionFavorites.containsKey(stripes))
			return mSessionFavorites.get(stripes);
		else if (getLayouts(stripes).size() > 0) {
			return getLayouts(stripes).iterator().next();
		}
		return getSquareLayout(stripes);
	}

	/**
	 * generiert ein Vorschaubild f�r ein Layout
	 * 
	 * @param arr
	 * @param background
	 * @param border
	 * @return
	 */
	public static BufferedImage generateImage(int[] arr, Color background,
			int border) {
		BufferedImage ret = new BufferedImage(64, 48,
				BufferedImage.TYPE_INT_RGB);

		Graphics G = ret.getGraphics();
		G.setColor(Color.BLACK);
		G.fillRect(0, 0, ret.getWidth(), ret.getHeight());
		G.setColor(background);
		G.fillRect(border, border, ret.getWidth() - (2 * border),
				ret.getHeight() - (2 * border));
		G.setColor(Color.red);

		for (SimpleEntry<Point, Dimension> p : calculateLayout(arr,
				new Dimension(ret.getWidth(), ret.getHeight()), border))
			G.fillArc((int) p.getKey().x, (int) p.getKey().y,
					p.getValue().width, p.getValue().height, 0, 360);

		return ret;
	}

	private static int max(int[] arr) {
		int ret = 0;

		for (int i : arr)
			if (i > ret)
				ret = i;

		return ret;
	}

	public static LinkedList<SimpleEntry<Point, Dimension>> calculateLayout(
			int[] arr, Dimension size, int border) {
		LinkedList<SimpleEntry<Point, Dimension>> ret = new LinkedList<SimpleEntry<Point, Dimension>>();

		if ((arr != null) && (arr.length > 0)) {
			int max = Math.max(max(arr), arr.length);
			if (max > 0) {
				Dimension subsize = new Dimension((size.width - 2 * border)
						/ max, (size.height - 2 * border) / max);

				int padTop = (size.height - arr.length * subsize.height) / 2;
				for (int y = 0; y < arr.length; y++)
					if (arr[y] > 0) {
						int padLeft = (size.width - (subsize.width * arr[y])) / 2;
						for (int x = 0; x < arr[y]; x++) {
							ret.add(new SimpleEntry<Point, Dimension>(
									new Point(padLeft + x * subsize.width,
											padTop + y * subsize.height),
									subsize));
						}
					}
			}
		}

		return ret;
	}
}
