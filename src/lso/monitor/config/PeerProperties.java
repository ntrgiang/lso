package lso.monitor.config;

import java.util.LinkedList;

import lso.monitor.extern.XMLFacade;

public class PeerProperties {

	static LinkedList<PeerProperty> mProperties = new LinkedList<PeerProperty>();
	// XML TAGS
	public static String XML_TAG_PEER_PROPERTIES = "PEER_PROPERTIES";

	public static XMLFacade toXML() {
		XMLFacade ret = new XMLFacade(XML_TAG_PEER_PROPERTIES);

		for (PeerProperty p : mProperties)
			ret.addChild(p.toXML());

		return ret;
	}

	public static void fromXML(XMLFacade properties) {
		reset();

		if ((properties == null)
				|| (!properties.getName().toLowerCase()
						.equals(XML_TAG_PEER_PROPERTIES.toLowerCase())))
			return;

		for (XMLFacade child : properties.getChildren()) {
			try {
				PeerProperty p = new PeerProperty(child);
				addProperty(p);
			} catch (Exception e) {
				System.out.println("NodeProperties.fromXML: " + e.toString());
			}
		}
	}

	private static void addProperty(PeerProperty p) {
		if (isKnownProperty(p.getName()))
			return;
		mProperties.add(p);
	}

	public static LinkedList<PeerProperty> getProperties() {
		return mProperties;
	}

	public static PeerProperty getProperty(String name) {
		name = name.toUpperCase();
		for (PeerProperty check : mProperties)
			if (check.getName().equals(name))
				return check;
		return null;
	}

	public static Boolean isKnownProperty(String name) {
		return (getProperty(name) != null);
	}

	private static void reset() {
		mProperties.clear();
	}
}
