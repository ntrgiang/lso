package lso.monitor.config;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import lso.Common;
import lso.monitor.extern.XMLFacade;

public class PeerImages {

	private static HashMap<String, BufferedImage> mPeerImages = new HashMap<String, BufferedImage>();

	public static void loadPeerImages(XMLFacade images) {
		for (XMLFacade child : images.getChildren()) {
			try {
				// BufferedImage img = ImageIO.read(new File(child.getText()));
				// mPeerImages.put(child.getName(), img);
				BufferedImage img = ImageIO.read(new File(child.getText()));
				int MAX_WIDTH = Common.MONITOR_MAX_IMAGE_WIDTH;
				int MAX_HEIGHT = Common.MONITOR_MAX_IMAGE_HEIGHT;
				if ((img.getWidth() > MAX_WIDTH)
						|| (img.getHeight() > MAX_HEIGHT)) {
					BufferedImage tmp = new BufferedImage(MAX_WIDTH,
							MAX_HEIGHT, img.getType());
					Graphics2D g = tmp.createGraphics();
					g.drawImage(img, 0, 0, MAX_WIDTH, MAX_HEIGHT, null);
					g.dispose();
					img = tmp;
				}
				mPeerImages.put(child.getName(), img);
			} catch (IOException e) {
			}
		}
	}

	public static BufferedImage getPeerImage(String type) {
		if (type == null)
			return null;
		type = type.toLowerCase();
		for (String key : mPeerImages.keySet())
			if (key.toLowerCase().equals(type))
				return mPeerImages.get(key);
		return null;
	}
}
