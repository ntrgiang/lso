package lso.monitor.config;

import java.io.File;
import java.io.IOException;

import lso.monitor.extern.XMLFacade;

public class Config {

	// XMLConfig
	private static XMLFacade mConfig = null;
	// XML Tags
	private static String XML_TAG_ROOT = "MONITOR";
	private static String XML_TAG_PEER_IMAGES = "PEER_IMAGES";
	private static String XML_TAG_LOG_POSITION = "LOG_POSITION";
	// default file?
	private static String CONFIG_FILE = "./config.cfg";

	public static boolean load() {
		reset();

		try {
			File cfgFile = new File(CONFIG_FILE);
			if (!cfgFile.isFile())
				return false;

			mConfig = XMLFacade.loadXML(cfgFile);

			if (!mConfig.getName().toLowerCase()
					.equals(XML_TAG_ROOT.toLowerCase()))
				reset();

			Layouts.fromXML(mConfig.getChild(Layouts.XML_TAG_LAYOUTS));
			PeerProperties.fromXML(mConfig
					.getChild(PeerProperties.XML_TAG_PEER_PROPERTIES));

			if (mConfig.getChild(XML_TAG_PEER_IMAGES) != null)
				PeerImages
						.loadPeerImages(mConfig.getChild(XML_TAG_PEER_IMAGES));
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public static boolean save() {
		if (mConfig == null)
			reset();

		mConfig.removeChild(Layouts.XML_TAG_LAYOUTS);
		mConfig.addChild(Layouts.toXML());

		mConfig.removeChild(PeerProperties.XML_TAG_PEER_PROPERTIES);
		mConfig.addChild(PeerProperties.toXML());

		try {
			mConfig.saveToFile(new File(CONFIG_FILE));
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	public static void reset() {
		mConfig = new XMLFacade(XML_TAG_ROOT);
		mConfig.addChild(new XMLFacade(XML_TAG_PEER_IMAGES));
	}

	public static boolean isLogTop() {
		if (mConfig.getChild(XML_TAG_LOG_POSITION) == null)
			return false;
		return mConfig.getChild(XML_TAG_LOG_POSITION).getText().toLowerCase()
				.equals("top");
	}

	public static boolean isLogRight() {
		if (mConfig.getChild(XML_TAG_LOG_POSITION) == null)
			return true;
		return mConfig.getChild(XML_TAG_LOG_POSITION).getText().toLowerCase()
				.equals("right");
	}
}
