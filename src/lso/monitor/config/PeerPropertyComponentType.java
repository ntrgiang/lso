package lso.monitor.config;

public enum PeerPropertyComponentType {
	UNKNOWN, TEXTFIELD, NUMBERFIELD, SLIDER, COMBOBOX, BOOLEAN, HIDDEN;

	public static PeerPropertyComponentType fromString(String str) {
		str = str.toUpperCase();

		if (str.equals("FIELD") || str.equals("TEXTFIELD"))
			return TEXTFIELD;
		if (str.equals("HIDDEN"))
			return HIDDEN;
		if (str.equals("COMBOBOX"))
			return COMBOBOX;
		if (str.equals("BOOLEAN"))
			return BOOLEAN;
		if (str.equals("NUMBERFIELD"))
			return NUMBERFIELD;

		return UNKNOWN;
	}

	public String toString() {
		switch (this) {
		case TEXTFIELD:
			return "TEXTFIELD";
		case SLIDER:
			return "SLIDER";
		case COMBOBOX:
			return "COMBOBOX";
		case BOOLEAN:
			return "BOOLEAN";
		case HIDDEN:
			return "HIDDEN";
		case NUMBERFIELD:
			return "NUMBERFIELD";
		default:
			return "";
		}
	}
}
