package lso.monitor.config;

import lso.monitor.extern.XMLFacade;

public class PeerProperty {

	private String mName;
	private String mDisplayName = null;
	private String mType = null;
	private String mDefault = null;
	private Double mMin = Double.NaN;
	private Double mMax = Double.NaN;
	private Double mSteps = Double.NaN;
	private Boolean mReadonly = false;
	private PeerPropertyComponentType mComponent = PeerPropertyComponentType.UNKNOWN;

	// XML TAG
	public static String XML_TAG_PROPERTY = "PROPERTY";
	private static String XML_TAG_NAME = "NAME";
	private static String XML_TAG_DISPLAY_NAME = "DISPLAY_NAME";
	private static String XML_TAG_TYPE = "TYPE";
	private static String XML_TAG_MIN = "MIN";
	private static String XML_TAG_MAX = "MAX";
	private static String XML_TAG_STEPS = "STEPS";
	private static String XML_TAG_DEFAULT = "DEFAULT";
	private static String XML_TAG_COMPONENT = "COMPONENT";
	private static String XML_TAG_READONLY = "READONLY";
	// Type TAGs
	private static String TAG_String = "String".toLowerCase();
	private static String TAG_Number = "Number".toLowerCase();
	private static String TAG_Boolean = "Boolean".toLowerCase();

	public PeerProperty(XMLFacade property) throws Exception {
		if ((property == null)
				|| (!property.getName().toLowerCase()
						.equals(XML_TAG_PROPERTY.toLowerCase())))
			throw new Exception("NaP");

		mName = property.getChild(XML_TAG_NAME).getText().toUpperCase();
		mType = property.getChild(XML_TAG_TYPE).getText();

		if (property.getChild(XML_TAG_DISPLAY_NAME) != null)
			mDisplayName = property.getChild(XML_TAG_DISPLAY_NAME).getText();

		if (isNumber() && (property.getChild(XML_TAG_STEPS) != null))
			mSteps = Double.parseDouble(property.getChild(XML_TAG_STEPS)
					.getText());

		if (property.getChild(XML_TAG_MIN) != null)
			mMin = Double.parseDouble(property.getChild(XML_TAG_MIN).getText());
		if (property.getChild(XML_TAG_MAX) != null)
			mMax = Double.parseDouble(property.getChild(XML_TAG_MAX).getText());

		if (property.getChild(XML_TAG_COMPONENT) != null)
			mComponent = PeerPropertyComponentType.fromString(property
					.getChild(XML_TAG_COMPONENT).getText());

		if (property.getChild(XML_TAG_DEFAULT) != null) {
			mDefault = property.getChild(XML_TAG_DEFAULT).getText();
		}

		if (property.getChild(XML_TAG_READONLY) != null) {
			mReadonly = Boolean.parseBoolean(property
					.getChild(XML_TAG_READONLY).getText());
		}
	}

	public PeerProperty() {

	}

	public XMLFacade toXML() {
		XMLFacade ret = new XMLFacade(XML_TAG_PROPERTY);

		ret.addChild(XML_TAG_NAME, mName);
		ret.addChild(XML_TAG_TYPE, mType);
		if ((mDisplayName != null) && (!mDisplayName.isEmpty()))
			ret.addChild(XML_TAG_DISPLAY_NAME, mDisplayName);

		if (!Double.isNaN(mSteps))
			ret.addChild(XML_TAG_STEPS, mSteps.toString());
		if (!Double.isNaN(mMin))
			ret.addChild(XML_TAG_MIN, mMin.toString());
		if (!Double.isNaN(mMax))
			ret.addChild(XML_TAG_MAX, mMax.toString());

		if (mComponent != PeerPropertyComponentType.UNKNOWN)
			ret.addChild(XML_TAG_COMPONENT, mComponent.toString());

		if (mDefault != null)
			ret.addChild(XML_TAG_DEFAULT, mDefault);
		if (mReadonly)
			ret.addChild(XML_TAG_READONLY, mReadonly.toString());

		return ret;
	}

	public String getName() {
		return mName;
	}

	public String getDisplayName() {
		if ((mDisplayName != null) && (!mDisplayName.isEmpty()))
			return mDisplayName;
		return mName;
	}

	public boolean isNumber() {
		return ((mType != null) && (mType.toLowerCase().equals(TAG_Number)));
	}

	public boolean isString() {
		return ((mType != null) && (mType.toLowerCase().equals(TAG_String)));
	}

	public boolean isBoolean() {
		return ((mType != null) && (mType.toLowerCase().equals(TAG_Boolean)));
	}

	public boolean isReadonly() {
		return mReadonly;
	}

	public Double getMin() {
		return mMin;
	}

	public Double getMax() {
		return mMax;
	}

	public Double getSteps() {
		if (Double.isNaN(mSteps))
			return 1.0;
		return mSteps;
	}

	public String getDefault() {
		return mDefault;
	}

	public boolean hasMin() {
		return !Double.isNaN(mMin);
	}

	public boolean hasMax() {
		return !Double.isNaN(mMax);
	}

	public boolean hasSteps() {
		return !Double.isNaN(mSteps);
	}

	/**
	 * @return which component type should be used
	 */
	public PeerPropertyComponentType getComponentType() {
		if (mComponent != PeerPropertyComponentType.UNKNOWN)
			return mComponent;

		if (isBoolean())
			return PeerPropertyComponentType.BOOLEAN;
		if (isNumber()) {
			if ((!Double.isNaN(mMin)) && (!Double.isNaN(mMax)))
				if ((mMax - mMin) / getSteps() <= 20)
					return PeerPropertyComponentType.COMBOBOX;
				else
					return PeerPropertyComponentType.NUMBERFIELD;
			return PeerPropertyComponentType.NUMBERFIELD;
		}
		return PeerPropertyComponentType.TEXTFIELD;
	}

	public void setName(String name) {
		mName = name;
	}

	public void setType(String type) {
		mType = type;
	}

	public void setDefault(String def) {
		mDefault = def;
	}

	public void setSteps(Double steps) {
		mSteps = steps;
	}

	public void setMin(Double min) {
		mMin = min;
	}

	public void setMax(Double max) {
		mMax = max;
	}
}