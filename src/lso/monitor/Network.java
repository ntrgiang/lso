package lso.monitor;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.UUID;

import lso.Address;
import lso.Child;
import lso.MStream;
import lso.MaintenanceNode;
import lso.Parent;
import lso.monitor.model.Peer;
import lso.monitor.model.Stream;
import lso.monitor.model.Tracker;
import lso.monitor.swingui.LogListener;
import lso.net.message.maintenance.GetChildren;
import lso.net.message.maintenance.GetInfo;
import lso.net.message.maintenance.GetParents;
import lso.net.message.maintenance.Kill;
import lso.net.message.maintenance.MGetAllPeersForStream;
import lso.net.message.maintenance.MGetStreams;
import lso.net.message.maintenance.SetProperties;
import lso.net.message.maintenance.Term;
import lso.net.response.ReturnCode;
import lso.net.response.maintenance.Children;
import lso.net.response.maintenance.Info;
import lso.net.response.maintenance.MStreams;
import lso.net.response.maintenance.MaintenanceResponse;
import lso.net.response.maintenance.Parents;
import lso.net.response.maintenance.PeerList;
import edu.uci.ics.jung.graph.DelegateForest;

public class Network {

	private MaintenanceNode mSocket;
	private Address mOwnAddress;

	public static Network last;

	private HashSet<LogListener> logListeners = new HashSet<LogListener>();

	public void addListener(LogListener l) {
		logListeners.add(l);
	}

	public void removeLIstener(LogListener l) {
		logListeners.remove(l);
	}

	public Network(Address monitor, MaintenanceNode socket) {
		mOwnAddress = monitor;
		mSocket = socket;
		last = this;
	}

	public Address getOwnAddress() {
		return mOwnAddress;
	}

	private boolean streamsEquals(MStream ms, Stream s) {
		return (ms.getIdentifier().equals(s.getIdentifier()))
				&& (ms.getStripes() == s.getStripes().length)
				&& (ms.getSource().equals(s.getSource()));
	}

	public boolean updateStreams(Tracker t) {
		if (mSocket == null)
			return false;

		for (LogListener l : logListeners)
			l.addLine("requesting Streams");

		MaintenanceResponse getStreamsResponse;
		try {
			getStreamsResponse = mSocket.send(t.getAddress(), new MGetStreams(
					UUID.randomUUID(), (new java.sql.Date(0))));
			MStreams getStreams = (MStreams) getStreamsResponse;

			LinkedList<Stream> newStreams = new LinkedList<Stream>();

			for (MStream ms : getStreams.getMStreams()) {
				boolean existed = false;
				for (Stream s : t.getStreams())
					if (streamsEquals(ms, s)) {
						existed = true;
						newStreams.add(s);
						break;
					}

				if (!existed)
					newStreams.add(new Stream(ms.getStripes(), ms
							.getIdentifier(), ms.getSource()));
			}

			t.getStreams().clear();
			Collections.sort(newStreams);
			t.getStreams().addAll(newStreams);

			for (LogListener l : logListeners)
				l.addLine("requesting Streams -> got " + newStreams.size()
						+ " Stream(s)!");
		} catch (Exception e) {
			for (LogListener l : logListeners)
				l.addLine("requesting Streams -> [Error]: " + e.toString());
			return false;
		}

		return true;
	}

	private boolean updateStreamPeers(Tracker t, Stream s) {
		if (mSocket == null)
			return false;
		if (!t.getStreams().contains(s))
			return false;

		for (LogListener l : logListeners)
			l.addLine("requesting Peers for " + s.getIdentifier());
		MaintenanceResponse getPeerListResponse;
		try {
			getPeerListResponse = mSocket.send(t.getAddress(),
					new MGetAllPeersForStream(s.getIdentifier()));
			PeerList getPeerList = (PeerList) getPeerListResponse;
			s.setPeers(getPeerList);
			for (LogListener l : logListeners)
				l.addLine("requesting Peers for " + s.getIdentifier()
						+ " -> got " + s.getPeers().size() + " Peer(s)!");
		} catch (Exception e) {
			System.out.println("updateStream: " + e.toString());
			return false;
		}

		return true;
	}

	public boolean updatePeerInfo(Peer p) {
		if (mSocket == null)
			return false;

		for (LogListener l : logListeners)
			l.addLine("requesting properties from " + p.toString());

		MaintenanceResponse getPeerInfoResponse;
		try {
			getPeerInfoResponse = mSocket.send(p.getAddress(), new GetInfo());
			Info getInfo = (Info) getPeerInfoResponse;
			for (String k : getInfo.getProperties().keySet()) {
				p.setPropertyValue(k, getInfo.getProperties().get(k));
			}
		} catch (Exception e) {
			System.out.println("updatePeerInfo: " + e.toString());
			return false;
		}

		return true;
	}

	private Parent[] getParents(Peer p) {
		MaintenanceResponse getParentsResponse;
		try {
			getParentsResponse = mSocket.send(p.getAddress(), new GetParents(
					UUID.randomUUID(), (new java.sql.Date(0))));
			Parents getParents = (Parents) getParentsResponse;
			return getParents.getParents();
		} catch (Exception e) {
		}
		return null;
	}

	private Child[] getChildren(Peer p) {
		MaintenanceResponse getChildrenResponse;
		try {
			getChildrenResponse = mSocket.send(p.getAddress(), new GetChildren(
					UUID.randomUUID(), (new java.sql.Date(0))));
			getChildrenResponse = mSocket.send(p.getAddress(), new GetChildren(
					UUID.randomUUID(), (new java.sql.Date(0))));
			Children getChildrens = (Children) getChildrenResponse;
			return getChildrens.getChildren();
		} catch (Exception e) {
			System.out.println("getChildren: " + e.toString());
		}
		return null;
	}

	private class Pair<E> {
		public E a;
		public E b;

		public Pair(E a, E b) {
			this.a = a;
			this.b = b;
		}

		@Override
		public boolean equals(Object o) {
			if (!(o instanceof Pair))
				return false;

			@SuppressWarnings("unchecked")
			Pair<Object> p = (Pair<Object>) o;
			if ((a.equals(p.a)) && (b.equals(p.b)))
				return true;

			return false;
		}
	}

	private boolean remove(HashSet<Pair<Peer>> hashSet, Pair<Peer> search) {
		for (Pair<Peer> p : hashSet)
			if (p.equals(search)) {
				hashSet.remove(p);
				return true;
			}
		return false;
	}

	public void updateStreamStructure(Tracker t, Stream s) {
		if ((t == null) || (s == null))
			return;
		updateStreamPeers(t, s);

		HashMap<Integer, HashSet<Pair<Peer>>> relations = new HashMap<Integer, HashSet<Pair<Peer>>>();
		for (int i = 0; i < s.getStripes().length; i++)
			relations.put(i, new HashSet<Pair<Peer>>());

		HashSet<Peer> deadPeers = new HashSet<Peer>();
		// get structure -> get parents
		for (Peer p : s.getPeers())
			if (!p.getAddress().equals(s.getSource())) {
				try {
					for (Parent parent : getParents(p)) {
						if (s.getPeer(parent.getAddress()) != null) {
							relations.get(parent.getStripe()).add(
									new Pair<Peer>(s.getPeer(parent
											.getAddress()), p));
						} else {
							for (LogListener l : logListeners)
								l.addLine("unknown parent <"
										+ parent.getAddress() + "> for Peer <"
										+ p.toString() + ">");
						}
					}
				} catch (Exception ex) {
					deadPeers.add(p);
				}
			}

		// remove existing edges
		for (Integer key : relations.keySet()) {
			DelegateForest<Peer, Integer> stripe = s.getStripes()[key];

			for (Integer e : new HashSet<Integer>(stripe.getEdges()))
				stripe.removeEdge(e, false);
		}

		Integer edge = 0;
		// confirm structure -> get children
		HashSet<Peer> peers = new HashSet<Peer>(s.getPeers());
		peers.removeAll(deadPeers);
		for (Peer p : peers)
			try {
				for (Child child : getChildren(p)) {
					Pair<Peer> pair = new Pair<Peer>(p, s.getPeer(child
							.getChildAddress().getMaintenance()));
					if (remove(relations.get(child.getStripe()), pair)) {
						// confirmed ->
						s.getStripes()[child.getStripe()].addEdge(edge, pair.a,
								pair.b);
						edge++;
					} else {
						relations.get(child.getStripe()).add(pair);
					}
				}
			} catch (Exception ex) {
				deadPeers.add(p);
			}

		// report errors
		for (Integer key : relations.keySet()) {
			for (Pair<Peer> pair : relations.get(key)) {
				for (LogListener l : logListeners)
					if ((pair.a == null) && (pair.b == null))
						;
					else if (pair.a == null)
						l.addLine("structure error: " + pair.b.toString()
								+ " reported NULL-parent for stripe " + key);
					else if (pair.b == null)
						l.addLine("structure error: " + pair.a.toString()
								+ " reported NULL-Child for stripe " + key);
					else
						l.addLine("structure error: " + pair.a.toString()
								+ " -> " + pair.b.toString() + " for stripe "
								+ key + " not confirmed!");
			}
		}
		for (Peer p : deadPeers) {
			s.removePeer(p);
			for (LogListener l : logListeners)
				l.addLine("Peer " + p.toString() + " seems to be dead!");
		}
	}

	public boolean killPeer(Peer p) {
		MaintenanceResponse killResponse;
		for (LogListener l : logListeners)
			l.addLine("killing " + p.getAddress().toString());
		try {
			killResponse = mSocket.send(p.getAddress(),
					new Kill(UUID.randomUUID(), (new java.sql.Date(0))));
			return killResponse.getReturnCode() == ReturnCode.OK;
		} catch (Exception e) {
			System.out.println("killPeer: " + e.toString());
		}
		return false;
	}

	public boolean terminatePeer(Peer p) {
		MaintenanceResponse terminateResponse;
		for (LogListener l : logListeners)
			l.addLine("terminating " + p.getAddress().toString());
		try {
			terminateResponse = mSocket.send(p.getAddress(),
					new Term(UUID.randomUUID(), (new java.sql.Date(0))));
			return terminateResponse.getReturnCode() == ReturnCode.OK;
		} catch (Exception e) {
			System.out.println("terminatePeer: " + e.toString());
		}
		return false;
	}

	public boolean sendPeerProperties(Peer p) {
		MaintenanceResponse setPropertiesResponse;
		for (LogListener l : logListeners)
			l.addLine("sending properties to " + p.toString());
		try {
			setPropertiesResponse = mSocket.send(p.getAddress(),
					new SetProperties(p.getProperties()));
			return setPropertiesResponse.getReturnCode() == ReturnCode.OK;
		} catch (Exception e) {
			System.out.println("sendPeerProperties: " + e.toString());
		}
		return false;
	}
}
