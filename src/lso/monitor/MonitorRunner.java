package lso.monitor;

import java.io.IOException;

import lso.Address;
import lso.MaintenanceNode;
import lso.Option;
import lso.OptionParser;
import lso.monitor.model.Tracker;
import lso.monitor.swingui.SwingUserInterface;
import lso.net.message.maintenance.MaintenanceMessageHandler;

public class MonitorRunner implements Runnable {

	public MonitorRunner(Address address, Address trackerMaintenanceAddress,
			int refresh, int defaultPeerBandwidth) throws IOException {
		this.maintenanceNode = new MaintenanceNode(address);
		MaintenanceMessageHandler mmh = new MonitorMaintenanceMessageHandler();
		this.maintenanceNode.setMessageHandler(mmh);

		this.maintenanceNodeThread = new Thread(this.maintenanceNode);
		this.currentTracker = new Tracker(trackerMaintenanceAddress);
		this.defaultRefreshRate = refresh;

		this.defaultPeerBandwidth = defaultPeerBandwidth;

		network = new Network(address, maintenanceNode);
		ui = new SwingUserInterface(this);
	}

	public void run() {
		this.maintenanceNodeThread.start();
		ui.run();
	}

	private UserInterface ui;

	private Network network;

	public int defaultRefreshRate;

	public int defaultPeerBandwidth;

	private MaintenanceNode maintenanceNode;

	private Thread maintenanceNodeThread;

	private Tracker currentTracker;

	public Tracker getCurrentTracker() {
		return currentTracker;
	}

	public void switchTracker(Address newAddress) {
		currentTracker = new Tracker(newAddress);
	}

	public Network getNetwork() {
		return network;
	}

	public static void main(String[] args) {
		/**
		 * define parameters
		 */
		Option bindOption = new Option("b", "bind", "Address to bind to", true,
				false);
		Option trackerOption = new Option("t", "tracker", "Tracker's address",
				true, false);
		Option bandwidthOption = new Option("p", "peer-bandwidth",
				"Bandwidth for starting a peer", true, false);
		Option refreshOption = new Option("r", "refresh",
				"Refresh rate in ms (0 to disable)", false, false);

		Option[] options = { bindOption, trackerOption, bandwidthOption,
				refreshOption };

		/**
		 * create option parser
		 */
		OptionParser optionParser = new OptionParser(options, "monitor.jar "
				+ "--bind hostname:port " + "--tracker hostname:port "
				+ "--peer-bandwidth bandwidth " + "[--refresh refresh-rate]");
		try {
			optionParser.parse(args);
		} catch (IllegalArgumentException e) {
			System.err.println(e.getMessage());
			System.exit(-1);
		}

		/**
		 * get parameters from option parser
		 */
		Option bindArgument = optionParser.getByLongOption("bind");
		Address address = bindArgument.asAddress(-1);

		Option trackerArgument = optionParser.getByLongOption("tracker");
		Address tracker = trackerArgument.asAddress(-1);

		Option bandwidthArgument = optionParser
				.getByLongOption("peer-bandwidth");
		int bandwidth = Integer.parseInt(bandwidthArgument.getValue());

		Option refreshArgument = optionParser.getByLongOption("refresh");
		int refresh = 0;
		if (refreshArgument != null && refreshArgument.getValue() != null) {
			refresh = Integer.parseInt(refreshArgument.getValue());
		}

		/**
		 * start monitor
		 */
		try {
			MonitorRunner monitorRunner = new MonitorRunner(address, tracker,
					refresh, bandwidth);
			monitorRunner.run();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
