package lso.monitor;

import lso.net.message.maintenance.Alive;
import lso.net.message.maintenance.ChangeLocation;
import lso.net.message.maintenance.ChangeParent;
import lso.net.message.maintenance.Connect;
import lso.net.message.maintenance.ForwardChild;
import lso.net.message.maintenance.GetChildren;
import lso.net.message.maintenance.GetInfo;
import lso.net.message.maintenance.GetParents;
import lso.net.message.maintenance.GetPeerList;
import lso.net.message.maintenance.GetStreams;
import lso.net.message.maintenance.Kill;
import lso.net.message.maintenance.Leave;
import lso.net.message.maintenance.LeaveStripe;
import lso.net.message.maintenance.MGetAllPeers;
import lso.net.message.maintenance.MGetAllPeersForStream;
import lso.net.message.maintenance.MGetStreams;
import lso.net.message.maintenance.MaintenanceMessageHandler;
import lso.net.message.maintenance.PAlive;
import lso.net.message.maintenance.RegisterStream;
import lso.net.message.maintenance.RequestChild;
import lso.net.message.maintenance.SAlive;
import lso.net.message.maintenance.SetProperties;
import lso.net.message.maintenance.Term;
import lso.net.message.maintenance.UnregisterStream;
import lso.net.message.maintenance.WhichConnections;
import lso.net.response.ReturnCode;
import lso.net.response.maintenance.MaintenanceResponse;
import lso.net.response.maintenance.Null;

public class MonitorMaintenanceMessageHandler extends MaintenanceMessageHandler {

	public MonitorMaintenanceMessageHandler() {
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// alive
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleAlive(Alive alive) {
		return new Null(ReturnCode.OK);
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// unknown messages
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleChangeParent(ChangeParent changeParent) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleConnect(Connect connect) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleForwardChild(ForwardChild forwardChild) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleGetInfo(GetInfo getInfo) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleGetParents(GetParents getParents) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleGetPeerList(GetPeerList getPeerList) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleGetStreams(GetStreams getStreams) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleLeave(Leave leave) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleRegisterStream(
			RegisterStream registerStream) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleSetProperties(
			SetProperties setProperties) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleUnregisterStream(
			UnregisterStream unregisterStream) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleGetChildren(GetChildren getChildren) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleKill(Kill kill) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleMGetAllPeers(MGetAllPeers mGetAllPeers) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleMGetAllPeersForStream(
			MGetAllPeersForStream mGetAllPeersForStream) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleMGetStreams(MGetStreams mGetStreams) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handlePAlive(PAlive pAlive) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleRequestChild(RequestChild requestChild) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleSAlive(SAlive sAlive) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleTerm(Term term) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleLeaveStripe(LeaveStripe leaveStripe) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleWhichConnections(
			WhichConnections whichConnections) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleChangeLocation(
			ChangeLocation changeAccessNetwork) {
		return this.createUnknownMessageResponse();
	}

}
