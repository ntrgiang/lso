package lso.monitor;

import lso.monitor.config.Config;

public abstract class UserInterface implements Runnable {

	protected MonitorRunner monitorRunner;

	public UserInterface(MonitorRunner mr) {
		monitorRunner = mr;
		Config.load();
	}

	protected void onExiting() {
		Config.save();
	}
}
