package lso.monitor.extern;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

public class XMLFacade {

	private final Element root;

	/**
	 * Creates a new XML root with the given name
	 */
	public XMLFacade(String name) {
		this.root = new Element(name);
	}

	public XMLFacade(XMLFacade parent, String name) {
		this(name);
		parent.addChild(this);
	}

	/**
	 * Creates a new JDOM XML root
	 */
	private XMLFacade(Element root) {
		this.root = root;
	}

	/**
	 * The JDOM root Element
	 */
	private Element getRootElement() {
		return root;
	}

	/**
	 * Returns the name of the tag of the node.
	 */
	public String getName() {
		return root.getName();
	}

	/**
	 * Adds a content text to the root
	 */
	public void addContent(String content) {
		root.addContent(content);
	}

	/**
	 * Returns the text of the root
	 */
	public String getText() {
		return root.getText();
	}

	/**
	 * Adds a new Child element to the root
	 */
	public XMLFacade addChild(String name, String content) {
		final Element e = new Element(name);
		e.addContent(content);
		root.addContent(e);
		return new XMLFacade(e);
	}

	/**
	 * Adds a attribut to the root
	 */
	public void setAttribute(String name, String value) {
		root.setAttribute(name, value);
	}

	/**
	 * Returns the value for the given attribut
	 * 
	 * @param name
	 *            - name of the attribute
	 * @return value of the attribute
	 */
	public String getAttribute(String name) {
		return root.getAttributeValue(name);
	}

	/**
	 * Adds an XML element to the root
	 */
	public void addChild(XMLFacade child) {
		root.addContent(child.getRootElement());
	}

	public void removeChild(String name) {
		root.removeChild(name);
	}

	/**
	 * The XML children of the root
	 */
	@SuppressWarnings("unchecked")
	public List<XMLFacade> getChildren() {
		final List<Element> childs = root.getChildren();
		final List<XMLFacade> c = new ArrayList<XMLFacade>(childs.size());
		for (Element child : childs) {
			c.add(new XMLFacade(child));
		}

		return c;
	}

	/**
	 * Returns the child with the given name
	 */
	public XMLFacade getChild(String name) {
		Element element = root.getChild(name);
		if (element != null)
			return new XMLFacade(element);

		return null;
	}

	/**
	 * Save the root to the given OutputStream
	 * 
	 * @param out
	 *            - OutputStream where to save the XML
	 * @throws IOException
	 */
	public void save(OutputStream out) throws IOException {
		Document doc = root.getDocument();
		if (doc == null)
			doc = new Document(root);

		XMLOutputter xmlOut = new XMLOutputter();
		xmlOut.setFormat(Format.getPrettyFormat());

		xmlOut.output(doc, out);
		out.flush();
	}

	/**
	 * Save the root to a file with the given filename
	 * 
	 * @param filename
	 *            - name of the file
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void saveToFile(String filename) throws IOException {
		saveToFile(new File(filename));
	}

	public void saveToFile(File file) throws IOException {
		if (file.isDirectory()) {
			throw new IllegalArgumentException("File must not be a directory!");
		}
		OutputStream out = null;
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			out = new BufferedOutputStream(new FileOutputStream(file));
			save(out);
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e1) {
				}
			}
		}
	}

	/**
	 * Creates an XML element with the given content
	 * 
	 * @param element
	 *            - name of the element
	 * @param content
	 *            - content of the element
	 * @return XML element with given content
	 */
	public static XMLFacade createElement(String element, String content) {
		final XMLFacade e = new XMLFacade(element);
		e.addContent(content);
		return e;
	}

	/**
	 * Loads a XML document from the given InputStream
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	public static XMLFacade loadXML(InputStream in) throws IOException {
		XMLFacade c = null;

		SAXBuilder builder = new SAXBuilder();
		builder.setValidation(false);
		builder.setIgnoringElementContentWhitespace(true);
		try {
			Document doc = builder.build(in);

			Element root = doc.getRootElement();
			c = new XMLFacade(root);
		} catch (JDOMException e) {
			throw new IOException(e);
		}
		return c;
	}

	public static XMLFacade loadXML(File file) throws IOException {
		if (!file.isFile()) {
			throw new IllegalArgumentException("File is a directory!");
		}
		InputStream in = null;
		try {
			in = new BufferedInputStream(new FileInputStream(file));
			return loadXML(in);
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
				}
			}
		}
	}

}
