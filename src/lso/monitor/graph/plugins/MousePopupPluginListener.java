package lso.monitor.graph.plugins;

import lso.monitor.model.Peer;

public interface MousePopupPluginListener {
	void terminatedPeer(Peer p);

	void killedPeer(Peer p);
}
