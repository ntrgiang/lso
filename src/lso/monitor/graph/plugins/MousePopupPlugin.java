package lso.monitor.graph.plugins;

import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;
import java.util.HashSet;

import javax.swing.AbstractAction;
import javax.swing.JPopupMenu;

import lso.monitor.graph.MultiTreeEdge;
import lso.monitor.graph.MultiTreeVertex;
import lso.monitor.model.Peer;
import edu.uci.ics.jung.algorithms.layout.GraphElementAccessor;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.AbstractPopupGraphMousePlugin;

/**
 * @author Thorsten Jacobi erstellt ein Popup-Menue beim Rechtsklick auf einen
 *         Vertex
 */
public class MousePopupPlugin extends AbstractPopupGraphMousePlugin implements
		MouseListener {

	HashSet<MousePopupPluginListener> listeners = new HashSet<MousePopupPluginListener>();

	public void addListener(MousePopupPluginListener l) {
		listeners.add(l);
	}

	public void removeListener(MousePopupPluginListener l) {
		listeners.remove(l);
	}

	public MousePopupPlugin() {
		this(MouseEvent.BUTTON3_MASK);
	}

	public MousePopupPlugin(int modifiers) {
		super(modifiers);
	}

	@SuppressWarnings("unchecked")
	protected void handlePopup(MouseEvent e) {
		final VisualizationViewer<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>> vv = (VisualizationViewer<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>>) e
				.getSource();
		Point2D p = e.getPoint();

		GraphElementAccessor<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>> pickSupport = vv
				.getPickSupport();
		if (pickSupport != null) {
			final MultiTreeVertex<Peer, Integer> v = pickSupport.getVertex(
					vv.getGraphLayout(), p.getX(), p.getY());
			if (v != null) {
				JPopupMenu popup = getPeerMenu(vv, v.getVertex(), v.getGraph());

				popup.show(vv, e.getX(), e.getY());
			}
		}
	}

	@SuppressWarnings("serial")
	private JPopupMenu getPeerMenu(
			final VisualizationViewer<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>> vv,
			final Peer p, final Graph<Peer, Integer> g) {
		JPopupMenu ret = new JPopupMenu();

		ret.add(new AbstractAction("Terminate") {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				p.terminate();
				for (MousePopupPluginListener l : listeners)
					l.terminatedPeer(p);
			}
		});

		ret.add(new AbstractAction("Kill") {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				p.kill();
				for (MousePopupPluginListener l : listeners)
					l.killedPeer(p);
			}
		});

		return ret;
	}
}
