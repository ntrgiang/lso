package lso.monitor.graph.plugins;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Point2D;

import lso.monitor.graph.MultiTreeEdge;
import lso.monitor.graph.MultiTreeVertex;
import lso.monitor.model.Peer;
import lso.monitor.swingui.StreamVisualization;
import edu.uci.ics.jung.algorithms.layout.GraphElementAccessor;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.AbstractPopupGraphMousePlugin;

/**
 * @author Thorsten Jacobi Interpretiert das Klicken auf einen Vertex als
 *         Auswahl dessen und meldet es der StreamVisualization
 */
public class MouseSelectionPlugin extends AbstractPopupGraphMousePlugin
		implements MouseListener {

	StreamVisualization parent;

	public MouseSelectionPlugin(StreamVisualization parent) {
		super(MouseEvent.BUTTON2_MASK);

		this.parent = parent;
	}

	@SuppressWarnings("unchecked")
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() != MouseEvent.BUTTON1)
			return;
		final VisualizationViewer<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>> vv = (VisualizationViewer<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>>) e
				.getSource();
		Point2D p = e.getPoint();

		GraphElementAccessor<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>> pickSupport = vv
				.getPickSupport();
		if (pickSupport != null) {
			final MultiTreeVertex<Peer, Integer> v = pickSupport.getVertex(
					vv.getGraphLayout(), p.getX(), p.getY());
			if (v != null) {
				if (parent != null)
					parent.selectPeer(v.getVertex(), !vv.getPickedVertexState()
							.isPicked(v));
			}
		}
	}

	@Override
	protected void handlePopup(MouseEvent arg0) {
	}
}
