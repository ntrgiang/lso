package lso.monitor.graph.transformers;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import lso.monitor.graph.MultiTreeVertex;
import lso.monitor.model.Peer;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.visualization.decorators.DefaultVertexIconTransformer;

/**
 * @author Thorsten Jacobi erstellt ein Bild fuer einen Vertex/Node, falls es
 *         fuer den Node-Typ definiert wurde -> es wird ein Bild mit gr��e =
 *         max(width, height) des definierten Bildes erstellt und (falls
 *         ausgewaehlt) ein Kreishintergrund in der Auswahl Farbe erstellt.
 */
public class VertexIconTransformer extends
		DefaultVertexIconTransformer<MultiTreeVertex<Peer, Integer>> implements
		Transformer<MultiTreeVertex<Peer, Integer>, Icon> {

	VertexColorTransformer mColors = null;

	public VertexIconTransformer(VertexColorTransformer colors) {
		mColors = colors;
	}

	@Override
	public Icon transform(MultiTreeVertex<Peer, Integer> v) {
		BufferedImage img = lso.monitor.config.PeerImages.getPeerImage(v
				.getVertex().getNodeType());
		if (img == null)
			return null;

		int max = Math.max(img.getWidth(), img.getHeight());
		BufferedImage colored = new BufferedImage(max, max,
				BufferedImage.TYPE_INT_ARGB);
		Graphics G = colored.getGraphics();
		if (mColors.getColor(v.getVertex()) != null) {
			G.setColor(mColors.getColor(v.getVertex()));
			G.fillArc(0, 0, max, max, 0, 360);
		}
		G.drawImage(img, (max - img.getWidth()) / 2,
				(max - img.getHeight()) / 2, null);

		return new ImageIcon(colored);
	}

}
