package lso.monitor.graph.transformers;

import lso.monitor.graph.MultiTreeVertex;
import lso.monitor.model.Peer;
import lso.monitor.model.Stream;

import org.apache.commons.collections15.Transformer;

/**
 * @author Thorsten Jacobi erstellt f�r jeden Peer eine (in dem aktuellen
 *         Graphen) eindeutige ID
 */
public class PeerVertexNumberTransformer implements
		Transformer<MultiTreeVertex<Peer, Integer>, String> {

	private Stream mCurrentStream;

	@Override
	public String transform(MultiTreeVertex<Peer, Integer> arg0) {
		return mCurrentStream.getPeerID(arg0.getVertex()).toString();
	}

	public void setStream(Stream stream) {
		this.mCurrentStream = stream;
	}
}
