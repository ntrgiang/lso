package lso.monitor.graph.transformers;

import java.awt.Color;
import java.awt.Paint;
import java.util.HashMap;
import java.util.Random;

import lso.monitor.graph.MultiTreeVertex;
import lso.monitor.model.Peer;

import org.apache.commons.collections15.Transformer;

/**
 * @author Thorsten Jacobi ordnet Vertex/Nodes eine Farbe zu
 */
public class VertexColorTransformer implements
		Transformer<MultiTreeVertex<Peer, Integer>, Paint> {

	private HashMap<Peer, Color> mColors = new HashMap<Peer, Color>();

	@Override
	public Paint transform(MultiTreeVertex<Peer, Integer> arg0) {
		if (mColors.containsKey(arg0.getVertex()))
			return mColors.get(arg0.getVertex());
		return Color.red;
	}

	public void reset() {
		mColors.clear();
	}

	public void setColor(Peer p, Color c) {
		if (c == null)
			while (isColorUsed(c = getRandomColor()))
				;
		mColors.put(p, c);
	}

	public void removeColor(Peer p) {
		mColors.remove(p);
	}

	private Color getRandomColor() {
		Random R = new Random();
		return new Color(R.nextInt(16) * 16, R.nextInt(16) * 16,
				R.nextInt(16) * 16);
	}

	private boolean isColorUsed(Color check) {
		if (check.equals(Color.red))
			return true;
		for (Color c : mColors.values()) {
			if (check.equals(c))
				return true;
		}
		return false;
	}

	public Color getColor(Peer p) {
		if (mColors.containsKey(p)) {
			return mColors.get(p);
		}
		return null;
	}
}
