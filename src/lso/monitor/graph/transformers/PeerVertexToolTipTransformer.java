package lso.monitor.graph.transformers;

import lso.monitor.graph.MultiTreeVertex;
import lso.monitor.model.Peer;

import org.apache.commons.collections15.Transformer;

/**
 * @author Thorsten Jacobi erstellt einen einfachen ToolTip fuer einen
 *         Vertex/Node Achtung: Diese Funktion wird sehr oft von Jung
 *         aufgerufen!(bei jeder Mausbewegung �ber einem Vertex)
 */
public class PeerVertexToolTipTransformer implements
		Transformer<MultiTreeVertex<Peer, Integer>, String> {

	@Override
	public String transform(MultiTreeVertex<Peer, Integer> arg0) {
		return arg0.getVertex().getAddress().toString();
	}

}
