package lso.monitor.graph.transformers;

import java.awt.Shape;
import java.awt.geom.Arc2D;
import java.awt.image.BufferedImage;

import lso.monitor.graph.MultiTreeVertex;
import lso.monitor.model.Peer;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.visualization.decorators.VertexIconShapeTransformer;

/**
 * @author Thorsten Jacobi erstellt einen einfachen Shape fuer Vertex/Nodes die
 *         ein Bild hinterlegt haben -> es wird ein Kreis erstellt mit
 *         Durchmesser = max(width, height)
 */
public class VertexShapeTransformer extends
		VertexIconShapeTransformer<MultiTreeVertex<Peer, Integer>> {

	public VertexShapeTransformer(
			Transformer<MultiTreeVertex<Peer, Integer>, Shape> delegate) {
		super(delegate);
	}

	public Shape transform(MultiTreeVertex<Peer, Integer> v) {
		Peer vertex = v.getVertex();
		if (vertex == null) {
			return delegate.transform(v);
		}
		String nodeType = vertex.getNodeType();
		if (nodeType == null) {
			return delegate.transform(v);
		}
		BufferedImage img = lso.monitor.config.PeerImages
				.getPeerImage(nodeType);

		if (img == null)
			return delegate.transform(v);

		int size = Math.max(img.getWidth(), img.getHeight());

		return new java.awt.geom.Arc2D.Double(-size / 2, -size / 2, size, size,
				0, 360, Arc2D.PIE);
	}
}
