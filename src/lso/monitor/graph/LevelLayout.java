package lso.monitor.graph;

import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.util.HashSet;
import java.util.LinkedList;

import lso.monitor.model.Peer;
import edu.uci.ics.jung.graph.DelegateForest;
import edu.uci.ics.jung.visualization.layout.PersistentLayout.Point;

public class LevelLayout extends BasicLayout {

	@Override
	protected void calculateStripe(Point location, Dimension size,
			DelegateForest<Peer, Integer> stripe, Peer source) {
		if ((stripe == null) || (stripe.getVertexCount() <= 0))
			return;

		int totalheight = getMainTree(stripe, source).getHeight()
				+ getMaxSubTreeHeight(stripe, source) + 1;
		LinkedList<Peer> roots = new LinkedList<Peer>();
		int height = size.height / totalheight;

		// Hauptbaum
		roots.add(source);
		calculate(location, size.width, height, stripe, roots);
		roots.clear();

		// Teilbaueme
		if (stripe.getRoots().size() > 1) {
			roots.addAll(sortPeers(new HashSet<Peer>(stripe.getRoots())));
			roots.remove(source);
			location.y += height
					* (getMainTree(stripe, source).getHeight() + 1);
			calculate(location, size.width, height, stripe, roots);
		}
	}

	private void calculate(Point location, int width, int height,
			DelegateForest<Peer, Integer> stripe, LinkedList<Peer> roots) {
		if (roots.size() == 0)
			return;

		int peerwidth = width / roots.size();
		int i = 0;
		LinkedList<Peer> children = new LinkedList<Peer>();
		for (Peer node : roots) {
			MultiTreeVertex<Peer, Integer> mtv = getVertex(node, stripe, true);
			setLocation(mtv, new Point2D.Double(location.x + (i + 0.5)
					* peerwidth, location.y));

			if (stripe.getParentEdge(node) != null)
				getEdge(stripe.getParentEdge(node), stripe, true);

			children.addAll(sortPeers(new HashSet<Peer>(stripe
					.getChildren(node))));
			i++;
		}

		calculate(new Point(location.x, location.y + height), width, height,
				stripe, children);
	}
}
