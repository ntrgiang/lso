package lso.monitor.graph;

import edu.uci.ics.jung.graph.Graph;

public class MultiTreeVertex<V, E> {

	private V mVertex;
	private Graph<V, E> mGraph;

	public MultiTreeVertex(V vertex, Graph<V, E> graph) {
		mVertex = vertex;
		mGraph = graph;
	}

	public V getVertex() {
		return mVertex;
	}

	public Graph<V, E> getGraph() {
		return mGraph;
	}

	@Override
	public String toString() {
		return mVertex.toString();
	}
}
