package lso.monitor.graph;

import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;

import lso.monitor.config.Layouts;
import lso.monitor.model.Peer;
import lso.monitor.model.Stream;

import org.apache.commons.collections15.Transformer;

import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.DelegateForest;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.Tree;
import edu.uci.ics.jung.visualization.layout.PersistentLayout.Point;

public abstract class BasicLayout implements
		Layout<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>> {

	private Dimension mSize = new Dimension(100, 100);
	private int[] mStripeLayout = null;

	protected Stream mStream = null;
	protected DelegateForest<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>> mComposedGraph = new DelegateForest<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>>();
	private HashMap<MultiTreeVertex<Peer, Integer>, Point2D> mLocations = new HashMap<MultiTreeVertex<Peer, Integer>, Point2D>();

	@Override
	public Point2D transform(MultiTreeVertex<Peer, Integer> arg0) {
		if (mLocations.containsKey(arg0))
			return mLocations.get(arg0);

		return new Point2D.Double(0, 0);
	}

	@Override
	public void setLocation(MultiTreeVertex<Peer, Integer> arg0, Point2D arg1) {
		mLocations.put(arg0, arg1);
	}

	@Override
	public Graph<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>> getGraph() {
		return mComposedGraph;
	}

	@Override
	public Dimension getSize() {
		return mSize;
	}

	@Override
	public void setSize(Dimension arg0) {
		mSize = arg0;
	}

	public void setStripeLayout(int[] layout) {
		mStripeLayout = layout;
	}

	public int[] getStripeLayout() {
		return mStripeLayout;
	}

	public void setStream(Stream stream) {
		if (stream != null) {
			if ((mStream == null)
					|| (mStream.getStripes().length != stream.getStripes().length)) // reset
																					// root
																					// layout
																					// if
																					// needed
				mStripeLayout = Layouts
						.getSessionFavorite(stream.getStripes().length);
		}

		mComposedGraph = new DelegateForest<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>>();

		mStream = stream;
	}

	@Override
	public void initialize() {
		calculate();
	}

	@Override
	public void reset() {
		calculate();
	}

	public void calculate() {
		if (mStream == null)
			return;
		if (mStream.getStripes().length == 0)
			return;

		int stripes = mStream.getStripes().length;
		if (mStripeLayout == null)
			mStripeLayout = lso.monitor.config.Layouts
					.getSessionFavorite(stripes);
		lso.monitor.config.Layouts.ensureCorrectLayout(mStripeLayout, stripes);

		Peer source = mStream.getSourcePeer();
		int tree = 0;
		for (SimpleEntry<Point, Dimension> p : lso.monitor.config.Layouts
				.calculateLayout(mStripeLayout, mSize, 0)) {
			if (tree >= stripes)
				break;
			removeDeletedPeersAndEdges(mStream.getStripes()[tree]);
			calculateStripe(p.getKey(), p.getValue(),
					mStream.getStripes()[tree], source);
			tree++;
		}
	}

	protected abstract void calculateStripe(Point location, Dimension size,
			DelegateForest<Peer, Integer> stripe, Peer source);

	protected void removeDeletedPeersAndEdges(
			DelegateForest<Peer, Integer> stripe) {

		HashSet<MultiTreeEdge<Peer, Integer>> edges = new HashSet<MultiTreeEdge<Peer, Integer>>(
				mComposedGraph.getEdges());
		for (Integer edge : stripe.getEdges())
			edges.remove(getEdge(edge, stripe, false));
		for (MultiTreeEdge<Peer, Integer> e : edges)
			if (e.getGraph() == stripe)
				mComposedGraph.removeEdge(e, false);

		HashSet<MultiTreeVertex<Peer, Integer>> vertices = new HashSet<MultiTreeVertex<Peer, Integer>>(
				mComposedGraph.getVertices());
		for (Peer vertex : stripe.getVertices())
			vertices.remove(getVertex(vertex, stripe, false));
		for (MultiTreeVertex<Peer, Integer> v : vertices)
			if (v.getGraph() == stripe)
				mComposedGraph.removeVertex(v, false);
	}

	protected MultiTreeVertex<Peer, Integer> getVertex(Peer node,
			Graph<Peer, Integer> stripe, boolean create) {
		for (MultiTreeVertex<Peer, Integer> v : mComposedGraph.getVertices())
			if ((v.getGraph() == stripe) && (v.getVertex() == node))
				return v;
		if (!create)
			return null;
		MultiTreeVertex<Peer, Integer> ret = new MultiTreeVertex<Peer, Integer>(
				node, stripe);
		mComposedGraph.addVertex(ret);
		return ret;
	}

	protected MultiTreeEdge<Peer, Integer> getEdge(Integer edge,
			Graph<Peer, Integer> stripe, boolean create) {
		Peer from = stripe.getSource(edge);
		Peer to = stripe.getDest(edge);

		for (MultiTreeEdge<Peer, Integer> e : mComposedGraph.getEdges())
			if ((e.getGraph() == stripe)
					&& (mComposedGraph.getSource(e).getVertex() == from)
					&& (mComposedGraph.getDest(e).getVertex() == to))
				return e;

		if (!create)
			return null;

		MultiTreeEdge<Peer, Integer> ret = new MultiTreeEdge<Peer, Integer>(
				edge, stripe);
		MultiTreeVertex<Peer, Integer> src = getVertex(from, stripe, true);
		MultiTreeVertex<Peer, Integer> dst = getVertex(to, stripe, true);
		mComposedGraph.addEdge(ret, src, dst);
		return ret;
	}

	@SuppressWarnings("unchecked")
	protected Tree<Peer, Integer> getMainTree(
			DelegateForest<Peer, Integer> stripe, Peer source) {
		for (Tree<Peer, Integer> tree : stripe.getTrees()) {
			if (tree.getRoot() == source)
				return tree;
		}
		System.out.println("getMainTree: Source is not root of any tree");
		try {
			return (Tree<Peer, Integer>) stripe.getTrees().toArray()[0];
		} catch (Exception e) {
			return null;
		}

	}

	protected int getMaxSubTreeHeight(DelegateForest<Peer, Integer> stripe,
			Peer source) {
		int max = 0;
		for (Tree<Peer, Integer> tree : stripe.getTrees()) {
			if (tree.getRoot() != source)
				max = Math.max(max, tree.getHeight() + 1);
		}
		return max;
	}

	protected LinkedList<Peer> sortPeers(Set<Peer> peers) {

		LinkedList<Peer> ret = new LinkedList<Peer>();

		while (peers.size() > 0) {
			int lowest = Integer.MAX_VALUE;
			Peer best = null;
			for (Peer p : peers) {
				int current = mStream.getPeerID(p);
				if (current < lowest) {
					lowest = current;
					best = p;
				}
			}

			peers.remove(best);
			ret.add(best);
		}

		return ret;
	}

	@Override
	public boolean isLocked(MultiTreeVertex<Peer, Integer> arg0) {
		return false;
	}

	@Override
	public void lock(MultiTreeVertex<Peer, Integer> arg0, boolean arg1) {
		System.out.println("BasicLayout: lock -> not supported");
	}

	@Override
	public void setGraph(
			Graph<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>> arg0) {
		System.out.println("BasicLayout: setGraph -> not supported");
	}

	@Override
	public void setInitializer(
			Transformer<MultiTreeVertex<Peer, Integer>, Point2D> arg0) {
		System.out.println("BasicLayout: setInitializer -> not supported");
	}

}
