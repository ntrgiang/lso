package lso.monitor.graph;

import edu.uci.ics.jung.graph.Graph;

public class MultiTreeEdge<V, E> {

	private E mEdge;
	private Graph<V, E> mGraph;

	public MultiTreeEdge(E edge, Graph<V, E> graph) {
		mEdge = edge;
		mGraph = graph;
	}

	public E getEdge() {
		return mEdge;
	}

	public Graph<V, E> getGraph() {
		return mGraph;
	}

	@Override
	public String toString() {
		return mEdge.toString();
	}
}
