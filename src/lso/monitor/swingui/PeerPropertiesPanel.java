package lso.monitor.swingui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

import lso.monitor.config.PeerProperty;
import lso.monitor.config.PeerPropertyComponentType;
import lso.monitor.model.Peer;

public class PeerPropertiesPanel extends JPanel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 809519754856085624L;
	private StreamVisualization mSV;
	private Peer mPeer;

	private PeerPropertiesPanelManager manager;

	private JPanel mPropertiesPanel;
	private JPanel mButtonPanel;
	private JButton btnColor;
	private JButton btnApply;
	private JButton btnReset;
	private JButton btnKill;
	private JButton btnTerminate;

	public PeerPropertiesPanel(PeerPropertiesPanelManager manager, Peer peer,
			StreamVisualization sv) {
		this.manager = manager;
		mSV = sv;
		mPeer = peer;

		setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1;
		gbc.weighty = 0;
		gbc.gridx = 0;
		gbc.gridy = 0;

		add(createHeadPanel(), gbc);

		gbc.gridy = 1;
		add(createPropertiesPanel(), gbc);

		gbc.gridy = 2;
		if (peer.getProperties().get("TYPE").equals("SOURCE")) {
			add(createButtonPanelSource(), gbc);
		} else {
			add(createButtonPanelPeer(), gbc);
		}

		gbc.gridy = 3;
		gbc.weighty = 1;
		gbc.fill = GridBagConstraints.VERTICAL;
		add(Box.createVerticalGlue(), gbc);

		for (PeerProperty np : lso.monitor.config.PeerProperties
				.getProperties()) {
			if (np.getComponentType() != PeerPropertyComponentType.HIDDEN
					&& peer.getProperties().containsKey(np.getName())) {
				addPropertyComponent(np.getName());
			}
		}
		for (String k : peer.getProperties().keySet()) {
			if (!lso.monitor.config.PeerProperties.isKnownProperty(k)) {
				addPropertyComponent(k);
			}
		}
	}

	private JPanel createHeadPanel() {
		JPanel ret = new JPanel(new BorderLayout());

		JLabel lbl;
		if (mSV.getStream().getPeerID(mPeer) == 0)
			lbl = new JLabel(" Source");
		else
			lbl = new JLabel(" Peer " + mSV.getStream().getPeerID(mPeer));

		ret.add(lbl, BorderLayout.CENTER);
		lbl.addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent arg0) {
			}

			@Override
			public void mousePressed(MouseEvent arg0) {
			}

			@Override
			public void mouseExited(MouseEvent arg0) {
			}

			@Override
			public void mouseEntered(MouseEvent arg0) {
			}

			@Override
			public void mouseClicked(MouseEvent arg0) {
				mPropertiesPanel.setVisible(!mPropertiesPanel.isVisible());
				mButtonPanel.setVisible(mPropertiesPanel.isVisible());
			}
		});

		btnColor = new JButton("");
		if (lso.monitor.config.PeerImages.getPeerImage(mPeer.getNodeType()) != null)
			btnColor.setIcon(new ImageIcon(lso.monitor.config.PeerImages
					.getPeerImage(mPeer.getNodeType())));
		btnColor.setActionCommand("color");
		btnColor.addActionListener(this);
		ret.add(btnColor, BorderLayout.LINE_START);

		JButton btnClose = new JButton("X");
		btnClose.setActionCommand("close");
		btnClose.addActionListener(this);
		ret.add(btnClose, BorderLayout.LINE_END);

		if (mSV.getPeerColor(mPeer) != null)
			btnColor.setBackground(mSV.getPeerColor(mPeer));
		return ret;
	}

	private JPanel createPropertiesPanel() {
		mPropertiesPanel = new JPanel(new GridLayout(1, 1));
		return mPropertiesPanel;
	}

	private JPanel createButtonPanelPeer() {
		mButtonPanel = new JPanel(new GridLayout(2, 2));

		btnApply = new JButton("Apply");
		btnApply.setActionCommand("apply");
		btnApply.addActionListener(this);
		mButtonPanel.add(btnApply);

		btnReset = new JButton("Reset");
		btnReset.setActionCommand("reset");
		btnReset.addActionListener(this);
		mButtonPanel.add(btnReset);

		btnTerminate = new JButton("Terminate");
		btnTerminate.setActionCommand("terminate");
		btnTerminate.addActionListener(this);
		mButtonPanel.add(btnTerminate);

		btnKill = new JButton("Kill");
		btnKill.setActionCommand("kill");
		btnKill.addActionListener(this);
		mButtonPanel.add(btnKill);

		enableButtons(false);

		return mButtonPanel;
	}

	private JPanel createButtonPanelSource() {
		mButtonPanel = new JPanel(new GridLayout(1, 2));

		btnApply = new JButton("Apply");
		btnApply.setActionCommand("apply");
		btnApply.addActionListener(this);
		mButtonPanel.add(btnApply);

		btnReset = new JButton("Reset");
		btnReset.setActionCommand("reset");
		btnReset.addActionListener(this);
		mButtonPanel.add(btnReset);

		enableButtons(false);

		return mButtonPanel;
	}

	public Peer getPeerObject() {
		return mPeer;
	}

	private HashMap<String, PeerPropertyComponent> mComponents = new HashMap<String, PeerPropertyComponent>();

	private void addPropertyComponent(String property) {
		property = property.toUpperCase();
		for (String prop : mComponents.keySet())
			if (prop.equals(property))
				return;

		PeerPropertyComponent comp = new PeerPropertyComponent(this, property);
		mComponents.put(property, comp);
		mPropertiesPanel.add(comp);
		((GridLayout) mPropertiesPanel.getLayout()).setRows(mComponents.size());
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().toLowerCase().equals("close")) {
			mSV.selectPeer(mPeer, false);
		} else if (e.getActionCommand().toLowerCase().equals("reset")) {
			reset();
		} else if (e.getActionCommand().toLowerCase().equals("apply")) {
			apply();
		} else if (e.getActionCommand().toLowerCase().equals("kill")) {
			kill();
		} else if (e.getActionCommand().toLowerCase().equals("terminate")) {
			terminate();
		} else if (e.getActionCommand().toLowerCase().equals("color")) {
			mSV.setPeerColor(mPeer, null);
			if (mSV.getPeerColor(mPeer) != null)
				btnColor.setBackground(mSV.getPeerColor(mPeer));
		}
	}

	public void apply() {
		for (PeerPropertyComponent comp : mComponents.values())
			comp.apply();
		enableButtons(false);
		repaint();

		mPeer.sendProperties();
	}

	public void reset() {
		for (PeerPropertyComponent comp : mComponents.values())
			comp.reset();
		enableButtons(false);
		repaint();
	}

	public void kill() {
		mPeer.kill();
		mSV.selectPeer(mPeer, false);
		mSV.updateStream();
	}

	public void terminate() {
		mPeer.terminate();
		mSV.selectPeer(mPeer, false);
		mSV.updateStream();
	}

	public void enableButtons(boolean enabled) {
		btnApply.setEnabled(enabled);
		btnReset.setEnabled(enabled);
		if (enabled)
			manager.enableButtons(enabled);
	}
}
