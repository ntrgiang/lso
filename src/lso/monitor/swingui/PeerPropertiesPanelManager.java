package lso.monitor.swingui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.sun.corba.se.impl.javax.rmi.CORBA.Util;

import lso.Common;
import lso.monitor.model.Peer;
import lso.source.Source.Mode;

public class PeerPropertiesPanelManager extends JPanel implements
		PeerSelectionChangeListener, ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 50304736358107960L;

	private MonitoringServer mParent;
	HashMap<Peer, PeerPropertiesPanel> mPeers = new HashMap<Peer, PeerPropertiesPanel>();
	private JButton btnApplyAll;
	private JButton btnResetAll;
	private JButton btnTermAll;
	private JButton btnKillAll;
	private JButton btnAddPeer;
	private JPanel panelPeers;

	public PeerPropertiesPanelManager(MonitoringServer parent) {
		mParent = parent;
		mParent.getStreamVisualization().addListener(this);

		// setBackground(Color.yellow);
		panelPeers = new JPanel();
		JPanel panelButtons = new JPanel(new GridLayout(3, 2));

		setBorder(BorderFactory.createTitledBorder("Properties"));
		setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();

		gbc.fill = GridBagConstraints.BOTH;
		gbc.anchor = GridBagConstraints.PAGE_START;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.weightx = 1;
		gbc.weighty = 1;
		add(new JScrollPane(panelPeers), gbc);

		gbc.anchor = GridBagConstraints.PAGE_END;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0;
		gbc.gridy = 1;
		gbc.weighty = 0;
		add(panelButtons, gbc);

		panelPeers.setLayout(new BoxLayout(panelPeers, BoxLayout.PAGE_AXIS));

		btnApplyAll = new JButton("Apply All");
		btnApplyAll.setActionCommand("apply");
		btnApplyAll.addActionListener(this);
		btnResetAll = new JButton("Reset All");
		btnResetAll.setActionCommand("reset");
		btnResetAll.addActionListener(this);

		btnTermAll = new JButton("Terminate All");
		btnTermAll.setActionCommand("term");
		btnTermAll.addActionListener(this);
		btnKillAll = new JButton("Kill All");
		btnKillAll.setActionCommand("kill");
		btnKillAll.addActionListener(this);

		btnAddPeer = new JButton("Add Peer");
		btnAddPeer.setActionCommand("addpeer");
		btnAddPeer.addActionListener(this);

		panelButtons.add(btnApplyAll);
		panelButtons.add(btnResetAll);
		panelButtons.add(btnTermAll);
		panelButtons.add(btnKillAll);
		panelButtons.add(btnAddPeer);

		reset();
	}

	public void addPeer(Peer peer) {
		if (mPeers.containsKey(peer))
			return;

		peer.updateProperties();
		PeerPropertiesPanel panel = new PeerPropertiesPanel(this, peer,
				mParent.getStreamVisualization());

		panelPeers.add(panel);
		mPeers.put(peer, panel);

		enableKillButtons(true);

		revalidate();
		repaint();
	}

	public void removePeer(Peer peer) {
		if (!mPeers.containsKey(peer))
			return;

		panelPeers.remove(mPeers.get(peer));
		mPeers.remove(peer);

		enableKillButtons(mPeers.size() > 0);

		revalidate();
		repaint();
	}

	public void reset() {
		for (Peer p : mPeers.keySet())
			panelPeers.remove(mPeers.get(p));
		mPeers.clear();

		enableButtons(false);
		enableKillButtons(false);

		revalidate();
		repaint();
	}

	public void enableButtons(boolean enabled) {
		btnResetAll.setEnabled(enabled);
		btnApplyAll.setEnabled(enabled);
	}

	public void enableKillButtons(boolean enabled) {
		btnKillAll.setEnabled(enabled);
		btnTermAll.setEnabled(enabled);
	}

	@Override
	public void handleSelectChanged(Peer p, Boolean newState) {
		if (newState)
			addPeer(p);
		else
			removePeer(p);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().toLowerCase().equals("apply")) {
			for (PeerPropertiesPanel p : mPeers.values())
				p.apply();
			enableButtons(false);
		} else if (e.getActionCommand().toLowerCase().equals("reset")) {
			for (PeerPropertiesPanel p : mPeers.values())
				p.reset();
			enableButtons(false);
		} else if (e.getActionCommand().toLowerCase().equals("kill")) {
			HashSet<Peer> peers = new HashSet<Peer>(mPeers.keySet());
			for (Peer p : peers) {
				p.kill();
				mParent.getStreamVisualization().selectPeer(p, false);
			}
			reset();
			mParent.getStreamVisualization().updateStream();
		} else if (e.getActionCommand().toLowerCase().equals("term")) {
			HashSet<Peer> peers = new HashSet<Peer>(mPeers.keySet());
			for (Peer p : peers) {
				p.terminate();
				mParent.getStreamVisualization().selectPeer(p, false);
			}
			reset();
			mParent.getStreamVisualization().updateStream();
		} else if (e.getActionCommand().toLowerCase().equals("addpeer")) {
			if (mParent.getStreamVisualization().getStream() == null)
				return;

			int portM = Common.PEER_DEFAULT_PORT_MAINTENANCE;
			int portD = Common.PEER_DEFAULT_PORT_DATA;
			String tracker = mParent.getTracker().getAddress().toString();
			String stream = mParent.getStreamVisualization().getStream()
					.getIdentifier();
			int bw = this.mParent.getDefaultPeerBandwidth();
			int stripes = mParent.getStreamVisualization().getStream()
					.getStripes().length;

			/**
			 * # 1 = hostname <br>
			 * # 2 = port maintenance <br>
			 * # 3 = first data port <br>
			 * # 4 = tracker address <br>
			 * # 5 = stream name <br>
			 * # 6 = upload bandwidth <br>
			 * # 7 = number of data ports <br>
			 * # 8 = mode
			 */
			String cmd = "./peer.sh "
					+ mParent.getTracker().getAddress().getHostname() + " "
					+ portM + " " + portD + " " + tracker + " " + stream + " "
					+ bw + " " + Common.PEER_DEFAULT_NUMBER_OF_DATA_PORTS + " "
					+ Mode.OverlayOnly.toString() + " &";
			if (Common.isWindows()) {
				cmd = "addpeer.bat "
						+ mParent.getTracker().getAddress().getHostname() + " "
						+ portM + " " + portD + " " + tracker + " " + stream
						+ " " + bw + " "
						+ Common.PEER_DEFAULT_NUMBER_OF_DATA_PORTS + " &";
			}
			System.out.println("starting new peer for " + stream);
			if (!this.execute(cmd, true, true)) {
				System.out.println("error");
			}
		}
	}

	private boolean execute(String cmd, boolean printErrors, boolean printInput) {
		try {
			Runtime.getRuntime().exec(cmd, null);
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

}
