package lso.monitor.swingui;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.UIManager;

import lso.monitor.MonitorRunner;
import lso.monitor.UserInterface;

public class SwingUserInterface extends UserInterface implements WindowListener {

	public SwingUserInterface(MonitorRunner mr) {
		super(mr);

	}

	protected void finalize() throws Throwable {
		super.finalize();
	}

	@Override
	public void run() {
		try {
			// Set cross-platform Java L&F (also called "Metal")
			UIManager.setLookAndFeel(UIManager
					.getCrossPlatformLookAndFeelClassName());
		} catch (Exception e) {
		}

		MonitoringServer ms = new MonitoringServer(monitorRunner);

		ms.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ms.setSize(1024, 768);
		ms.setVisible(true);
		ms.setExtendedState(ms.getExtendedState() | JFrame.MAXIMIZED_BOTH);

		ms.addWindowListener(this);

		ms.setRefreshRate(monitorRunner.defaultRefreshRate);
		ms.setDefaultPeerBandwidth(monitorRunner.defaultPeerBandwidth);
	}

	@Override
	public void windowClosing(WindowEvent e) {
		onExiting();
	}

	@Override
	public void windowClosed(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowOpened(WindowEvent e) {
	}
}
