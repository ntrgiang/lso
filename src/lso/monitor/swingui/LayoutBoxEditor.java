package lso.monitor.swingui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.ComboBoxEditor;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import lso.monitor.config.Layouts;

public class LayoutBoxEditor extends JPanel implements ComboBoxEditor,
		CaretListener, ComponentListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5767591502163311140L;

	private JLabel lblImage;
	private JTextField txtText;

	public LayoutBoxEditor() {
		addComponentListener(this);
		setLayout(null);

		lblImage = new JLabel();
		lblImage.setPreferredSize(new Dimension(64, 48));
		lblImage.setLocation(0, 0);
		lblImage.setSize(64, 48);
		lblImage.setVisible(true);

		txtText = new JTextField();
		txtText.addCaretListener(this);
		txtText.setLocation(64, 0);
		txtText.setSize(Math.max(this.getWidth() - 64, 64), 48);
		txtText.setVisible(true);

		add(lblImage);
		add(txtText);
	}

	@Override
	public void addActionListener(ActionListener l) {
		txtText.addActionListener(l);
	}

	@Override
	public void removeActionListener(ActionListener l) {
		txtText.addActionListener(l);
	}

	@Override
	public Component getEditorComponent() {
		return this;
	}

	@Override
	public Object getItem() {
		return txtText.getText();
	}

	@Override
	public void setItem(Object anObject) {
		if (anObject == null)
			txtText.setText(null);
		else
			txtText.setText(anObject.toString());
		updateIcon();
	}

	@Override
	public void caretUpdate(CaretEvent e) {
		updateIcon();
	}

	@Override
	public void componentResized(ComponentEvent e) {
		if (txtText != null)
			txtText.setSize(Math.max(this.getWidth() - 64, 64), 48);
	}

	private void updateIcon() {
		if (lblImage == null)
			return;
		lblImage.setIcon(new ImageIcon(Layouts.generateImage(
				Layouts.convertStringToArray(txtText.getText()),
				getBackground(), 0)));
	}

	// unused events
	@Override
	public void selectAll() {
	}

	@Override
	public void componentHidden(ComponentEvent e) {
	}

	@Override
	public void componentMoved(ComponentEvent e) {
	}

	@Override
	public void componentShown(ComponentEvent e) {
	}
}
