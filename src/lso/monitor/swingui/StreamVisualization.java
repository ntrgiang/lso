package lso.monitor.swingui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

import javax.swing.JPanel;
import javax.swing.Timer;

import lso.monitor.config.Layouts;
import lso.monitor.graph.BasicLayout;
import lso.monitor.graph.LevelLayout;
import lso.monitor.graph.MultiTreeEdge;
import lso.monitor.graph.MultiTreeVertex;
import lso.monitor.graph.plugins.MousePopupPlugin;
import lso.monitor.graph.plugins.MousePopupPluginListener;
import lso.monitor.graph.plugins.MouseSelectionPlugin;
import lso.monitor.graph.transformers.PeerVertexNumberTransformer;
import lso.monitor.graph.transformers.PeerVertexToolTipTransformer;
import lso.monitor.graph.transformers.VertexColorTransformer;
import lso.monitor.graph.transformers.VertexIconTransformer;
import lso.monitor.graph.transformers.VertexShapeTransformer;
import lso.monitor.model.Peer;
import lso.monitor.model.Stream;
import lso.monitor.model.Tracker;
import edu.uci.ics.jung.algorithms.layout.StaticLayout;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.layout.LayoutTransition;
import edu.uci.ics.jung.visualization.util.Animator;

public class StreamVisualization extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7398092146832121445L;

	private Tracker mCurrentTracker = null;
	private Stream mCurrentStream = null;
	private BasicLayout layout;
	private VisualizationViewer<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>> vv;
	private VertexColorTransformer mColors = new VertexColorTransformer();
	private PeerVertexNumberTransformer mPeerNumerator = new PeerVertexNumberTransformer();

	private HashSet<PeerSelectionChangeListener> mSelectionListeners = new HashSet<PeerSelectionChangeListener>();
	private Timer pTimer;

	private MousePopupPlugin mpp = new MousePopupPlugin();

	public StreamVisualization() {
		setLayout(new BorderLayout());

		layout = new LevelLayout();
		layout.setSize(new Dimension(500, 500));

		vv = new VisualizationViewer<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>>(
				layout);

		DefaultModalGraphMouse<String, String> gm = new DefaultModalGraphMouse<String, String>();
		gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
		gm.add(mpp); // MousePopupPlugin
		mpp.addListener(new MousePopupPluginListener() { // deselect Peer if
															// terminate/kill
			@Override
			public void terminatedPeer(Peer p) {
				selectPeer(p, false);
			}

			@Override
			public void killedPeer(Peer p) {
				selectPeer(p, false);
			}
		});

		gm.add(new MouseSelectionPlugin(this));
		vv.setGraphMouse(gm);

		vv.setVertexToolTipTransformer(new PeerVertexToolTipTransformer());
		vv.getRenderContext().setVertexFillPaintTransformer(mColors);
		vv.getRenderContext().setVertexLabelTransformer(mPeerNumerator);
		vv.getRenderContext().setVertexIconTransformer(
				new VertexIconTransformer(mColors));
		vv.getRenderContext().setVertexShapeTransformer(
				new VertexShapeTransformer(vv.getRenderContext()
						.getVertexShapeTransformer()));

		add(vv, BorderLayout.CENTER);
	}

	public void setStream(Tracker tracker, Stream stream) {
		mCurrentTracker = tracker;
		mCurrentStream = stream;

		mColors.reset();
		mPeerNumerator.setStream(mCurrentStream);

		forceRefreshGraph();
	}

	private void forceRefreshGraph() {
		layout.setStream(mCurrentStream);

		layout.initialize();
		vv.setGraphLayout(new StaticLayout<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>>(
				layout.getGraph(), layout, layout.getSize()));
		vv.repaint();
	}

	public Stream getStream() {
		return mCurrentStream;
	}

	public void setSize(Dimension size) {
		super.setSize(size);
		layout.setSize(size);
		layout.initialize();
		vv.setSize(size);
		vv.repaint();
	}

	public void setStripeLayout(int[] sl) {
		if (layout == null)
			return;

		layout.setStripeLayout(sl);
		Layouts.setSessionFavorite(getStream().getStripes().length, sl);

		forceRefreshGraph();
	}

	public int[] getStripeLayout() {
		if (layout == null)
			return null;
		return layout.getStripeLayout();
	}

	public void setLayout(BasicLayout layout) {
		layout.setStream(mCurrentStream);
		layout.setStripeLayout(this.layout.getStripeLayout());
		layout.setSize(this.layout.getSize());
		this.layout = layout;

		forceRefreshGraph();
	}

	public void addListener(PeerSelectionChangeListener l) {
		if ((l != null) && (!mSelectionListeners.contains(l)))
			mSelectionListeners.add(l);
	}

	public void removeListener(PeerSelectionChangeListener l) {
		mSelectionListeners.remove(l);
	}

	public void selectPeer(Peer p, Boolean selected) {
		if (selected) {
			setPeerColor(p, null);
		} else {
			mColors.removeColor(p);
		}

		for (MultiTreeVertex<Peer, Integer> v : layout.getGraph().getVertices())
			if (v.getVertex() == p)
				vv.getPickedVertexState().pick(v, selected);

		for (PeerSelectionChangeListener l : mSelectionListeners)
			l.handleSelectChanged(p, selected);
	}

	public Color getPeerColor(Peer p) {
		return mColors.getColor(p);
	}

	public void setPeerColor(Peer p, Color c) {
		mColors.setColor(p, c);
		vv.repaint();
	}

	public void updateStream() {
		if ((mCurrentStream != null)
				&& (mCurrentStream.updateStructure(mCurrentTracker))) {
			try {
				layout.initialize();
				StaticLayout<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>> slayout = new StaticLayout<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>>(
						layout.getGraph(), layout);
				LayoutTransition<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>> lt = new LayoutTransition<MultiTreeVertex<Peer, Integer>, MultiTreeEdge<Peer, Integer>>(
						vv, vv.getGraphLayout(), slayout);
				Animator animator = new Animator(lt);
				animator.start();

				vv.repaint();
			} catch (Exception e) {
				System.out.println("caught exception '" + e.getClass()
						+ "' in StreamVisualization");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	public void setRefreshRate(int ms) {
		if ((ms <= 0) && (pTimer != null)) {
			pTimer.stop();
			pTimer = null;
			return;
		}

		if (pTimer == null) {
			pTimer = new Timer(ms, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					pTimer.stop();
					Thread T = new Thread(new Runnable() {
						@Override
						public void run() {
							updateStream();
							pTimer.stop();
							pTimer.start();
						}
					});
					T.run();
				}
			});
			pTimer.start();
			return;
		} else {
			pTimer.setDelay(ms);
			pTimer.setInitialDelay(ms);
			pTimer.restart();
		}
	}

	public int getRefreshRate() {
		if (pTimer == null)
			return 0;
		return pTimer.getDelay();
	}

	public MousePopupPlugin getMousePopupPlugin() {
		return mpp;
	}
}
