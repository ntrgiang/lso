package lso.monitor.swingui;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.NumberFormat;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.NumberFormatter;

import lso.monitor.config.PeerProperty;

public class PeerPropertyComponent extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1946117445282660761L;
	private PeerPropertiesPanel mParent;
	private PeerProperty mProperty;
	private JComponent mControl;

	public PeerPropertyComponent(PeerPropertiesPanel parent, String propertyname) {
		mParent = parent;
		mProperty = lso.monitor.config.PeerProperties.getProperty(propertyname);

		((FlowLayout) getLayout()).setAlignment(FlowLayout.LEADING);

		if (mProperty == null) {
			mProperty = new PeerProperty();
			mProperty.setName(propertyname);
			mProperty.setType("String");
		}
		// setBackground(Color.red);
		createControls();

		reset();
	}

	private void createControls() {

		mControl = null;

		if (mProperty.isReadonly()) {
			mControl = new JTextArea();
			((JTextArea) mControl).setEditable(false);
		} else {
			switch (mProperty.getComponentType()) {
			case BOOLEAN:
				mControl = new JCheckBox("");

				((JCheckBox) mControl).addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						mParent.enableButtons(true);
					}
				});
				break;
			case COMBOBOX:
				mControl = new JComboBox();
				((JComboBox) mControl).setEditable(!mProperty.hasSteps());
				if ((mProperty.hasMin()) && (mProperty.hasMax()))
					for (double value = mProperty.getMin(); value <= mProperty
							.getMax(); value += mProperty.getSteps())
						((JComboBox) mControl).addItem(value);

				((JComboBox) mControl).addItemListener(new ItemListener() {
					@Override
					public void itemStateChanged(ItemEvent arg0) {
						mParent.enableButtons(true);
					}
				});
				break;
			case SLIDER:
				mControl = new JSlider(mProperty.getMin().intValue(), mProperty
						.getMax().intValue());
				((JSlider) mControl).setMinorTickSpacing(mProperty.getSteps()
						.intValue());

				((JSlider) mControl).addChangeListener(new ChangeListener() {
					@Override
					public void stateChanged(ChangeEvent arg0) {
						mParent.enableButtons(true);
					}
				});
				break;
			case NUMBERFIELD:
				NumberFormat nf = NumberFormat.getInstance();
				nf.setGroupingUsed(false);
				NumberFormatter nfer = new NumberFormatter(nf);
				if (mProperty.hasMax())
					nfer.setMaximum(mProperty.getMax());
				if (mProperty.hasMin())
					nfer.setMaximum(mProperty.getMin());

				mControl = new JFormattedTextField(nfer);
				((JFormattedTextField) mControl).setColumns(10);

				((JFormattedTextField) mControl)
						.addCaretListener(new CaretListener() {
							@Override
							public void caretUpdate(CaretEvent arg0) {
								String value = "";
								if (mParent.getPeerObject().hasProperty(
										mProperty.getName()))
									value = mParent.getPeerObject()
											.getPropertyValue(
													mProperty.getName());
								if (!((JTextField) mControl).getText().equals(
										value))
									mParent.enableButtons(true);
							}
						});
				break;
			default:
			case TEXTFIELD:
				mControl = new JTextField();
				((JTextField) mControl).setColumns(10);

				((JTextField) mControl).addCaretListener(new CaretListener() {
					@Override
					public void caretUpdate(CaretEvent arg0) {
						String value = "";
						if (mParent.getPeerObject().hasProperty(
								mProperty.getName()))
							value = mParent.getPeerObject().getPropertyValue(
									mProperty.getName());
						if (!((JTextField) mControl).getText().equals(value))
							mParent.enableButtons(true);
					}
				});
				break;
			}
		}

		if (mControl != null) {
			add(new JLabel(mProperty.getDisplayName()));
			add(mControl);
		}
	}

	public void reset() {
		if (mControl == null)
			return;

		String value;
		if (mParent.getPeerObject().hasProperty(mProperty.getName()))
			value = mParent.getPeerObject().getPropertyValue(
					mProperty.getName());
		else
			value = mProperty.getDefault();

		try {
			if (mControl instanceof JTextField) {
				((JTextField) mControl).setText(value);
			} else if (mControl instanceof JTextArea) {
				((JTextArea) mControl).setText(value);
			} else if (mControl instanceof JCheckBox) {
				((JCheckBox) mControl).setSelected(Boolean.parseBoolean(value));
			} else if (mControl instanceof JComboBox) {
				((JComboBox) mControl).setSelectedItem(Double
						.parseDouble(value));
			}
		} catch (Exception ex) {
		}

		repaint();
	}

	public void apply() {
		if (mControl == null)
			return;

		if (mControl instanceof JTextField) {
			mParent.getPeerObject().setPropertyValue(mProperty.getName(),
					((JTextField) mControl).getText());
		} else if (mControl instanceof JCheckBox) {
			mParent.getPeerObject().setPropertyValue(mProperty.getName(),
					String.valueOf(((JCheckBox) mControl).isSelected()));
		} else if (mControl instanceof JComboBox) {
			mParent.getPeerObject().setPropertyValue(mProperty.getName(),
					String.valueOf(((JComboBox) mControl).getSelectedItem()));
		}
	}
}
