package lso.monitor.swingui;

import lso.monitor.model.Peer;

public interface PeerSelectionChangeListener {

	public abstract void handleSelectChanged(Peer p, Boolean newState);
}
