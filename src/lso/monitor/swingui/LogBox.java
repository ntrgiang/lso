package lso.monitor.swingui;

import java.awt.GridLayout;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class LogBox extends JPanel implements LogListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -979391749794418725L;
	private JTextArea text = new JTextArea();

	public LogBox() {
		text.setEditable(false);
		setLayout(new GridLayout(1, 1));
		add(new JScrollPane(text));
	}

	@Override
	public void addLine(String line) {
		text.append("[" + (new SimpleDateFormat("HH:mm:ss")).format(new Date())
				+ "] " + line + "\n");
		text.select(text.getText().length(), text.getText().length());
	}

}
