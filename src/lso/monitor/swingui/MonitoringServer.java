package lso.monitor.swingui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SpringLayout;

import lso.Address;
import lso.monitor.MonitorRunner;
import lso.monitor.Network;
import lso.monitor.config.Config;
import lso.monitor.config.Layouts;
import lso.monitor.extern.SpringUtilities;
import lso.monitor.graph.LevelLayout;
import lso.monitor.graph.TreeLayout;
import lso.monitor.graph.plugins.MousePopupPluginListener;
import lso.monitor.model.Peer;
import lso.monitor.model.Stream;
import lso.monitor.model.Tracker;

public class MonitoringServer extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3685790986347693869L;

	MonitorRunner mParent;

	// Components

	// Splitters
	JSplitPane splitterLeftGraph;
	JSplitPane splitterSettingsPeers;
	JSplitPane splitterGraphLog;

	JPanel panelSettings;
	JPanel panelGraph;
	JPanel panelLog;
	// Settings Panel
	JLabel lblTracker;
	JComboBox cmbTracker;
	JLabel lblStream;
	JComboBox cmbStream;
	JLabel lblStripeLayouts;
	JComboBox cmbStripeLayouts;
	JLabel lblLayouts;
	JComboBox cmbLayouts;
	JLabel lblRefresh;
	JComboBox cmbRefresh;
	JButton btnRefreshNow;

	PeerPropertiesPanelManager peerManager;
	// Graph Panel
	StreamVisualization sv;

	int defaultPeerBandwidth;

	private boolean dontFireEvents = false;

	public MonitoringServer(MonitorRunner parent) {
		super("Monitor");

		mParent = parent;

		setSize(1024, 768);

		initializeSettingsPanel();
		initializeGraphPanel();
		peerManager = new PeerPropertiesPanelManager(this);
		panelSettings.setMinimumSize(new Dimension(240, 240));
		panelGraph.setMinimumSize(new Dimension(150, 200));

		splitterSettingsPeers = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
				panelSettings, peerManager);
		splitterSettingsPeers.setDividerLocation(panelSettings.getHeight());
		splitterSettingsPeers.setOneTouchExpandable(true);

		panelLog = new LogBox();
		if (Network.last != null)
			Network.last.addListener((LogListener) panelLog);
		panelLog.setMaximumSize(new Dimension(200, 200));

		if (Config.isLogTop())
			splitterGraphLog = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
					panelLog, panelGraph);
		else
			splitterGraphLog = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
					panelGraph, panelLog);
		splitterGraphLog.setOneTouchExpandable(true);

		splitterLeftGraph = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
				splitterSettingsPeers, splitterGraphLog);
		splitterLeftGraph.setDividerLocation(220);
		splitterLeftGraph.setOneTouchExpandable(true);

		getContentPane().add(splitterLeftGraph);

		refreshStreamBox();
		if (getTracker() != null)
			addItemToBox(cmbTracker, getTracker().getAddress().toString()
					.toLowerCase());
	}

	private void initializeSettingsPanel() {
		panelSettings = new JPanel(new GridLayout(1, 1));
		panelSettings.setBorder(BorderFactory.createTitledBorder("Settings"));

		final JPanel subSettings = new JPanel(new SpringLayout());

		// Tracker
		lblTracker = new JLabel("Tracker:");
		subSettings.add(lblTracker);

		cmbTracker = new JComboBox();
		subSettings.add(cmbTracker);
		cmbTracker.setEditable(true);
		cmbTracker.setSelectedItem(getTracker().getAddress().toString());
		cmbTracker.addActionListener(this);

		// Stream
		lblStream = new JLabel("Stream:");
		subSettings.add(lblStream);

		cmbStream = new JComboBox();
		subSettings.add(cmbStream);
		cmbStream.addActionListener(this);

		// Root-Layouts
		lblStripeLayouts = new JLabel("Layout:");
		subSettings.add(lblStripeLayouts);

		cmbStripeLayouts = new JComboBox();
		subSettings.add(cmbStripeLayouts);
		cmbStripeLayouts.setEditable(false);
		cmbStripeLayouts.addActionListener(this);

		cmbStripeLayouts.setRenderer(new LayoutBoxRenderer());
		cmbStripeLayouts.setEditor(new LayoutBoxEditor());
		cmbStripeLayouts.setEnabled(false);

		// Layouts
		lblLayouts = new JLabel("Architecture:");
		subSettings.add(lblLayouts);

		cmbLayouts = new JComboBox();
		subSettings.add(cmbLayouts);
		cmbLayouts.setEditable(false);
		cmbLayouts.addActionListener(this);
		cmbLayouts.addItem("Level");
		cmbLayouts.addItem("Tree");

		// Layouts
		lblRefresh = new JLabel("Refresh:");
		subSettings.add(lblRefresh);

		cmbRefresh = new JComboBox();
		subSettings.add(cmbRefresh);
		cmbRefresh.setEditable(true);
		cmbRefresh.addActionListener(this);
		cmbRefresh.addItem("disabled");
		cmbRefresh.addItem("1s");
		cmbRefresh.addItem("5s");

		subSettings.add(new JLabel(""));
		btnRefreshNow = new JButton("Refresh Now!");
		btnRefreshNow.addActionListener(this);
		subSettings.add(btnRefreshNow);

		SpringUtilities.makeCompactGrid(subSettings,
				subSettings.getComponentCount() / 2, 2, 5, 5, 5, 5);
		panelSettings.add(subSettings);
	}

	private void initializeGraphPanel() {
		panelGraph = new JPanel(new BorderLayout());
		sv = new StreamVisualization();
		sv.getMousePopupPlugin().addListener(new MousePopupPluginListener() {
			@Override
			public void terminatedPeer(Peer p) {
				refreshStreamBox();
			}

			@Override
			public void killedPeer(Peer p) {
				refreshStreamBox();
			}
		});
		panelGraph.add(sv, BorderLayout.CENTER);
	}

	private void addItemToBox(JComboBox box, String item) {
		for (int i = 0; i < box.getItemCount(); i++)
			if (box.getItemAt(i).equals(item))
				return;
		box.addItem(item);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (dontFireEvents)
			return;

		if (e.getSource() instanceof JComboBox) {
			JComboBox box = (JComboBox) e.getSource();
			if (box == cmbTracker) {
				try {
					Address addr = new Address(box.getSelectedItem().toString());
					mParent.switchTracker(addr);

					addItemToBox(box, addr.toString().toLowerCase());

					refreshStreamBox();
				} catch (Exception Ex) {
					JOptionPane.showMessageDialog(this,
							"Error: Could not parse Address! [host:port]",
							"Error", JOptionPane.ERROR_MESSAGE);
				}
			} else if ((box == cmbStream) && (getTracker() != null)
					&& (box.getSelectedIndex() >= 0)) {
				if (box.getSelectedIndex() < 0)
					return;

				Stream selected = getTracker().getStreams().get(
						box.getSelectedIndex());

				refreshStreamBox();

				if (!getTracker().getStreams().contains(selected)) {// Stream
																	// doesnt
																	// exist
																	// anymore
																	// ...
					if (box.getItemCount() > 0)
						box.setSelectedIndex(0);
				} else {
					if (sv.getStream() != selected) {
						sv.setStream(getTracker(), selected);
						peerManager.reset();
						refreshLayoutBox();
					}

					cmbStream.setSelectedItem(selected.getIdentifier());
					sv.updateStream();
				}
			} else if ((box == cmbStripeLayouts)) {
				if ((cmbStripeLayouts.getSelectedItem() == null)
						|| (sv.getStream() == null))
					return;

				Layouts.addLayout(sv.getStream().getStripes().length, Layouts
						.convertStringToArray((String) box.getSelectedItem()));
				sv.setStripeLayout(Layouts.convertStringToArray((String) box
						.getSelectedItem()));
				refreshLayoutBox();
			} else if (box == cmbLayouts) {
				if ((cmbLayouts.getSelectedItem() == null) || (sv == null))
					return;
				if (cmbLayouts.getSelectedItem().toString().toLowerCase()
						.equals("level"))
					sv.setLayout(new LevelLayout());
				else if (cmbLayouts.getSelectedItem().toString().toLowerCase()
						.equals("tree"))
					sv.setLayout(new TreeLayout());
			} else if (box == cmbRefresh) {
				String val = cmbRefresh.getSelectedItem().toString()
						.toLowerCase();
				if (sv == null)
					return;
				try {
					if (val.endsWith("ms") || !val.endsWith("s")) {
						val = val.replace("ms", "");
						Integer num = Integer.parseInt(val);
						setRefreshRate(num);
					} else if (val.endsWith("s")) {
						val = val.replace("s", "");
						Integer num = Integer.parseInt(val);
						setRefreshRate(num * 1000);
					} else
						setRefreshRate(0);
				} catch (Exception ex) {
					setRefreshRate(0);
				}

			}
		} else if (e.getSource() == btnRefreshNow) {
			sv.updateStream();
		}
	}

	private void refreshStreamBox() {
		if (cmbStream == null)
			return;

		dontFireEvents = true;
		cmbStream.removeAllItems();

		if (getTracker() != null) {
			if (getTracker().updateStreams()) {
				try {
					for (Stream s : getTracker().getStreams())
						cmbStream.addItem(s.getIdentifier());
				} catch (Exception Ex) {
				}
			} else {
				JOptionPane.showMessageDialog(this,
						"Error: Could not connect to Tracker!", "Error",
						JOptionPane.ERROR_MESSAGE);
			}
		}
		dontFireEvents = false;

		if (sv.getStream() != null)
			if (!getTracker().getStreams().contains(sv.getStream())) {// Stream
																		// doesnt
																		// exist
																		// anymore
																		// ...
				if (cmbStream.getItemCount() > 0)
					cmbStream.setSelectedIndex(0);
			} else
				cmbStream.setSelectedItem(sv.getStream().getIdentifier());
		else if (cmbStream.getItemCount() > 0)
			cmbStream.setSelectedIndex(0);

		cmbStream.setEnabled(cmbStream.getItemCount() > 0);
	}

	private void refreshLayoutBox() {
		if (cmbStripeLayouts == null)
			return;

		dontFireEvents = true;
		cmbStripeLayouts.removeAllItems();

		cmbStripeLayouts.setEnabled(sv.getStream() != null);
		cmbStripeLayouts.setEditable(sv.getStream() != null);
		if (sv.getStream() != null) {
			HashSet<int[]> l = Layouts
					.getLayouts(sv.getStream().getStripes().length);
			for (int[] layout : l)
				cmbStripeLayouts.addItem(Layouts.convertArrayToString(layout));
		}

		dontFireEvents = false;

		if (sv.getStripeLayout() != null)
			cmbStripeLayouts.setSelectedItem(Layouts.convertArrayToString(sv
					.getStripeLayout()));
	}

	public Tracker getTracker() {
		if (mParent == null)
			return null;
		return mParent.getCurrentTracker();
	}

	public StreamVisualization getStreamVisualization() {
		return sv;
	}

	public void setDefaultPeerBandwidth(int defaultPeerBandwidth) {
		this.defaultPeerBandwidth = defaultPeerBandwidth;
	}

	public int getDefaultPeerBandwidth() {
		return this.defaultPeerBandwidth;
	}

	public void setRefreshRate(Integer ms) {
		if (ms > 0) {
			String item = null;
			if ((ms % 1000) == 0)
				item = (new Integer(ms / 1000)).toString() + "s";
			else
				item = ms.toString() + "ms";
			addItemToBox(cmbRefresh, item);
			cmbRefresh.setSelectedItem(item);
		} else {
			cmbRefresh.setSelectedIndex(0); // disabled
		}
		btnRefreshNow.setEnabled(ms == 0);
		sv.setRefreshRate(ms);
	}
}
