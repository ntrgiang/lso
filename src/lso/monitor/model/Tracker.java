package lso.monitor.model;

import java.util.LinkedList;

import lso.Address;
import lso.monitor.Network;

public class Tracker {
	private Address mAddress;
	private LinkedList<Stream> mStreams = new LinkedList<Stream>();

	public Tracker(Address address) {
		mAddress = address;
	}

	public Address getAddress() {
		return mAddress;
	}

	public LinkedList<Stream> getStreams() {
		return mStreams;
	}

	public boolean updateStreams() {
		return ((Network.last != null) && (Network.last.updateStreams(this)));
	}
}
