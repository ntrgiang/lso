package lso.monitor.model;

import java.util.HashMap;
import java.util.HashSet;

import lso.Address;
import lso.monitor.Network;
import lso.net.response.maintenance.PeerList;
import edu.uci.ics.jung.graph.DelegateForest;

public class Stream implements Comparable<Stream> {

	private HashSet<Peer> mPeers = new HashSet<Peer>();
	private DelegateForest<Peer, Integer>[] mStripes;
	private Address mSource = null;
	private String mIdentifier;

	private int mCurrentPeerID = 1;
	private HashMap<Peer, Integer> mIDs = new HashMap<Peer, Integer>();

	@SuppressWarnings("unchecked")
	public Stream(int stripes, String identifier, Address source) {
		mStripes = new DelegateForest[stripes];
		for (int i = 0; i < stripes; i++)
			mStripes[i] = new DelegateForest<Peer, Integer>();
		mIdentifier = identifier;
		mSource = source;
		addPeer(new Peer(mSource));
	}

	public String getIdentifier() {
		return mIdentifier;
	}

	public Address getSource() {
		return mSource;
	}

	public Peer getSourcePeer() {
		return getPeer(mSource);
	}

	public void addPeer(Peer peer) {
		mPeers.add(peer);
		for (DelegateForest<Peer, Integer> f : mStripes)
			f.addVertex(peer);
	}

	public void removePeer(Peer peer) {
		mPeers.remove(peer);
		for (DelegateForest<Peer, Integer> f : mStripes)
			f.removeVertex(peer, false);
	}

	public HashSet<Peer> getPeers() {
		return mPeers;
	}

	public Peer getPeer(Address address) {
		for (Peer p : mPeers)
			if (p.getAddress().equals(address))
				return p;
		return null;
	}

	public DelegateForest<Peer, Integer>[] getStripes() {
		return mStripes;
	}

	public void setChild(Peer parent, Peer child, int stripe) {
		mStripes[stripe]
				.addEdge(mStripes[stripe].getEdgeCount(), parent, child);
	}

	public void setPeers(PeerList pl) {
		HashSet<Peer> removePeers = new HashSet<Peer>(mPeers);
		removePeers.remove(getSourcePeer());

		for (Address addr : pl.getPeers()) {
			Peer p = getPeer(addr);
			if (p == null) { // new Peer
				p = new Peer(addr);
				addPeer(p);
			} else {
				removePeers.remove(p);
			}
		}

		for (Peer p : removePeers) {
			mPeers.remove(p);
			for (DelegateForest<Peer, Integer> stripe : mStripes)
				stripe.removeVertex(p, false);
		}
	}

	public Integer getPeerID(Peer p) {
		if (p.getAddress().equals(mSource))
			return 0;

		if (!mIDs.containsKey(p)) {
			mIDs.put(p, mCurrentPeerID);
			mCurrentPeerID++;
		}

		return mIDs.get(p);
	}

	public Integer getHighestPeerID() {
		int max = 1;
		for (Integer ID : mIDs.values())
			if (ID > max)
				max = ID;
		return max;
	}

	public boolean updateStructure(Tracker t) {
		if ((Network.last == null) || (t == null))
			return false;
		Network.last.updateStreamStructure(t, this);
		return true;
	}

	@Override
	public int compareTo(Stream o) {
		return mIdentifier.compareTo(o.getIdentifier());
	}
}
