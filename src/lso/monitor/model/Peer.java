package lso.monitor.model;

import java.util.HashMap;

import lso.Address;
import lso.monitor.Network;

public class Peer {
	private Address mAddress;
	private HashMap<String, String> mProperties = new HashMap<String, String>();

	public Peer(Address address) {
		mAddress = address;
		Network.last.updatePeerInfo(this);
	}

	public Address getAddress() {
		return mAddress;
	}

	public boolean hasProperty(String prop) {
		return mProperties.containsKey(prop.toUpperCase());
	}

	public String getPropertyValue(String property) {
		return mProperties.get(property.toUpperCase());
	}

	public String getNodeType() {
		return getPropertyValue("TYPE");
	}

	public void setPropertyValue(String property, String value) {
		mProperties.put(property.toUpperCase(), value);
	}

	public HashMap<String, String> getProperties() {
		return mProperties;
	}

	public String getPeerName() {
		if (hasProperty("NAME"))
			return getPropertyValue("NAME");
		if (getAddress() == null)
			return "NOCONFIG PEER";
		return getAddress().toString();
	}

	@Override
	public String toString() {
		return getPeerName();
	}

	public String getPropertiesString() {
		String ret = "";

		for (String key : mProperties.keySet())
			ret += key + ": " + mProperties.get(key) + "\n";

		return ret;
	}

	public boolean updateProperties() {
		return (Network.last != null) && (Network.last.updatePeerInfo(this));
	}

	public boolean sendProperties() {
		return (Network.last != null)
				&& (Network.last.sendPeerProperties(this));
	}

	public boolean kill() {
		return (Network.last != null) && (Network.last.killPeer(this));
	}

	public boolean terminate() {
		return (Network.last != null) && (Network.last.terminatePeer(this));
	}
}
