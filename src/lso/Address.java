package lso;

/**
 * An Address represents an endpoint and consists of a hostname and a port.
 * 
 * @author Zoran Zaric <zz@zoranzaric.de>
 */
public class Address {
	public final static int MIN_PORT = 1;
	public final static int MAX_PORT = 65535;

	private String hostname;
	private int port;

	private void setHostname(String hostname) throws IllegalArgumentException {
		if (hostname.contains(":")) {
			throw new IllegalArgumentException(
					"A Addresses hostname must not contain a colon");
		}

		this.hostname = hostname;
	}

	public String getHostname() {
		return hostname;
	}

	private void setPort(int port) {
		if (port < MIN_PORT || port > MAX_PORT) {
			throw new IllegalArgumentException(
					"Illegal port. Must be between <" + MIN_PORT + "> and <"
							+ MAX_PORT + ">, was <" + port + ">");
		}

		this.port = port;
	}

	public int getPort() {
		return port;
	}

	public Address(String hostname, int port) throws IllegalArgumentException {
		this.setHostname(hostname);
		this.setPort(port);
	}

	public Address(String stringRepresentation) {
		String[] splittedStringRepresentation = stringRepresentation.split(":");

		if (splittedStringRepresentation.length != 2) {
			throw new IllegalArgumentException(
					"A Addresses hostname must not contain a colon");
		}

		String hostname = splittedStringRepresentation[0];
		int port = Integer.parseInt(splittedStringRepresentation[1]);

		this.setHostname(hostname);
		this.setPort(port);
	}

	@Override
	public boolean equals(Object obj) {
		if ((obj == null) || !(obj instanceof Address)) {
			return false;
		}

		Address address = (Address) obj;

		return (this.getHostname().equals(address.getHostname()) && (this
				.getPort() == address.getPort()));
	}

	@Override
	public String toString() {
		return this.getHostname() + ":" + this.getPort();
	}

	@Override
	public int hashCode() {
		return this.toString().hashCode();
	}

	public static final String dummyHost = "dummy";
	public static final int dummyPort = 1;

	public static Address getDummy() {
		return new Address(dummyHost, dummyPort);
	}

	public boolean isDummy() {
		return this.port == dummyPort && this.hostname.equals(dummyHost);
	}
}
