package lso;

import java.io.IOException;

import lso.log.Logger;
import lso.net.MaintenanceMessageReceiver;
import lso.net.MaintenanceMessageSender;
import lso.net.ProtocolBufferMaintenanceMessageReceiver;
import lso.net.ProtocolBufferMaintenanceMessageSender;
import lso.net.message.NotFoundException;
import lso.net.message.UnknownMessageException;
import lso.net.message.maintenance.MaintenanceMessage;
import lso.net.message.maintenance.MaintenanceMessageHandler;
import lso.net.response.maintenance.MaintenanceResponse;

public class MaintenanceNode implements Runnable {
	Logger logger;

	Address maintenanceAddress;

	MaintenanceMessageHandler messageHandler;

	MaintenanceMessageSender maintenanceMessageSender;
	MaintenanceMessageReceiver maintenanceMessageReceiver;

	public MaintenanceNode(Address maintenanceAddress) {
		// this.messageHandler = messageHandler;
		this.maintenanceAddress = maintenanceAddress;
		// this.maintenanceMessageSender = new
		// ProtocolBufferMaintenanceMessageSender();
		// this.maintenanceMessageReceiver = new
		// ProtocolBufferMaintenanceMessageReceiver(
		// maintenanceAddress, this.messageHandler);
	}

	public void setMessageHandler(MaintenanceMessageHandler messageHandler)
			throws IOException {
		this.messageHandler = messageHandler;
		this.maintenanceMessageSender = new ProtocolBufferMaintenanceMessageSender();
		this.maintenanceMessageReceiver = new ProtocolBufferMaintenanceMessageReceiver(
				maintenanceAddress, this.messageHandler);
	}

	public MaintenanceResponse send(Address maintenanceAddress,
			MaintenanceMessage maintenanceMessage) throws IOException,
			IllegalArgumentException, UnknownMessageException,
			NotFoundException {
		return this.maintenanceMessageSender.send(maintenanceAddress,
				maintenanceMessage);
	}

	protected void listen() {
		while (true) {
			try {
				this.maintenanceMessageReceiver.listen();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void run() {
		this.listen();
	}

	public void quit() {
		this.maintenanceMessageReceiver.quit();
	}
}
