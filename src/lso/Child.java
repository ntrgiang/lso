package lso;

public class Child {
	private int stripe;
	private ChildAddress childAddress;

	public int getStripe() {
		return this.stripe;
	}

	public ChildAddress getChildAddress() {
		return this.childAddress;
	}

	public Child(int stripe, ChildAddress childAddress) {
		this.stripe = stripe;
		this.childAddress = childAddress;
	}

	public String toString() {
		return "Child(" + this.stripe + ") "
				+ this.childAddress.getMaintenance();
	}

	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof Child)) {
			return false;
		}
		Child c = (Child) obj;
		return c.getChildAddress().equals(this.childAddress)
				&& c.getStripe() == this.stripe;
	}
}
