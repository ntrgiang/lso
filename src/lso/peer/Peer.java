package lso.peer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lso.Address;
import lso.Common;
import lso.Location;
import lso.Parent;
import lso.Stream;
import lso.source.Source;
import lso.streaming.Streamer;

public class Peer extends Source {
	protected boolean mobile;

	protected Parent[] parents;

	protected Location location;

	protected boolean locationChanged;

	protected String type;

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// CONSTRUCTOR
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public Peer(Address maintenanceAddress, Address dataAddress, Stream stream,
			String name, boolean mobile, int uploadBandwidth,
			Location location, Mode mode, Streamer streamer) {
		super(maintenanceAddress, dataAddress, stream, name, uploadBandwidth,
				mode, streamer);

		this.mobile = mobile;
		this.location = location;
		this.locationChanged = false;

		if (Common.isAndroid()) {
			this.type = "ANDROID";
		} else if (Common.isLinux()) {
			this.type = "LINUX";
		} else if (Common.isMac()) {
			this.type = "MAC";
		} else if (Common.isUnix()) {
			this.type = "UNIX";
		} else if (Common.isWindows()) {
			this.type = "WINDOWS";
		} else {
			this.type = "UNKNOWN";
		}
	}

	protected void init() {
		super.init();
		if (this.stream.getStripes() < 0) {
			this.parents = new Parent[0];
			return;
		}
		this.parents = new Parent[this.stream.getStripes()];
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// GETTER / SETTER
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public boolean isMobile() {
		return this.mobile;
	}

	public Map<String, String> getProperties() {
		Map<String, String> properties = this.getCommonProperties();
		String parents = "";
		for (Parent parent : this.parents) {
			if (parent != null) {
				parents += parent.getAddress().getPort() + " ";
			}
		}
		StringBuffer heights = new StringBuffer();
		for (int height : this.heights) {
			if (heights.length() == 0) {
				heights.append(height);
			} else {
				heights.append(" " + height);
			}
		}

		properties.put("DATA_ADDRESS", this.getDataAddress().toString());
		properties.put("DATA_PORTS",
				this.getRtspPort() + " - " + this.getRtpPort4());
		properties.put("MOBILE", this.mobile + "");
		properties.put("PARENTS", parents);
		properties.put("TYPE", this.type);
		properties.put("LOCATION", "" + this.location.getNumber());
		properties.put("HEIGHTS", heights.toString());

		return properties;
	}

	public boolean setProperties(Map<String, String> properties) {
		if (properties.containsKey("TYPE")) {
			this.type = properties.get("TYPE");
		}

		if (properties.containsKey("LOCATION")) {
			this.setLocation(new Location(properties.get("LOCATION")));
		}

		return super.setProperties(properties);
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.locationChanged = this.locationChanged
				|| !location.equals(this.location);
		if (this.locationChanged) {
			System.out.println("Change Location "
					+ this.location.getStringRepresentation() + " => "
					+ location.getStringRepresentation());
		}
		this.location = location;
	}

	public boolean isLocationChanged() {
		return this.locationChanged;
	}

	public void resetLocationChanged() {
		this.locationChanged = false;
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// PARENTS
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public Parent getParent(int stripe) {
		return this.parents[stripe];
	}

	public boolean setParent(int stripe, Address maintenanceAddress) {
		this.parents[stripe] = new Parent(stripe, maintenanceAddress);
		return true;
	}

	public void removeParent(int stripe) {
		this.parents[stripe] = null;
	}

	public Parent[] getParentList() {
		return this.parents;
	}

	public Parent[] getAvailableParents() {
		int counter = 0;
		for (Parent p : this.parents) {
			if (p != null) {
				counter++;
			}
		}
		Parent[] parents = new Parent[counter];
		int index = 0;
		for (Parent p : this.parents) {
			if (p != null) {
				parents[index++] = p;
			}
		}
		return parents;
	}

	public List<Integer> getStripesOfParent(Address ma) {
		ArrayList<Integer> list = new ArrayList<Integer>(this.parents.length);
		for (Parent parent : this.parents) {
			if (parent != null && ma.equals(parent.getAddress())) {
				list.add(parent.getStripe());
			}
		}
		return list;
	}

}
