package lso.peer;

import java.io.IOException;
import java.util.List;

import lso.Address;
import lso.Child;
import lso.Common;
import lso.MaintenanceNode;
import lso.Parent;
import lso.net.message.NotFoundException;
import lso.net.message.UnknownMessageException;
import lso.net.message.maintenance.ChangeLocation;
import lso.net.message.maintenance.Connect;
import lso.net.message.maintenance.GetPeerList;
import lso.net.message.maintenance.LeaveStripe;
import lso.net.message.maintenance.MaintenanceMessage;
import lso.net.message.maintenance.WhichConnections;
import lso.net.response.ReturnCode;
import lso.net.response.maintenance.Connections;
import lso.net.response.maintenance.MaintenanceResponse;
import lso.net.response.maintenance.PeerList;
import lso.source.Source.Mode;
import lso.source.SourceTopologyCheck;

public class PeerTopologyCheck extends SourceTopologyCheck {
	private Peer peer;

	public PeerTopologyCheck(Peer peer, MaintenanceNode maintenanceNode,
			long wait) {
		super(peer, maintenanceNode, wait);
		this.peer = peer;
	}

	@Override
	public void run() {
		this.turn = 0;

		while (true) {
			try {
				Thread.sleep(this.wait
						+ this.peer.getRandom().nextInt((int) this.wait));

				if (this.isTurn(Common.TURN_LOCATION)) {
					this.checkLocationChanged();
				}

				if (this.isTurn(Common.TURN_LOOPS)) {
					this.checkForSelfLoops();
					this.checkForLoops();
				}

				if (this.isTurn(Common.TURN_CHILDREN_ALIVE)) {
					this.checkChildrenAlive();
				}
				if (this.isTurn(Common.TURN_CHILDREN_CONNECTIONS)) {
					this.checkChildrenConnections();
				}

				if (this.isTurn(Common.TURN_PARENTS_ALIVE)) {
					this.checkParentsAlive();
				}
				if (this.isTurn(Common.TURN_PARENTS_CONNECTIONS)) {
					this.checkParentsConnections();
				}
				if (this.isTurn(Common.TURN_PARENTS_REJOIN)) {
					this.reconnectInStripesWithNoParent();
				}

				if (this.isTurn(Common.TURN_BANDWIDTH_FREE)) {
					this.checkFreeBandwidth();
				}
				if (this.isTurn(Common.TURN_BANDWIDTH_INSUFFICIENT)) {
					this.checkInsufficientBandwidth();
				}

				if (this.isTurn(Common.TURN_HEIGHTS)) {
					this.checkHeights();
				}

				if (this.isTurn(Common.TURN_UPDATE_STREAMER)) {
					this.updateStreamer();
				}

			} catch (InterruptedException e) {
				break;
			} catch (Exception e) {
				System.out.println("Caught exception in PeerTopologyCheck: "
						+ e.getClass());
				e.printStackTrace();
			}

			this.turn++;
		}
	}

	/**
	 * Checks if the peer's location has changed. If this is the case, a
	 * notification about the change is sent to the tracker. If the notification
	 * could be sent successfully, the peer leaves the overlay in order to
	 * rejoin (automatically done in findNewParents()).
	 * 
	 * @return true, if the tracker could be notified and a leave message was
	 *         successfully sent to all neighbors; false otherwise
	 */
	protected boolean checkLocationChanged() {
		if (!this.peer.isLocationChanged()) {
			return true;
		}
		System.out.println("Notifying tracker about changed location: "
				+ this.peer.getLocation());

		MaintenanceMessage msg = new ChangeLocation(this.peer.getLocation()
				.getStringRepresentation(), this.peer.getMaintenanceAddress());
		try {
			MaintenanceResponse r = this.maintenanceNode.send(this.peer
					.getStream().getTracker(), msg);
			if (r != null && r.getReturnCode() == ReturnCode.OK) {
				this.peer.resetLocationChanged();
				boolean success = true;
				for (int stripe = 0; stripe < this.peer.getStream()
						.getStripes(); stripe++) {
					success &= this.disconnectAll(stripe);
					this.peer.removeParent(stripe);
					Child[] children = this.peer.getChildren(stripe);
					for (Child child : children) {
						this.peer.removeChildInStripe(child);
					}
				}
				return success;
			}
		} catch (IllegalArgumentException e) {
		} catch (IOException e) {
		} catch (UnknownMessageException e) {
		} catch (NotFoundException e) {
		}
		System.out.println("  => ERROR");
		return false;
	}

	/**
	 * Checks if the peer is connected to itself (as parent or child). In case
	 * it has itself as parent, LeaveStripe is sent to all children. In case it
	 * has itself as child, it only send a disconnect to itself.
	 * 
	 * @return true, if everything is ok, i.e. no self-loop was found; false
	 *         otherwise
	 */
	protected boolean checkForSelfLoops() {
		boolean ok = true;
		Address ma = this.peer.getMaintenanceAddress();
		for (int stripe = 0; stripe < this.peer.getStream().getStripes(); stripe++) {
			Parent p = this.peer.getParent(stripe);
			if (ma.equals(p)) {
				System.out.println("self loop found: " + p.getAddress()
						+ " is parent in stripe " + stripe);
				this.disconnectAll(stripe);
				ok = false;
				continue;
			}
			for (Child child : this.peer.getChildren(stripe)) {
				Address ca = child.getChildAddress().getMaintenance();
				if (ma.equals(ca)) {
					System.out.println("self loop found: " + ca
							+ " is child in stripe " + stripe);
					this.disconnect(stripe, ca, Address.getDummy());
					ok = false;
				}
			}
		}
		return ok;
	}

	/**
	 * Checks if the parent in a stripe is also a child in the same stripe. If
	 * it is the case, LeaveStripe is sent to all neighbors.
	 * 
	 * @return true, if everything is ok, i.e. no loop was found; false
	 *         otherwise
	 */
	protected boolean checkForLoops() {
		boolean ok = true;
		for (int stripe = 0; stripe < this.peer.getStream().getStripes(); stripe++) {
			Parent parent = this.peer.getParent(stripe);
			if (parent == null) {
				continue;
			}
			if (!this.peer.hasChild(stripe, parent.getAddress())) {
				continue;
			}
			System.out.println("loop found: " + parent.getAddress()
					+ " is parent/child in stripe " + stripe);
			ok = false;
			this.disconnectAll(stripe);
		}
		return ok;
	}

	/**
	 * Checks if all parents are still online. If a peers does not answer it is
	 * assumed to have gone offline without a notification and is removed as
	 * parent in the respective stripe.
	 * 
	 * @return true, if all parents are online; false otherwise
	 */
	protected boolean checkParentsAlive() {
		boolean success = true;
		for (Parent parent : this.peer.getAvailableParents()) {
			Address address = parent.getAddress();
			if (!this.isAlive(address)) {
				System.out.println("parent '" + address + "' in stripe "
						+ parent.getStripe() + " is not alive");
				this.peer.removeParent(parent.getStripe());
				success = false;
			}
		}
		return success;
	}

	/**
	 * Checks if all parents have this peer as a child in the respective stream.
	 * If a peer that is assumed as a parent does not have this peer listed as a
	 * child in the stripe, the parent is removed as the parent in the
	 * respective stripe.
	 * 
	 * @return true, if all parents know this peer as their child; false
	 *         otherwise
	 */
	protected boolean checkParentsConnections() {
		boolean success = true;
		for (Parent parent : this.peer.getAvailableParents()) {
			if (!this.hasPeerAsChild(parent)) {
				System.out.println("Parent '" + parent.getAddress()
						+ "' in stripe " + parent.getStripe()
						+ " does not have me as child");
				this.peer.removeParent(parent.getStripe());
				success = false;
			}
		}
		return success;
	}

	/**
	 * Checks if this peer has additional bandwidth left over to server another
	 * child in any stripe. If this is the case, another child is requested
	 * (moved up) from another child in a randomly chosen stripe.
	 * 
	 * @return
	 */
	protected boolean checkFreeBandwidth() {

		int available = this.peer.getMaxNumberOfChildren();
		int used = this.peer.getChildrenCount();

		// pull child up as long as bandwidth is available
		while (used < available) {

			boolean found = false;
			// select a stripe at random
			List<Integer> stripeList = Common.getRandomOrder(this.peer
					.getStream().getStripes(), this.peer.getRandom());
			for (int stripe : stripeList) {
				Child[] children = this.peer.getChildren(stripe);
				Child newChild = this.requestNewChild(children, stripe);
				if (newChild == null) {
					continue;
				}
				if (!this.connectToNewChild(newChild, stripe, used, available)) {
					continue;
				}
				found = true;
				break;
			}

			if (!found) {
				return false;
			}

			if (!Common.pullUpMultiple) {
				return true;
			}

			used = this.peer.getChildrenCount();

		}

		return true;
	}

	/**
	 * Checks if this peer has too many children according to its currently
	 * available bandwidth.
	 * 
	 * @return true, if there is sufficient bandwidth or a child could be
	 *         forwarded (moved down); false otherwise
	 */
	protected boolean checkInsufficientBandwidth() {

		int available = this.peer.getMaxNumberOfChildren();
		int used = this.peer.getChildrenCount();

		/**
		 * if no bandwidth is available, send a disconnect to all children
		 */
		if (available == 0) {
			boolean success = true;
			for (int stripe = 0; stripe < this.peer.getStream().getStripes(); stripe++) {
				Address parent = this.peer.getParent(stripe) == null ? Address
						.getDummy() : this.peer.getParent(stripe).getAddress();
				success &= this.disconnectAllChildren(stripe, parent);
			}
			return success;
		}

		/**
		 * if the maximum number of children in all stripes is 1, send
		 * disconnects to random children in order to free up bandwidth
		 */
		if (this.isMaxOneChildPerStripe()) {
			while (used > available) {
				List<Integer> stripeList = Common.getRandomOrder(this.peer
						.getStream().getStripes(), this.peer.getRandom());
				for (int stripe : stripeList) {
					Child[] c = this.peer.getChildren(stripe);
					if (c.length == 0) {
						continue;
					}
					Address parent = this.peer.getParent(stripe) == null ? Address
							.getDummy() : this.peer.getParent(stripe)
							.getAddress();
					if (!this.disconnect(stripe, c[0].getChildAddress()
							.getMaintenance(), parent)) {
						return false;
					}

					if (!Common.pushDownMultiple) {
						return true;
					}
				}

				used = this.peer.getChildrenCount();
			}
		}

		/**
		 * free up bandwidth until used == available by pushing children down
		 * the tree, exit in case only one child per tree is left, hence
		 * forwarding is not possible
		 */
		while (used > available) {
			if (this.isMaxOneChildPerStripe()) {
				return false;
			}

			List<Integer> stripeList = Common.getRandomOrder(this.peer
					.getStream().getStripes(), this.peer.getRandom());
			for (int stripe : stripeList) {
				Child[] c = this.peer.getTwoRandomChildren(stripe);
				if (c == null || c.length != 2) {
					continue;
				}
				if (!this.forwardChild(c, stripe, used, available)) {
					return false;
				}

				if (!Common.pushDownMultiple) {
					return true;
				}

				break;
			}

			used = this.peer.getChildrenCount();
		}

		return true;

		// int used = this.peer.getChildrenCount();
		// int available = this.peer.getMaxNumberOfChildren();
		//
		// if (used <= available) {
		// return true;
		// }
		//
		// /**
		// * if no bandwidth is available, send a disconnect to all children
		// */
		// if (available == 0) {
		// boolean success = true;
		// for (int stripe = 0; stripe < this.peer.getStream().getStripes();
		// stripe++) {
		// Address parent = this.peer.getParent(stripe) == null ? Address
		// .getDummy() : this.peer.getParent(stripe).getAddress();
		// success &= this.disconnectAllChildren(stripe, parent);
		// }
		// return success;
		// }
		//
		// List<Integer> stripeList =
		// Common.getRandomOrder(this.peer.getStream()
		// .getStripes(), this.peer.getRandom());
		//
		// /**
		// * if the maximum number of children in all stripes is 1, send a
		// * disconnect to a child in one stripe to free up bandwidth
		// */
		// boolean maxOneChildPerStripe = true;
		// for (int stripe = 0; stripe < this.peer.getStream().getStripes();
		// stripe++) {
		// if (this.peer.getChildrenCount(stripe) > 1) {
		// maxOneChildPerStripe = false;
		// }
		// }
		//
		// if (maxOneChildPerStripe) {
		// for (int stripe : stripeList) {
		// Child[] c = this.peer.getChildren(stripe);
		// if (c.length == 0) {
		// continue;
		// }
		// Address parent = this.peer.getParent(stripe) == null ? Address
		// .getDummy() : this.peer.getParent(stripe).getAddress();
		// return this.disconnect(stripe, c[0].getChildAddress()
		// .getMaintenance(), parent);
		// }
		// }

		// /**
		// * find a stripe with at least two children and forward one child to
		// * another
		// */
		// for (int stripe : stripeList) {
		// Child[] c = this.peer.getTwoRandomChildren(stripe);
		// if (c == null || c.length != 2) {
		// continue;
		// }
		// return this.forwardChild(c, stripe, used, available);
		// }
		// return false;
	}

	protected boolean isMaxOneChildPerStripe() {
		boolean maxOneChildPerStripe = true;
		for (int stripe = 0; stripe < this.peer.getStream().getStripes(); stripe++) {
			if (this.peer.getChildrenCount(stripe) > 1) {
				maxOneChildPerStripe = false;
			}
		}
		return maxOneChildPerStripe;
	}

	/**
	 * in case there is no parent in a stripe anymore (should happen only in
	 * case of a failure / KILL), the peer reconnects to the source directly
	 * 
	 * @return true, if the connection could be established or there are parents
	 *         in all stripes; false otherwise
	 */
	protected boolean reconnectInStripesWithNoParent() {
		boolean success = true;
		for (int stripe = 0; stripe < this.peer.getStream().getStripes(); stripe++) {
			if (this.peer.getParent(stripe) == null) {
				this.connect(stripe, this.peer.getStream().getSource());
			}
		}
		return success;
	}

	/**
	 * Sends a LeaveStripe message to all children in a stripe where this peer
	 * currently has no parent.
	 * 
	 * @return true, if no such stripe exists; false otherwise
	 */
	// protected boolean leaveStripesWithNoParent() {
	// boolean success = true;
	// for (int stripe = 0; stripe < this.peer.getStream().getStripes();
	// stripe++) {
	// if (this.peer.getParent(stripe) == null) {
	// this.disconnectAll(stripe);
	// success = false;
	// }
	// }
	// return success;
	// }

	/**
	 * Attempts to find a new parent for every stripe where currently no parent
	 * is assigned.
	 * 
	 * @return true, if no parents are missing or a new parent could be found in
	 *         every stripe where no parent is currently assigned
	 */
	// protected boolean findNewParents() {
	// boolean success = true;
	// Address[] peerList = null;
	// for (int stripe = 0; stripe < this.peer.getStream().getStripes();
	// stripe++) {
	// if (this.peer.getParent(stripe) != null) {
	// continue;
	// }
	// if (peerList == null) {
	// peerList = this.getPeerList();
	// }
	// success &= this.findNewParent(stripe, peerList);
	// }
	// return success;
	// }

	/**
	 * Attempts to find a new parent in the given stripe.
	 * 
	 * @param stripe
	 *            stripe to find a new parent in
	 * @param peerList
	 *            list of peers retrieved from the tracker
	 * @return true, if a new parent could be found for the given stripe; false
	 *         otherwise
	 */
	// protected boolean findNewParent(int stripe, Address[] peerList) {
	// if (peerList == null) {
	// System.err.println("Could not retrieve PeerList");
	// return false;
	// }
	//
	// if (peerList.length == 0) {
	// System.err.println("Retrieved empty PeerList");
	// return false;
	// }
	//
	// List<Integer> order = Common.getRandomOrder(peerList.length,
	// this.peer.getRandom());
	// for (int index : order) {
	//
	// Address newParent = peerList[index];
	// if (newParent.equals(this.peer.getMaintenanceAddress())) {
	// continue;
	// }
	// for (Child child : this.peer.getChildren(stripe)) {
	// if (newParent.equals(child.getChildAddress().getMaintenance())) {
	// continue;
	// }
	// }
	//
	// if (this.connect(stripe, newParent)) {
	// return true;
	// }
	// }
	// System.err.println("could not find a new parent for stripe " + stripe);
	// return false;
	// }

	/**
	 * send a connect message for $stripe to the specified $newParent address.
	 * also, the new address is set as parent in the respective stripe if the
	 * connection request is accepted.
	 * 
	 * @param stripe
	 * @param newParent
	 * @return true, if the connection is accepted; false otherwise
	 */
	protected boolean connect(int stripe, Address newParent) {
		Address ma = this.peer.getMaintenanceAddress();
		Address da = this.peer.getDataAddress();

		MaintenanceMessage connect = new Connect(stripe, ma, da);
		try {
			MaintenanceResponse r = this.maintenanceNode.send(newParent,
					connect);
			if (r != null && r.getReturnCode() == ReturnCode.OK) {
				peer.setParent(stripe, newParent);
				System.out.println("connected to new parent '" + newParent
						+ "' in stripe " + stripe);
				return true;
			} else {
				peer.removeParent(stripe);
				System.err.println("could not connect to new parent '"
						+ newParent + "' in stripe " + stripe);
			}
		} catch (IllegalArgumentException e) {
		} catch (IOException e) {
		} catch (UnknownMessageException e) {
		} catch (NotFoundException e) {
		}
		return false;
	}

	/**
	 * Sends a LeaveStripe message to parent and children in the given stripe.
	 * 
	 * @param stripe
	 *            stripe to disconnect all neighbors in
	 * @return true, if the disconnect was successfully send to all neighbors;
	 *         false otherwise
	 */
	protected boolean disconnectAll(int stripe) {
		boolean success = true;
		Parent p = this.peer.getParent(stripe);
		if (p != null) {
			success &= this.disconnect(stripe, p.getAddress(),
					Address.getDummy());
		}
		success &= this.disconnectAllChildren(stripe, Address.getDummy());
		return success;
	}

	/**
	 * Sends a LeaveStripe message to all children in the given stripe.
	 * 
	 * @param stripe
	 *            stripe to disconnect all children in
	 * @return true, if the disconnect was successfully sent to all children;
	 *         false otherwise
	 */
	protected boolean disconnectAllChildren(int stripe, Address newParent) {
		boolean success = true;
		for (Child child : this.peer.getChildren(stripe)) {
			Address ca = child.getChildAddress().getMaintenance();
			success &= this.disconnect(stripe, ca, newParent);
		}
		return success;
	}

	/**
	 * Sends a LeaveStripe message to the given address for the specified stripe
	 * 
	 * @param stripe
	 *            stripe to disconnect in
	 * @param address
	 *            address of the peer to send the disconnect to
	 * @return true, if the disconnect was successfully sent; false otherwise
	 */
	protected boolean disconnect(int stripe, Address address, Address newParent) {
		if (address == null) {
			return false;
		}
		Address ma = this.peer.getMaintenanceAddress();
		LeaveStripe ls = new LeaveStripe(stripe, ma, newParent);
		try {
			MaintenanceResponse r = this.maintenanceNode.send(address, ls);
			if (r != null && r.getReturnCode() == ReturnCode.OK) {
				return true;
			}
		} catch (IllegalArgumentException e) {
		} catch (IOException e) {
		} catch (UnknownMessageException e) {
		} catch (NotFoundException e) {
		}
		return false;
	}

	/**
	 * Checks if the given parent has this peer listed as a child in the
	 * respective stripe.
	 * 
	 * @param parent
	 *            parent to check
	 * @return true, if the parent has this peer listed as child in the same
	 *         stripe; false otherwise
	 */
	protected boolean hasPeerAsChild(Parent parent) {
		Address addr = parent.getAddress();
		MaintenanceMessage msg = new WhichConnections(
				this.peer.getMaintenanceAddress());
		try {
			MaintenanceResponse r = this.maintenanceNode.send(addr, msg);
			Connections conn = (Connections) r;
			for (Child c : conn.getChild()) {
				if (c.getStripe() == parent.getStripe()
						&& c.getChildAddress().getMaintenance()
								.equals(this.peer.getMaintenanceAddress())) {
					int height = conn.getHeights()[parent.getStripe()] + 1;
					this.peer.setHeight(parent.getStripe(), height);
					return true;
				}
			}
			return false;
		} catch (IllegalArgumentException e) {
		} catch (IOException e) {
		} catch (UnknownMessageException e) {
		} catch (NotFoundException e) {
		}
		return false;
	}

	/**
	 * Retrieves a list of peers from the tracker.
	 * 
	 * @return list of peers retrieved from the tracker; null if no list of
	 *         peers could be retrieved.
	 */
	protected Address[] getPeerList() {
		Address tracker = this.peer.getStream().getTracker();
		Address ma = this.peer.getMaintenanceAddress();
		String sid = this.peer.getStream().getIdentifier();
		String location = this.peer.getLocation().getStringRepresentation();
		MaintenanceMessage gpl = new GetPeerList(sid, ma, location);
		try {
			MaintenanceResponse r = this.maintenanceNode.send(tracker, gpl);
			if (r != null && r.getReturnCode() == ReturnCode.OK) {
				return ((PeerList) r).getPeers();
			}
		} catch (IllegalArgumentException e) {
		} catch (IOException e) {
		} catch (UnknownMessageException e) {
		} catch (NotFoundException e) {
		}
		return null;
	}

	public static final int defaultHeight = -1;

	/**
	 * checks if there is a stripe with no parent and sets the stripe's height
	 * to $defaultHeight
	 * 
	 * @return true, if there is a parent set for each stripe; false otherwise
	 */
	protected boolean checkHeights() {
		boolean allOk = true;
		for (int stripe = 0; stripe < this.peer.getStream().getStripes(); stripe++) {
			if (this.peer.getParent(stripe) == null) {
				this.peer.setHeight(stripe, defaultHeight);
				allOk = false;
			}
		}
		return allOk;
	}
}
