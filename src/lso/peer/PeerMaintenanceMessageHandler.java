package lso.peer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lso.Address;
import lso.Child;
import lso.ChildAddress;
import lso.MaintenanceNode;
import lso.Parent;
import lso.net.message.NotFoundException;
import lso.net.message.UnknownMessageException;
import lso.net.message.maintenance.Alive;
import lso.net.message.maintenance.ChangeParent;
import lso.net.message.maintenance.Connect;
import lso.net.message.maintenance.ForwardChild;
import lso.net.message.maintenance.GetChildren;
import lso.net.message.maintenance.GetInfo;
import lso.net.message.maintenance.GetParents;
import lso.net.message.maintenance.GetPeerList;
import lso.net.message.maintenance.GetStreams;
import lso.net.message.maintenance.Kill;
import lso.net.message.maintenance.Leave;
import lso.net.message.maintenance.LeaveStripe;
import lso.net.message.maintenance.MGetAllPeers;
import lso.net.message.maintenance.MGetAllPeersForStream;
import lso.net.message.maintenance.MGetStreams;
import lso.net.message.maintenance.MaintenanceMessage;
import lso.net.message.maintenance.PAlive;
import lso.net.message.maintenance.RegisterStream;
import lso.net.message.maintenance.RequestChild;
import lso.net.message.maintenance.SAlive;
import lso.net.message.maintenance.SetProperties;
import lso.net.message.maintenance.Term;
import lso.net.message.maintenance.UnregisterStream;
import lso.net.message.maintenance.WhichConnections;
import lso.net.response.ReturnCode;
import lso.net.response.maintenance.Connections;
import lso.net.response.maintenance.MaintenanceResponse;
import lso.net.response.maintenance.Null;
import lso.net.response.maintenance.Parents;
import lso.source.SourceMaintenanceMessageHandler;

public class PeerMaintenanceMessageHandler extends
		SourceMaintenanceMessageHandler {

	private Peer peer;

	private PeerRunner peerRunner;

	public PeerMaintenanceMessageHandler(Peer peer, PeerRunner peerRunner,
			MaintenanceNode maintenanceNode) {
		super(peer, null, maintenanceNode);
		this.peer = peer;
		this.peerRunner = peerRunner;
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// peer
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handlePAlive(PAlive pAlive) {
		return super.handlePAlive(pAlive);
	}

	@Override
	protected MaintenanceResponse handleWhichConnections(
			WhichConnections whichConnections) {
		Address addr = whichConnections.getMaintenanceAddress();

		ArrayList<Child> listC = new ArrayList<Child>();
		for (Child child : this.peer.getChildren()) {
			if (addr.equals(child.getChildAddress().getMaintenance())) {
				listC.add(child);
			}
		}

		Child[] child = new Child[listC.size()];
		for (int i = 0; i < listC.size(); i++) {
			child[i] = listC.get(i);
		}

		ArrayList<Parent> listP = new ArrayList<Parent>();
		for (Parent parent : this.peer.getAvailableParents()) {
			if (addr.equals(parent.getAddress())) {
				listP.add(parent);
			}
		}

		Parent[] parent = new Parent[listP.size()];
		for (int i = 0; i < listP.size(); i++) {
			parent[i] = listP.get(i);
		}

		return new Connections(ReturnCode.OK, parent, child,
				this.peer.getHeights());
	}

	@Override
	protected MaintenanceResponse handleLeave(Leave leave) {
		Address ma = leave.getMaintenanceAddress();
		System.out.println("LEAVE from " + ma);

		/**
		 * send leaveStripe to all children in relevant stripes
		 */
		List<Integer> stripes = this.peer.getStripesOfParent(ma);
		for (int stripe : stripes) {
			this.handleParentLeave(stripe);
		}

		/**
		 * same behavior as source...
		 */
		return super.handleLeave(leave);
	}

	@Override
	protected MaintenanceResponse handleLeaveStripe(LeaveStripe leaveStripe) {
		int stripe = leaveStripe.getStripe();
		Address ma = leaveStripe.getMaintenanceAddress();
		Address newParent = leaveStripe.getNewParent();
		System.out.println("LEAVE_STRIPE from " + ma + " for " + stripe);

		if (!newParent.isDummy()) {
			/**
			 * rejoin stripe tree at $newParent
			 */

			MaintenanceMessage connect = new Connect(stripe,
					this.peer.getMaintenanceAddress(),
					this.peer.getDataAddress());
			try {
				MaintenanceResponse r = this.maintenanceNode.send(newParent,
						connect);
				if (r != null && r.getReturnCode() == ReturnCode.OK) {
					peer.setParent(stripe, newParent);
					System.out.println("connected to new parent '" + newParent
							+ "' for stripe " + stripe);
					return r;
				} else {
					peer.removeParent(stripe);
					System.err.println("could not connect to new parent '"
							+ newParent + "' in stripe " + stripe);
				}
			} catch (IllegalArgumentException e) {
			} catch (IOException e) {
			} catch (UnknownMessageException e) {
			} catch (NotFoundException e) {
			}

			return new Null(ReturnCode.CANNOT_CONNECT);
		} else {
			/**
			 * send leaveStripe to all children in this stripe
			 */
			Parent parent = this.peer.getParent(stripe);
			if (parent == null || ma.equals(parent.getAddress())) {
				this.handleParentLeave(stripe);
			}
			return super.handleLeaveStripe(leaveStripe);
		}
	}

	private void handleParentLeave(int stripe) {
		this.peer.removeParent(stripe);
		System.out.println("  => is parent in " + stripe);
		for (Child child : this.peer.getChildren(stripe)) {
			System.out.println("    => send leaveStripe to " + child);
			Address ca = child.getChildAddress().getMaintenance();
			this.peer.removeChildInStripe(stripe, ca);
			this.sendLeaveStripe(stripe, ca, Address.getDummy());
		}
	}

	private void sendLeaveStripe(int stripe, Address ma, Address newParent) {
		LeaveStripe leaveStripe2 = new LeaveStripe(stripe,
				this.peer.getMaintenanceAddress(), newParent);
		try {
			MaintenanceResponse response = this.maintenanceNode.send(ma,
					leaveStripe2);
			if (response.getReturnCode() != ReturnCode.OK) {
				System.out.println("    CANNOT SEND LeaveStripe: " + ma
						+ " \n    => " + response);
			}
			return;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnknownMessageException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		System.out.println("    -- CANNOT SEND LeaveStripe: " + ma);
	}

	@Override
	protected MaintenanceResponse handleConnect(Connect connect) {
		Address ma = connect.getMaintenanceAddress();
		int stripe = connect.getStripeIdentifier();

		/**
		 * connect from parent in this stripe
		 */
		if (ma.equals(this.peer.getParent(stripe))) {
			System.out
					.println("Connect from '" + ma + "' for stripe " + stripe);
			System.out.println("  => is parent in this stripe");
			this.peer.removeParent(stripe);
			return new Null(ReturnCode.CANNOT_CONNECT);
		}

		/**
		 * not connected to a parent in this stripe
		 */
		if (this.peer.getParent(stripe) == null) {
			System.out
					.println("Connect from '" + ma + "' for stripe " + stripe);
			System.out.println("  => no parent in this stripe");
			return new Null(ReturnCode.CANNOT_CONNECT);
		}

		/**
		 * same behavior as source
		 */
		return super.handleConnect(connect);
	}

	@Override
	protected MaintenanceResponse handleRequestChild(RequestChild requestChild) {
		int stripe = requestChild.getStripe();
		Child child = this.peer.getRandomChild(stripe);

		/**
		 * return Null if there is no child in this stripe
		 */
		if (child == null || child.getChildAddress() == null
				|| child.getChildAddress().getMaintenance() == null
				|| child.getChildAddress().getData() == null) {
			return new Null(ReturnCode.OK);
		}

		/**
		 * remove child from own list and return it
		 */
		this.peer.removeChildInStripe(child);
		return new lso.net.response.maintenance.Child(ReturnCode.OK,
				child.getChildAddress());
	}

	@Override
	protected MaintenanceResponse handleChangeParent(ChangeParent changeParent) {
		int stripe = changeParent.getStripeIdentifier();
		Address ma = changeParent.getMaintenanceAddress();

		System.out.println("ChangeParent to " + ma + " (before: "
				+ this.peer.getParent(stripe) + ")");

		/**
		 * new parent is peer itself
		 */
		if (ma.equals(this.peer.getMaintenanceAddress())) {
			System.out.println("  => itself");
			return new Null(ReturnCode.CANNOT_CONNECT);
		}

		/**
		 * new parent is child in this stripe
		 */
		if (this.peer.hasChild(stripe, ma)) {
			System.out.println("  => child in this stripe");
			return new Null(ReturnCode.CANNOT_CONNECT);
		}

		/**
		 * set new parent
		 */
		this.peer.setParent(stripe, ma);
		return new Null(ReturnCode.OK);
	}

	@Override
	protected MaintenanceResponse handleForwardChild(ForwardChild forwardChild) {
		int stripe = forwardChild.getStripeIdentifier();
		Address ma = forwardChild.getMaintenanceAddress();
		Address da = forwardChild.getDataAddress();

		System.out.println("ForwardChild of peer '" + ma + "' in stripe "
				+ stripe);

		/**
		 * peer itself
		 */
		if (ma.equals(this.peer.getMaintenanceAddress())) {
			System.out.println("  => itself");
			return new Null(ReturnCode.CANNOT_CONNECT);
		}

		/**
		 * already child in this stripe
		 */
		Child child = new Child(stripe, new ChildAddress(ma, da));
		if (this.peer.hasChild(child)) {
			System.out.println("  => already child in this stripe");
			return new Null(ReturnCode.OK);
		}

		/**
		 * child is already parent in this stripe
		 */
		if (ma.equals(this.peer.getParent(stripe))) {
			System.out.println("  => parent in this stripe");
			this.peer.removeParent(stripe);
			return new Null(ReturnCode.CANNOT_CONNECT);
		}

		/**
		 * accept new child
		 */
		this.peer.addChild(stripe, new ChildAddress(ma, da));

		return new Null(ReturnCode.OK);
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// monitor info
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleGetParents(GetParents getParents) {
		return new Parents(ReturnCode.OK, this.peer.getAvailableParents());
	}

	@Override
	protected MaintenanceResponse handleGetChildren(GetChildren getChildren) {
		return super.handleGetChildren(getChildren);
	}

	@Override
	protected MaintenanceResponse handleGetInfo(GetInfo getInfo) {
		return super.handleGetInfo(getInfo);
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// monitor commands
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleKill(Kill kill) {
		this.peerRunner.die();
		return new Null(ReturnCode.OK);
	}

	@Override
	protected MaintenanceResponse handleTerm(Term term) {
		this.peerRunner.shutdown();
		return new Null(ReturnCode.OK);
	}

	@Override
	protected MaintenanceResponse handleSetProperties(
			SetProperties setProperties) {
		return super.handleSetProperties(setProperties);
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// unknown messages
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleMGetAllPeers(MGetAllPeers mGetAllPeers) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleMGetAllPeersForStream(
			MGetAllPeersForStream mGetAllPeersForStream) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleMGetStreams(MGetStreams mGetStreams) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleSAlive(SAlive sAlive) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleGetPeerList(GetPeerList getPeerList) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleGetStreams(GetStreams getStreams) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleUnregisterStream(
			UnregisterStream unregisterStream) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleRegisterStream(
			RegisterStream registerStream) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleAlive(Alive alive) {
		return this.createUnknownMessageResponse();
	}
}
