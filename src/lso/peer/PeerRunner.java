package lso.peer;

import java.io.IOException;
import java.util.Random;

import lso.Address;
import lso.Child;
import lso.Common;
import lso.Location;
import lso.MaintenanceNode;
import lso.Option;
import lso.OptionParser;
import lso.Parent;
import lso.Stream;
import lso.net.message.NotFoundException;
import lso.net.message.UnknownMessageException;
import lso.net.message.maintenance.Connect;
import lso.net.message.maintenance.GetPeerList;
import lso.net.message.maintenance.GetStreams;
import lso.net.message.maintenance.Leave;
import lso.net.message.maintenance.LeaveStripe;
import lso.net.message.maintenance.MaintenanceMessage;
import lso.net.message.maintenance.MaintenanceMessageHandler;
import lso.net.response.ReturnCode;
import lso.net.response.maintenance.MaintenanceResponse;
import lso.net.response.maintenance.PeerList;
import lso.net.response.maintenance.Streams;
import lso.source.KeepAlive;
import lso.source.Source.Mode;
import lso.streaming.Streamer;
import de.tudarmstadt.kom.betterproxy.StreamEngine;

public class PeerRunner implements Runnable {
	public static PeerRunner peerRunner = null;

	/**
	 * STARTING A PEER INSTANCE
	 */
	public static PeerRunner startPeer(Address trackerMaintenanceAddress,
			Address maintenanceAddress, Address dataAddress, Stream stream,
			String name, int bandwidth, Location location, Mode mode,
			StreamEngine engine) throws IOException, IllegalArgumentException,
			UnknownMessageException, NotFoundException {
		peerRunner = new PeerRunner(maintenanceAddress, dataAddress, stream,
				name, bandwidth, location, mode, engine);
		peerRunner.startMaintenanceNode();

		/**
		 * retrieving list of streams from tracker
		 */
		MaintenanceResponse getStreamsResponse = peerRunner.maintenanceNode
				.send(trackerMaintenanceAddress, new GetStreams());
		if (getStreamsResponse.getReturnCode() == ReturnCode.OK) {
			Streams gs = (Streams) getStreamsResponse;
			for (Stream s : gs.getStreams()) {
				if (s.getIdentifier().equals(stream.getIdentifier())) {
					stream.setStripes(s.getStripes());
					stream.setVideoRate(s.getVideoRate());
					stream.setSource(s.getSource());
					stream.setStreamDescriptor(s.getStreamDescriptor());
					break;
				}
			}
		}

		// exit if specified stream unknown to tracker
		if (stream == null || stream.getStripes() == -1) {
			System.err.println("stream " + stream.getIdentifier()
					+ " unknown to tracker");
			System.exit(-1);
		}

		peerRunner.peer.setStripes(stream.getStripes());

		/**
		 * retrieving peer list for stream
		 */
		MaintenanceResponse getPeerListResponse = peerRunner.maintenanceNode
				.send(trackerMaintenanceAddress,
						new GetPeerList(stream.getIdentifier(),
								maintenanceAddress, location
										.getStringRepresentation()));

		// exit if no peer list could be retrieved
		if (getPeerListResponse.getReturnCode() != ReturnCode.OK) {
			System.err.println("cannot retrieve peer list for stream "
					+ stream.getIdentifier());
			System.exit(-1);
		}

		/**
		 * joining the overlay
		 */
		PeerList gpl = (PeerList) getPeerListResponse;

		// sending join request for each stripe
		for (int i = 0; i < peerRunner.peer.getStream().getStripes(); i++) {
			int index = peerRunner.peer.getRandom().nextInt(
					gpl.getPeers().length);
			Address peer = gpl.getPeers()[index];

			peerRunner.peer.setParent(i, peer);
			Connect connect = new Connect(i, maintenanceAddress, dataAddress);
			MaintenanceResponse connectResponse = peerRunner.maintenanceNode
					.send(peer, connect);
			if (connectResponse.getReturnCode() != ReturnCode.OK) {
				peerRunner.peer.removeParent(i);
				System.err.println("cannot join stripe " + i);
			}
		}

		/**
		 * start threads
		 */
		peerRunner.keepAliveThread.start();
		peerRunner.topologyCheckThread.start();

		return peerRunner;

	}

	/**
	 * SHUTTING DOWN A PEER INSTANCE
	 */
	public void shutdown() {
		MaintenanceMessage leave = new Leave(this.peer.getMaintenanceAddress());
		try {
			// send leave to tracker
			Address tracker = this.peer.getStream().getTracker();
			PeerRunner.this.maintenanceNode.send(tracker, leave);

			// forward children to parent
			for (int stripe = 0; stripe < this.peer.getStream().getStripes(); stripe++) {
				Parent parent = this.peer.getParent(stripe);
				Child[] children = this.peer.getChildren(stripe);
				for (Child child : children) {
					Address ca = child.getChildAddress().getMaintenance();
					MaintenanceMessage ls = new LeaveStripe(stripe,
							this.peer.getMaintenanceAddress(),
							parent.getAddress());
					PeerRunner.this.maintenanceNode.send(ca, ls);
				}
			}

			// send leave to parents
			for (Parent parent : PeerRunner.this.peer.getAvailableParents()) {
				Address addr = parent.getAddress();
				this.maintenanceNode.send(addr, leave);
			}

			// send leave to children
			// for (Child child : PeerRunner.this.peer.getChildren()) {
			// Address addr = child.getChildAddress().getMaintenance();
			// this.maintenanceNode.send(addr, leave);
			// }

			// stop threads
			this.keepAliveThread.interrupt();
			this.keepAliveThread = null;

			this.topologyCheckThread.interrupt();
			this.topologyCheckThread = null;

			this.maintenanceNode.quit();
			this.maintenanceNodeThread.interrupt();
			this.maintenanceNodeThread = null;

			this.maintenanceNode = null;

			if (this.peer.getStreamer() != null
					&& this.peer.getStreamer().getStreamEngine() != null) {
				this.peer.getStreamer().getStreamEngine().stop();
			}

			System.exit(0);

		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnknownMessageException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * KILLING A PEER INSTANCE
	 */
	public void die() {
		MaintenanceMessage leave = new Leave(this.peer.getMaintenanceAddress());
		try {
			// send leave to tracker
			Address tracker = this.peer.getStream().getTracker();
			PeerRunner.this.maintenanceNode.send(tracker, leave);

			// stop threads
			this.keepAliveThread.interrupt();
			this.keepAliveThread = null;

			this.topologyCheckThread.interrupt();
			this.topologyCheckThread = null;

			this.maintenanceNode.quit();
			this.maintenanceNodeThread.interrupt();
			this.maintenanceNodeThread = null;

			this.maintenanceNode = null;

			this.peer.getStreamer().getStreamEngine().stop();

			System.exit(0);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Peer peer;

	private MaintenanceNode maintenanceNode;

	private Thread maintenanceNodeThread;

	private Thread keepAliveThread;

	private Thread topologyCheckThread;

	public PeerRunner(Address maintenanceAddress, Address dataAddress,
			Stream stream, String name, int bandwidth, Location location,
			Mode mode, StreamEngine engine) throws IOException {
		// TODO instantiate Streamer correctly @ PeerRunner
		Streamer streamer = new Streamer(engine);
		this.peer = new Peer(maintenanceAddress, dataAddress, stream, name,
				false, bandwidth, location, mode, streamer);

		this.maintenanceNode = new MaintenanceNode(maintenanceAddress);
		MaintenanceMessageHandler mmh = new PeerMaintenanceMessageHandler(
				this.peer, this, this.maintenanceNode);
		this.maintenanceNode.setMessageHandler(mmh);

		this.maintenanceNodeThread = new Thread(this.maintenanceNode);

		this.keepAliveThread = new KeepAlive(this.maintenanceNode, this.peer,
				Common.PEER_KEEP_ALIVE_WAIT);

		this.topologyCheckThread = new PeerTopologyCheck(this.peer,
				this.maintenanceNode, Common.PEER_TOPOLOGY_CHECK_WAIT);

		PeerShutdownHook prsh = new PeerShutdownHook(this);
		Runtime.getRuntime().addShutdownHook(prsh);
	}

	public MaintenanceNode getMaintenanceNode() {
		return this.maintenanceNode;
	}

	@Override
	public void run() {
		this.startMaintenanceNode();
	}

	private void startMaintenanceNode() {
		this.maintenanceNodeThread.start();
	}

	public static void main(String[] args) {
		/**
		 * define parameters
		 */
		Option bindOption = new Option("b", "bind", "Address to bind to", true,
				false);
		Option dataOption = new Option("d", "data", "Address for data traffic",
				true, false);
		Option trackerOption = new Option("t", "tracker", "Tracker's address",
				true, false);
		Option streamOption = new Option("s", "stream",
				"Name of the target stream", true, false);
		Option bandwidthOption = new Option("w", "bandwidth",
				"Available bandwidth.", true, false);
		Option nameOption = new Option("n", "name", "Name of the peer", false,
				false);
		Option accessNetworkOption = new Option("a", "accessNetwork",
				"Access Network", false, false);
		Option modeOption = new Option("m", "mode", "Mode to run the Peer in",
				true, false);

		Option[] options = { bindOption, dataOption, trackerOption,
				streamOption, bandwidthOption, nameOption, accessNetworkOption,
				modeOption };

		/**
		 * create option parser
		 */
		OptionParser optionParser = new OptionParser(options, "peer.jar "
				+ "--bind hostname:port " + "--data hostname:port "
				+ "--tracker hostname:port " + "--stream stream"
				+ "--bandwidth bandwidth " + "[--name name] "
				+ "[--accessNetwork accessNetwork] " + "--mode mode ");
		try {
			optionParser.parse(args);
		} catch (IllegalArgumentException e) {
			System.err.println(e.getMessage());
			System.exit(-1);
		}

		/**
		 * get parameters from option parser
		 */
		Option bindArgument = optionParser.getByLongOption("bind");
		Address address = bindArgument.asAddress(-1);

		Option dataArgument = optionParser.getByLongOption("data");
		Address dataAddress = dataArgument.asAddress(-1);

		Option trackerArgument = optionParser.getByLongOption("tracker");
		Address tracker = trackerArgument.asAddress(-1);

		Option streamArgument = optionParser.getByLongOption("stream");
		Stream stream = new Stream(streamArgument.getValue(), -1, -1, "none",
				tracker, null);

		Option bandwidthArgument = optionParser.getByLongOption("bandwidth");
		int bandwidth = Integer.parseInt(bandwidthArgument.getValue());

		Option nameArgument = optionParser.getByLongOption("name");
		String name = "Peer (" + address.toString() + ")";
		if ((nameArgument != null) && nameArgument.getValue() != null) {
			name = nameArgument.getValue();
		}

		Option accessNetworkArgument = optionParser
				.getByLongOption("accessNetwork");
		Location location = Location.getDefaultLocation();
		if (Common.LOCATION_CHOOSE_RANDOM_AT_STARTUP) {
			location = Location.getRandomLocation(new Random());
		}
		if ((accessNetworkArgument != null)
				&& accessNetworkArgument.getValue() != null) {
			location = new Location(accessNetworkArgument.getValue());
		}

		Option modeArgument = optionParser.getByLongOption("mode");
		Mode mode = Mode.valueOf(modeArgument.getValue());

		/**
		 * Create the StreamEngine
		 */

		int deviceType = StreamEngine.DEVICE_PC;
		int clientType = StreamEngine.PEER;
		int streamSource = StreamEngine.STREAM_SOURCE_FILE;
		int camera = -1;
		String filePath = "";
		int stripes = -1;
		// TODO get additional TCP port!!!!!!!!
		int playerProxyRtspPort = address.getPort() + 500;
		int serverProxyFirstRtpPort = -1;
		int clientProxyFirstRtpPort = dataAddress.getPort();

		StreamEngine engine = new StreamEngine(deviceType, clientType,
				streamSource, camera, filePath, stripes, playerProxyRtspPort,
				serverProxyFirstRtpPort, clientProxyFirstRtpPort);

		/**
		 * start peer
		 */
		try {

			PeerRunner runner = PeerRunner.startPeer(tracker, address,
					dataAddress, stream, name, bandwidth, location, mode,
					engine);

			engine.setStreamDescription(runner.peer.getStream()
					.getStreamDescriptor());
			engine.setStripes(runner.peer.getStream().getStripes());

			if (runner.peer.getStream().getStreamDescriptor()
					.equals(Common.STREAM_DESCRIPTOR_DEFAULT)) {
				System.out.println("not starting stream engine");
				runner.peer.setStreamer(null);
			} else {
				System.out.println("starting stream engine");
				engine.start();
			}

		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (UnknownMessageException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
	}
}
