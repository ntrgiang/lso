package lso.peer;

public class PeerShutdownHook extends Thread {
	private PeerRunner peerRunner;

	public PeerShutdownHook(PeerRunner peerRunner) {
		this.peerRunner = peerRunner;
	}

	@Override
	public void run() {
		System.err.println("shutdown peer");
		this.peerRunner.shutdown();
	}
}
