package lso.log;

/**
 * This is a Logger implementation that uses Log4j as its backend.
 * 
 * @author Zoran Zaric <zz@zoranzaric.de>
 */
public class Log4jLogger extends Logger {

	private org.apache.log4j.Logger logger;

	public static Logger getRootLogger() {
		Log4jLogger l = new Log4jLogger();
		l.logger = org.apache.log4j.Logger.getRootLogger();
		return l;
	}

	public static Logger getLogger(String name) {
		Log4jLogger l = new Log4jLogger();
		l.logger = org.apache.log4j.Logger.getLogger(name);
		return l;
	}

	public void setLevel(Level level) {
		this.logger.setLevel(this.convertLevel(level));
	}

	public void trace(Object message) {
		this.logger.trace(message);
	}

	public void debug(Object message) {
		this.logger.debug(message);
	}

	public void info(Object message) {
		this.logger.info(message);
	}

	public void warn(Object message) {
		this.logger.warn(message);
	}

	public void error(Object message) {
		this.logger.error(message);
	}

	public void fatal(Object message) {
		this.logger.fatal(message);
	}

	public void log(Level l, Object message) {

		this.logger.log(this.convertLevel(l), message);
	}

	private org.apache.log4j.Level convertLevel(Level l) {
		org.apache.log4j.Level level = org.apache.log4j.Level.TRACE;
		switch (l) {
		case TRACE:
			level = org.apache.log4j.Level.TRACE;
			break;
		case DEBUG:
			level = org.apache.log4j.Level.DEBUG;
			break;
		case INFO:
			level = org.apache.log4j.Level.INFO;
			break;
		case WARN:
			level = org.apache.log4j.Level.WARN;
			break;
		case ERROR:
			level = org.apache.log4j.Level.ERROR;
			break;
		case FATAL:
			level = org.apache.log4j.Level.FATAL;
			break;
		}

		return level;
	}
}
