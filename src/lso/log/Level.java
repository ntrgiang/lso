package lso.log;

public enum Level {
	TRACE, DEBUG, INFO, WARN, ERROR, FATAL
}
