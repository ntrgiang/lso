package lso.log;

public abstract class Logger {
	public static Logger getRootLogger() {
		return null;
	}

	public static Logger getLogger(String name) {
		return null;
	}

	public void setLevel(Level level) {
	}

	public void trace(Object message) {
	}

	public void debug(Object message) {
	}

	public void info(Object message) {
	}

	public void warn(Object message) {
	}

	public void error(Object message) {
	}

	public void fatal(Object message) {
	}

	public void log(Level l, Object message) {
	}
}
