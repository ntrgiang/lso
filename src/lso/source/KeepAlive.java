package lso.source;

import java.io.IOException;

import lso.Address;
import lso.MaintenanceNode;
import lso.net.message.NotFoundException;
import lso.net.message.UnknownMessageException;
import lso.net.message.maintenance.Alive;

public class KeepAlive extends Thread {
	private MaintenanceNode maintenanceNode;

	private Source source;

	private long wait;

	public KeepAlive(MaintenanceNode maintenanceNode, Source source, long wait) {
		this.maintenanceNode = maintenanceNode;
		this.source = source;
		this.wait = wait;
	}

	@Override
	public void run() {
		try {
			while (true) {
				try {
					Address t = this.source.getStream().getTracker();
					Alive alive = new Alive(this.source.getMaintenanceAddress());
					this.maintenanceNode.send(t, alive);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (UnknownMessageException e) {
					e.printStackTrace();
				} catch (NotFoundException e) {
					e.printStackTrace();
				}
				Thread.sleep(this.wait);
			}
		} catch (InterruptedException e) {
			System.out.println("terminating KeepAlive");
		}
	}
}
