package lso.source;

public class SourceShutdownHook extends Thread {
	private SourceRunner sourceRunner;

	public SourceShutdownHook(SourceRunner sourceRunner) {
		this.sourceRunner = sourceRunner;
	}

	@Override
	public void run() {
		System.err.println("shutdown source");
		this.sourceRunner.shutdown();
	}
}
