package lso.source;

import java.util.ArrayList;

import lso.Address;
import lso.Child;
import lso.ChildAddress;
import lso.MaintenanceNode;
import lso.Parent;
import lso.net.message.maintenance.Alive;
import lso.net.message.maintenance.ChangeLocation;
import lso.net.message.maintenance.ChangeParent;
import lso.net.message.maintenance.Connect;
import lso.net.message.maintenance.ForwardChild;
import lso.net.message.maintenance.GetChildren;
import lso.net.message.maintenance.GetInfo;
import lso.net.message.maintenance.GetParents;
import lso.net.message.maintenance.GetPeerList;
import lso.net.message.maintenance.GetStreams;
import lso.net.message.maintenance.Kill;
import lso.net.message.maintenance.Leave;
import lso.net.message.maintenance.LeaveStripe;
import lso.net.message.maintenance.MGetAllPeers;
import lso.net.message.maintenance.MGetAllPeersForStream;
import lso.net.message.maintenance.MGetStreams;
import lso.net.message.maintenance.MaintenanceMessageHandler;
import lso.net.message.maintenance.PAlive;
import lso.net.message.maintenance.RegisterStream;
import lso.net.message.maintenance.RequestChild;
import lso.net.message.maintenance.SAlive;
import lso.net.message.maintenance.SetProperties;
import lso.net.message.maintenance.Term;
import lso.net.message.maintenance.UnregisterStream;
import lso.net.message.maintenance.WhichConnections;
import lso.net.response.ReturnCode;
import lso.net.response.maintenance.Children;
import lso.net.response.maintenance.Connections;
import lso.net.response.maintenance.Info;
import lso.net.response.maintenance.MaintenanceResponse;
import lso.net.response.maintenance.Null;

public class SourceMaintenanceMessageHandler extends MaintenanceMessageHandler {
	private Source source;

	private SourceRunner sourceRunner;

	protected MaintenanceNode maintenanceNode;

	public SourceMaintenanceMessageHandler(Source source,
			SourceRunner sourceRunner, MaintenanceNode maintenanceNode) {
		this.source = source;
		this.sourceRunner = sourceRunner;
		this.maintenanceNode = maintenanceNode;
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// peer
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleSAlive(SAlive sAlive) {
		return new Null(ReturnCode.OK);
	}

	@Override
	protected MaintenanceResponse handlePAlive(PAlive pAlive) {
		return new Null(ReturnCode.OK);
	}

	@Override
	protected MaintenanceResponse handleWhichConnections(
			WhichConnections whichConnections) {
		Address addr = whichConnections.getMaintenanceAddress();

		ArrayList<Child> list = new ArrayList<Child>();
		for (Child child : this.source.getChildren()) {
			if (addr.equals(child.getChildAddress().getMaintenance())) {
				list.add(child);
			}
		}
		Child[] child = new Child[list.size()];
		for (int i = 0; i < list.size(); i++) {
			child[i] = list.get(i);
		}

		Parent[] parent = new Parent[0];

		return new Connections(ReturnCode.OK, parent, child,
				this.source.getHeights());
	}

	@Override
	protected MaintenanceResponse handleLeave(Leave leave) {
		Address maintenanceAddress = leave.getMaintenanceAddress();
		System.out.println("LEAVE from " + maintenanceAddress);

		/**
		 * remove child from all stripes
		 */
		this.source.removeChild(maintenanceAddress);

		return new Null(ReturnCode.OK);
	}

	@Override
	protected MaintenanceResponse handleLeaveStripe(LeaveStripe leaveStripe) {
		Address ma = leaveStripe.getMaintenanceAddress();
		int stripe = leaveStripe.getStripe();

		/**
		 * remove peer if it is child in the stripe
		 */
		this.source.removeChildInStripe(stripe, ma);

		return new Null(ReturnCode.OK);
	}

	@Override
	protected MaintenanceResponse handleConnect(Connect connect) {
		int stripe = connect.getStripeIdentifier();
		Address ma = connect.getMaintenanceAddress();
		Address da = connect.getDataAddress();

		System.out.println("Connect from '" + ma + "' for stripe " + stripe);

		/**
		 * connect from itself
		 */
		if (ma.equals(this.source.getMaintenanceAddress())) {
			System.out.println("  => from itself");
			return new Null(ReturnCode.CANNOT_CONNECT);
		}

		/**
		 * already child in this stripe
		 */
		Child child = new Child(stripe, new ChildAddress(ma, da));
		if (this.source.hasChild(child)) {
			System.out.println("  => already child in this stripe");
			return new Null(ReturnCode.OK);
		}

		/**
		 * accept request
		 */
		this.source.addChild(stripe, new ChildAddress(ma, da));

		return new Null(ReturnCode.OK);
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// monitor info
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleGetChildren(GetChildren getChildren) {
		return new Children(ReturnCode.OK, this.source.getChildren());
	}

	@Override
	protected MaintenanceResponse handleGetInfo(GetInfo getInfo) {
		return new Info(ReturnCode.OK, this.source.getProperties());
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// monitor commands
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleKill(Kill kill) {
		this.sourceRunner.shutdown();
		// TODO kill source without notifying children & tracker
		return new Null(ReturnCode.OK);
	}

	@Override
	protected MaintenanceResponse handleTerm(Term term) {
		this.sourceRunner.shutdown();
		return new Null(ReturnCode.OK);
	}

	@Override
	protected MaintenanceResponse handleSetProperties(
			SetProperties setProperties) {
		if (this.source.setProperties(setProperties.getProperties())) {
			return new Null(ReturnCode.OK);
		} else {
			return new Null(ReturnCode.NOT_FOUND);
		}
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// unknown messages
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	@Override
	protected MaintenanceResponse handleAlive(Alive alive) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleChangeParent(ChangeParent changeParent) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleForwardChild(ForwardChild forwardChild) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleGetParents(GetParents getParents) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleGetPeerList(GetPeerList getPeerList) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleGetStreams(GetStreams getStreams) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleRegisterStream(
			RegisterStream registerStream) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleUnregisterStream(
			UnregisterStream unregisterStream) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleMGetAllPeers(MGetAllPeers mGetAllPeers) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleMGetAllPeersForStream(
			MGetAllPeersForStream mGetAllPeersForStream) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleMGetStreams(MGetStreams mGetStreams) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleRequestChild(RequestChild requestChild) {
		return this.createUnknownMessageResponse();
	}

	@Override
	protected MaintenanceResponse handleChangeLocation(
			ChangeLocation changeAccessNetwork) {
		return this.createUnknownMessageResponse();
	}
}
