package lso.source;

import java.io.IOException;

import lso.Address;
import lso.Common;
import lso.MaintenanceNode;
import lso.Option;
import lso.OptionParser;
import lso.Stream;
import lso.net.message.NotFoundException;
import lso.net.message.UnknownMessageException;
import lso.net.message.maintenance.MaintenanceMessageHandler;
import lso.net.message.maintenance.RegisterStream;
import lso.net.message.maintenance.UnregisterStream;
import lso.source.Source.Mode;
import lso.streaming.Streamer;
import de.tudarmstadt.kom.betterproxy.StreamEngine;

public class SourceRunner implements Runnable {
	/**
	 * STARTING A SOURCE INSTANCE
	 */
	public static SourceRunner startSource(Address trackerMaintenanceAddress,
			Address maintenanceAddress, Address dataAddress, Stream stream,
			String name, int uploadBandwidth, Mode mode, StreamEngine engine)
			throws IOException, IllegalArgumentException,
			UnknownMessageException, NotFoundException {

		System.out.println("STARTING SOURCE '" + stream.getIdentifier() + "' ("
				+ stream.getStripes() + ")");

		SourceRunner sourceRunner = new SourceRunner(maintenanceAddress,
				dataAddress, stream, name, uploadBandwidth, mode, engine);

		// register stream at tracker
		RegisterStream registerStream = new RegisterStream(sourceRunner.source
				.getStream().getIdentifier(), sourceRunner.source.getStream()
				.getStripes(), sourceRunner.source.getStream().getVideoRate(),
				sourceRunner.source.getStream().getStreamDescriptor(),
				sourceRunner.source.getMaintenanceAddress());
		Address tracker = sourceRunner.source.getStream().getTracker();
		sourceRunner.maintenanceNode.send(tracker, registerStream);

		// start threads
		sourceRunner.startMaintenanceNode();
		sourceRunner.keepAliveThread.start();
		sourceRunner.topologyCheckThread.start();

		return sourceRunner;
	}

	public Source source;

	private MaintenanceNode maintenanceNode;
	private Thread maintenanceNodeThread;

	private Thread keepAliveThread;
	private Thread topologyCheckThread;

	public SourceRunner(Address maintenanceAddress, Address dataAddress,
			Stream stream, String name, int uploadBandwidth, Mode mode,
			StreamEngine engine) throws IOException {
		Streamer streamer = new Streamer(engine);
		this.source = new Source(maintenanceAddress, dataAddress, stream, name,
				uploadBandwidth, mode, streamer);
		this.maintenanceNode = new MaintenanceNode(maintenanceAddress);
		MaintenanceMessageHandler mmh = new SourceMaintenanceMessageHandler(
				this.source, this, this.maintenanceNode);
		this.maintenanceNode.setMessageHandler(mmh);

		this.maintenanceNodeThread = new Thread(this.maintenanceNode);

		this.keepAliveThread = new KeepAlive(maintenanceNode, source,
				Common.SOURCE_KEEP_ALIVE_WAIT);
		this.topologyCheckThread = new SourceTopologyCheck(this.source,
				this.maintenanceNode, Common.SOURCE_TOPOLOGY_CHECK_WAIT);

		SourceShutdownHook srsh = new SourceShutdownHook(this);
		Runtime.getRuntime().addShutdownHook(srsh);
	}

	public void shutdown() {
		UnregisterStream urspbm = new UnregisterStream(SourceRunner.this.source
				.getStream().getIdentifier(),
				SourceRunner.this.source.getMaintenanceAddress());
		System.err.println("unregister source at tracker");
		try {
			SourceRunner.this.maintenanceNode.send(SourceRunner.this.source
					.getStream().getTrackerMaintenanceAddress(), urspbm);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (UnknownMessageException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		this.startMaintenanceNode();
	}

	private void startMaintenanceNode() {
		this.maintenanceNodeThread.start();
	}

	public static void main(String[] args) {
		/**
		 * define parameters
		 */
		Option bindOption = new Option("b", "bind", "Address to bind to", true,
				false);
		Option dataOption = new Option("d", "data", "Address for data traffic",
				true, false);
		Option trackerOption = new Option("t", "tracker", "Tracker's address",
				true, false);
		Option streamOption = new Option("s", "stream", "Name of the stream",
				true, false);
		Option stripeOption = new Option("x", "stripes", "Number of stripes",
				true, false);
		Option bandwidthOption = new Option("w", "bandwidth",
				"Available bandwidth.", true, false);
		Option videoRateOption = new Option("v", "videoRate",
				"Video Rate (kb/s)", true, false);
		Option nameOption = new Option("n", "name", "Name of the source",
				false, false);
		Option modeOption = new Option("m", "mode", "Mode to run the Peer in",
				true, false);

		Option[] options = { bindOption, dataOption, trackerOption,
				streamOption, stripeOption, bandwidthOption, videoRateOption,
				nameOption, modeOption };

		/**
		 * create option parser
		 */
		OptionParser optionParser = new OptionParser(options, "source.jar "
				+ "--bind hostname:port " + "--tracker hostname:port "
				+ "--stream stream " + "--stripes stripes "
				+ "--videoRate videoRate " + "--bandwidth bandwidth "
				+ "[--name name] " + "--mode mode ");
		try {
			optionParser.parse(args);
		} catch (IllegalArgumentException e) {
			System.err.println(e.getMessage());
			System.exit(-1);
		}

		/**
		 * get parameters from option parser
		 */
		Option bindArgument = optionParser.getByLongOption("bind");
		Address address = bindArgument.asAddress(-1);

		Option dataArgument = optionParser.getByLongOption("data");
		Address dataAddress = dataArgument.asAddress(-1);

		Option trackerArgument = optionParser.getByLongOption("tracker");
		Address tracker = trackerArgument.asAddress(-1);

		Option stripeArgument = optionParser.getByLongOption("stripes");
		int stripes = Integer.parseInt(stripeArgument.getValue());

		// TODO parse videoRate / get if from source file
		Option videoRateArgument = optionParser.getByLongOption("videoRate");
		int videoRate = Integer.parseInt(videoRateArgument.getValue());

		// TODO parse streamDescriptor / get if from the video file
		Option streamArgument = optionParser.getByLongOption("stream");
		Stream stream = new Stream(streamArgument.getValue(), stripes,
				videoRate, Common.STREAM_DESCRIPTOR_DEFAULT, tracker, address);

		Option bandwidthArgument = optionParser.getByLongOption("bandwidth");
		int uploadBandwidth = Integer.parseInt(bandwidthArgument.getValue());

		Option nameArgument = optionParser.getByLongOption("name");
		String name = "Source (" + stream.getIdentifier() + ") ("
				+ address.toString() + ")";
		if ((nameArgument != null) && nameArgument.getValue() != null) {
			name = nameArgument.getValue();
		}

		Option modeArgument = optionParser.getByLongOption("mode");
		Mode mode = Mode.valueOf(modeArgument.getValue());

		/**
		 * start source
		 */
		try {
			SourceRunner.startSource(tracker, address, dataAddress, stream,
					name, uploadBandwidth, mode, null);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (UnknownMessageException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
	}
}
