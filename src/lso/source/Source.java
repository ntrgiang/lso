package lso.source;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import lso.Address;
import lso.Child;
import lso.ChildAddress;
import lso.Stream;
import lso.streaming.Streamer;

public class Source {
	public static enum Mode {
		OverlayOnly, Headless, Full
	};

	protected Address maintenanceAddress;
	protected Address dataAddress;
	protected Stream stream;
	protected String name;

	protected int uploadBandwidth;

	protected Set<Child>[] children;

	protected Random random;

	protected int[] heights;

	protected Streamer streamer;

	protected Mode mode;

	protected boolean childrenChanged;

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// CONSTRUCTOR
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public Source(Address maintenanceAddress, Address dataAddress,
			Stream stream, String name, int uploadBandwidth, Mode mode,
			Streamer streamer) {
		this.maintenanceAddress = maintenanceAddress;
		this.dataAddress = dataAddress;
		this.stream = stream;
		this.name = name;
		this.uploadBandwidth = uploadBandwidth;

		this.random = new Random();

		this.mode = mode;

		this.streamer = streamer;

		this.childrenChanged = false;

		this.init();
	}

	@SuppressWarnings("unchecked")
	protected void init() {
		if (this.stream.getStripes() < 0) {
			this.children = new Set[0];
			return;
		}
		this.children = new Set[this.stream.getStripes()];
		for (int i = 0; i < this.children.length; i++) {
			this.children[i] = new HashSet<Child>();
		}
		this.heights = new int[this.stream.getStripes()];
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// GETTER / SETTER
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public Map<String, String> getProperties() {
		Map<String, String> properties = this.getCommonProperties();

		properties.put("TYPE", "SOURCE");
		properties.put("STRIPES", "" + this.stream.getStripes());
		properties.put("VIDEO_RATE", this.stream.getVideoRate() + " kb/s ("
				+ this.stream.getVideoRatePerStripe() + " kb/s)");

		return properties;
	}

	protected Map<String, String> getCommonProperties() {
		Map<String, String> properties = new HashMap<String, String>();
		String c = "";
		for (Set<Child> children : this.children) {
			for (Child child : children) {
				c += child.getChildAddress().getMaintenance().getPort() + " ";
			}
		}
		properties.put("NAME", this.getName());
		properties.put("MAINTENANCE_ADDRESS", this.getMaintenanceAddress()
				.toString());
		properties.put("UPLOAD_BANDWIDTH",
				"" + (int) Math.floor(this.getUploadBandwidth()));
		properties.put("BANDWIDTH_USED",
				"" + (int) Math.floor(this.getBandwidthUsed()));
		properties.put("BANDWIDTH_USAGE",
				"" + (int) Math.floor(this.getBandwidthUsed()) + " / "
						+ (int) this.getUploadBandwidth());
		properties.put("CHILDREN", c);
		properties.put("HOSTNAME", this.maintenanceAddress.getHostname());
		properties.put("MAINTENANCE_PORT", this.maintenanceAddress.getPort()
				+ "");

		// properties.put("STREAM_DESCRIPTOR",
		// this.stream.getStreamDescriptor());
		return properties;
	}

	public boolean setProperties(Map<String, String> properties) {
		if (!properties.containsKey("UPLOAD_BANDWIDTH")) {
			return false;
		}

		try {
			this.setUploadBandwidth(Integer.parseInt(properties
					.get("UPLOAD_BANDWIDTH")));
		} catch (NumberFormatException e) {
			this.setUploadBandwidth((int) Double.parseDouble(properties
					.get("UPLOAD_BANDWIDTH")));
		}

		return true;
	}

	public Address getMaintenanceAddress() {
		return this.maintenanceAddress;
	}

	public Address getDataAddress() {
		return this.dataAddress;
	}

	public int getRtspPort() {
		return this.dataAddress.getPort();
	}

	public int getRtpPort1() {
		return this.dataAddress.getPort() + 1;
	}

	public int getRtpPort2() {
		return this.dataAddress.getPort() + 2;
	}

	public int getRtpPort3() {
		return this.dataAddress.getPort() + 3;
	}

	public int getRtpPort4() {
		return this.dataAddress.getPort() + 4;
	}

	public Stream getStream() {
		return this.stream;
	}

	public String getName() {
		return this.name;
	}

	public double getBandwidthUsed() {
		return (double) this.getChildrenCount()
				* (double) this.stream.getVideoRate()
				/ (double) this.stream.getStripes();
	}

	public double getUploadBandwidth() {
		return uploadBandwidth;
	}

	public void setUploadBandwidth(int uploadBandwidth) {
		this.uploadBandwidth = uploadBandwidth;
	}

	public int getMaxNumberOfChildren() {
		return (int) Math.floor((double) this.uploadBandwidth
				/ ((double) this.stream.getVideoRate() / (double) this.stream
						.getStripes()));
	}

	public int getMaxNumberOfChildrenPerStripe() {
		return (int) Math.floor((double) this.uploadBandwidth
				/ (double) this.stream.getVideoRate());
	}

	public Random getRandom() {
		return this.random;
	}

	public void setStripes(int stripes) {
		this.stream.setStripes(stripes);
		this.init();
	}

	public Streamer getStreamer() {
		return this.streamer;
	}

	public void setStreamer(Streamer streamer) {
		this.streamer = streamer;
	}

	public Mode getMode() {
		return this.mode;
	}

	// ///////////////////////////////////////
	// ///////////////////////////////////////
	// CHILDREN
	// ///////////////////////////////////////
	// ///////////////////////////////////////

	public boolean addChild(int stripe, ChildAddress childAddress) {
		Child child = new Child(stripe, childAddress);
		this.children[stripe].add(child);
		this.childrenChanged = true;
		return true;
	}

	public void removeChild(Address ma) {
		for (int i = 0; i < this.children.length; i++) {
			this.removeChildInStripe(i, ma);
		}
		this.childrenChanged = true;
	}

	public void removeChildInStripe(int stripe, Address ma) {
		Child child = null;
		for (Child c : this.children[stripe]) {
			if (ma.equals(c.getChildAddress().getMaintenance())) {
				child = c;
				break;
			}
		}
		if (child != null) {
			this.children[stripe].remove(child);
			System.out.println("removing child '" + ma + "' in stripe "
					+ stripe);
		}
		this.childrenChanged = true;
	}

	public void removeChildInStripe(Child child) {
		this.children[child.getStripe()].remove(child);
		this.childrenChanged = true;
	}

	public int getChildrenCount() {
		int sum = 0;
		for (Set<Child> s : this.children) {
			sum += s.size();
		}
		return sum;
	}

	public int getChildrenCount(int stripe) {
		return this.children[stripe].size();
	}

	public Child[] getChildren() {
		Child[] c = new Child[this.getChildrenCount()];
		int index = 0;
		for (Set<Child> children : this.children) {
			for (Child child : children) {
				c[index++] = child;
			}
		}
		return c;
	}

	public int[] getHeights() {
		return this.heights;
	}

	public int getHeight(int stripe) {
		return this.heights[stripe];
	}

	public void setHeight(int stripe, int height) {
		this.heights[stripe] = height;
	}

	public Child getRandomChild(int stripe) {
		if (this.children[stripe].size() == 0) {
			return null;
		}
		int index = this.random.nextInt(this.children[stripe].size());
		int i = 0;
		for (Child child : this.children[stripe]) {
			if (i++ == index) {
				return child;
			}
		}
		return null;
	}

	public Child[] getChildren(int stripe) {
		Child[] children = new Child[this.children[stripe].size()];
		int index = 0;
		for (Child child : this.children[stripe]) {
			children[index++] = child;
		}
		return children;
	}

	public Child[] getTwoRandomChildren(int stripe) {
		if (this.children[stripe].size() < 2) {
			return null;
		}
		Child[] children = this.getChildren(stripe);
		Child[] c = new Child[2];
		c[0] = children[this.random.nextInt(children.length)];
		c[1] = children[this.random.nextInt(children.length)];
		while (c[1].equals(c[0])) {
			c[1] = children[this.random.nextInt(children.length)];
		}
		return c;
	}

	public boolean hasChild(Child child) {
		return this.children[child.getStripe()].contains(child);
	}

	public boolean hasChild(int stripe, Address ma) {
		for (Child child : this.children[stripe]) {
			if (ma.equals(child.getChildAddress().getMaintenance())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return the childrenChanged
	 */
	public boolean isChildrenChanged() {
		return childrenChanged;
	}

	/**
	 * @param childrenChanged
	 *            the childrenChanged to set
	 */
	public void setChildrenChanged(boolean childrenChanged) {
		this.childrenChanged = childrenChanged;
	}
}
