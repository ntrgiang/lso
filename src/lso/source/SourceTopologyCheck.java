package lso.source;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import lso.Address;
import lso.Child;
import lso.ChildAddress;
import lso.Common;
import lso.MaintenanceNode;
import lso.Parent;
import lso.net.message.NotFoundException;
import lso.net.message.UnknownMessageException;
import lso.net.message.maintenance.ChangeParent;
import lso.net.message.maintenance.ForwardChild;
import lso.net.message.maintenance.MaintenanceMessage;
import lso.net.message.maintenance.PAlive;
import lso.net.message.maintenance.RequestChild;
import lso.net.message.maintenance.WhichConnections;
import lso.net.response.ReturnCode;
import lso.net.response.maintenance.Connections;
import lso.net.response.maintenance.MaintenanceResponse;
import lso.source.Source.Mode;

public class SourceTopologyCheck extends Thread {
	private Source source;

	protected MaintenanceNode maintenanceNode;

	protected long wait;

	protected int turn;

	public SourceTopologyCheck(Source source, MaintenanceNode maintenanceNode,
			long wait) {
		this.source = source;
		this.maintenanceNode = maintenanceNode;
		this.wait = wait;
	}

	protected boolean isTurn(int mod) {
		return mod > 0 && (this.turn % mod) == 0;
	}

	@Override
	public void run() {
		this.turn = 0;

		while (true) {

			try {
				Thread.sleep(this.wait
						+ this.source.getRandom().nextInt((int) this.wait));

				if (this.isTurn(Common.TURN_CHILDREN_ALIVE)) {
					this.checkChildrenAlive();
				}
				if (this.isTurn(Common.TURN_CHILDREN_CONNECTIONS)) {
					this.checkChildrenConnections();
				}

				if (this.isTurn(Common.TURN_BANDWIDTH_FREE)) {
					this.checkFreeBandwidth();
				}
				if (this.isTurn(Common.TURN_BANDWIDTH_INSUFFICIENT)) {
					this.checkInsufficientBandwidth();
				}
				if (this.isTurn(Common.TURN_UPDATE_STREAMER)) {
					this.updateStreamer();
				}

			} catch (InterruptedException e) {
				break;
			} catch (Exception e) {
				System.out.println("Caught exception in SourceTopologyCheck: "
						+ e.getClass());
				e.printStackTrace();
			}

			turn++;
		}
	}

	protected boolean updateStreamer() {
		if (this.source.getStreamer() == null
				|| this.source.getStreamer().getStreamEngine() == null) {
			return false;
		}
		if (!this.source.isChildrenChanged()) {
			return true;
		}
		boolean success = true;
		for (int stripe = 0; stripe < this.source.getStream().getStripes(); stripe++) {
			success &= this.source.getStreamer().updateStripe(stripe,
					this.source.getChildren(stripe));
		}
		this.source.setChildrenChanged(false);
		return success;
	}

	/**
	 * (#children) messages <br>
	 * Checks if all children are still online. If a peers does not answer it is
	 * assumed to have gone offline without a notification and is removed from
	 * the child list.
	 * 
	 * @return true, if all children are online; false otherwise
	 */
	protected boolean checkChildrenAlive() {
		boolean success = true;
		for (Child child : this.source.getChildren()) {
			Address address = child.getChildAddress().getMaintenance();
			if (!this.isAlive(address)) {
				System.out.println("child is not alive: " + child);
				this.source.removeChild(address);
				success = false;
			}
		}
		return success;
	}

	/**
	 * (#children) messages <br>
	 * Checks if all children have this peer as a parent in the respective
	 * stream. If a peer that is assumed as a child does not have this peer
	 * listed as a peer in the stripe, the child is removed from the respective
	 * stripe's child list.
	 * 
	 * @return true, if all children know their correct parent; false otherwise
	 */
	protected boolean checkChildrenConnections() {
		boolean success = true;
		for (Child child : this.source.getChildren()) {
			if (!this.hasPeerAsParent(child)) {
				System.out
						.println("child does not have me as parent: " + child);
				this.source.removeChildInStripe(child);
				success = false;
			}
		}
		return success;
	}

	/**
	 * (#freeSlots) messages <br>
	 * Checks for each stripe if this source has additional bandwidth left over
	 * to serve another child. If this is the case, another child for the
	 * respective stripe is requested (moved up) from a child in the respective
	 * stripe.
	 * 
	 * @return true, if no additional bandwidth is available or a new child
	 *         could be found in each stripe with free bandwidth; false
	 *         otherwise
	 */
	private boolean checkFreeBandwidth() {
		boolean success = true;
		for (int stripe = 0; stripe < this.source.getStream().getStripes(); stripe++) {
			success &= this.checkFreeBandwidth(stripe);
		}
		return success;
	}

	/**
	 * (1) message [in case of free bandwidth only] <br>
	 * Checks if this source has additional bandwidth left over to serve another
	 * child in this stripe. If this is the case, another child is requested
	 * (moved up) from a child in this stripe.
	 * 
	 * @param stripe
	 *            stripe to check free bandwidth for
	 * @return true, if no additional bandwidth is available or a new child for
	 *         the stripe could be found; false otherwise
	 */
	private boolean checkFreeBandwidth(int stripe) {

		int available = this.source.getMaxNumberOfChildrenPerStripe();
		int used = this.source.getChildrenCount(stripe);

		while (used < available) {

			Child[] children = this.source.getChildren(stripe);
			Child newChild = this.requestNewChild(children, stripe);
			if (newChild == null) {
				return false;
			}
			if (!this.connectToNewChild(newChild, stripe, used, available)) {
				return false;
			}

			if (!Common.pullUpMultiple) {
				break;
			}

			used = this.source.getChildrenCount(stripe);
		}

		return true;
	}

	/**
	 * (#stripesWithTooManyChildren) messages <br>
	 * Checks if this source has too many children in any stripe.
	 * 
	 * @return true, if there is sufficient bandwidth or a child could be
	 *         forwarded (moved down); false otherwise
	 */
	private boolean checkInsufficientBandwidth() {
		boolean success = true;
		for (int stripe = 0; stripe < this.source.getStream().getStripes(); stripe++) {
			success &= this.checkInsufficientBandwidth(stripe);
		}
		return success;
	}

	/**
	 * (1) message [in case of insufficient bandwidth only] <br>
	 * Checks if this source has too many children to serve in the given stripe.
	 * If this is the case, two random children in the stripe are selected and
	 * one is forwarded to the other.
	 * 
	 * @param stripe
	 *            stripe in which to check
	 * @return true if there is sufficient bandwidth or a child could be
	 *         forwarded (moved down); false otherwise
	 */
	private boolean checkInsufficientBandwidth(int stripe) {

		int available = this.source.getMaxNumberOfChildrenPerStripe();
		int used = this.source.getChildrenCount(stripe);

		while (used > available) {

			Child[] children = this.source.getChildren(stripe);
			Child[] c = this.source.getTwoRandomChildren(stripe);

			if (c == null || c.length != 2) {
				return false;
			}

			if (!this.forwardChild(c, stripe, used, available)) {
				return false;
			}

			if (!Common.pushDownMultiple) {
				break;
			}

			used = this.source.getChildrenCount(stripe);
		}

		return true;

		// Child[] children = this.source.getChildren(stripe);
		//
		// int used = children.length;
		// // int available = this.source.getBandwidth();
		// int available = this.source.getMaxNumberOfChildrenPerStripe();
		//
		// if (used <= available) {
		// return true;
		// }
		//
		// Child[] c = this.source.getTwoRandomChildren(stripe);
		// if (c == null || c.length != 2) {
		// return false;
		// }
		//
		// return this.forwardChild(c, stripe, used, available);
	}

	/**
	 * (1) message <br>
	 * Sends a PAlive message to the peer with the given address to check if it
	 * is still alive or has gone offline without notifying this peer.
	 * 
	 * @param address
	 *            peer's address to check
	 * @return true, if the peer replies with ReturnCode.OK; false otherwise
	 */
	protected boolean isAlive(Address address) {
		MaintenanceMessage msg = new PAlive(this.source.getMaintenanceAddress());
		try {
			MaintenanceResponse r = this.maintenanceNode.send(address, msg);
			if (r != null && r.getReturnCode() == ReturnCode.OK) {
				return true;
			}
		} catch (IllegalArgumentException e) {
		} catch (IOException e) {
		} catch (UnknownMessageException e) {
		} catch (NotFoundException e) {
		}
		return false;
	}

	/**
	 * (1) message <br>
	 * Checks if the given child has this peer listed as a parent in the
	 * respective stripe.
	 * 
	 * @param child
	 *            child to check
	 * @return true, if the child has this peer listed as parent inthe same
	 *         stripe; false otherwise
	 */
	protected boolean hasPeerAsParent(Child child) {
		Address addr = child.getChildAddress().getMaintenance();
		MaintenanceMessage msg = new WhichConnections(
				this.source.getMaintenanceAddress());
		try {
			MaintenanceResponse r = this.maintenanceNode.send(addr, msg);
			Connections c = (Connections) r;
			for (Parent p : c.getParent()) {
				if (p.getStripe() == child.getStripe()
						&& p.getAddress().equals(
								this.source.getMaintenanceAddress())) {
					return true;
				}
			}
			return false;
		} catch (IllegalArgumentException e) {
		} catch (IOException e) {
		} catch (UnknownMessageException e) {
		} catch (NotFoundException e) {
		}
		return false;
	}

	/**
	 * max: (#children) messages <br>
	 * Requests a new child for a stripe from the children (to move up the
	 * respective stripe's tree).
	 * 
	 * @param children
	 *            children in the stripe
	 * @param stripe
	 *            stripe to find a new child in
	 * @return new child received from one of the children; false if no
	 *         additional child is available
	 */
	protected Child requestNewChild(Child[] children, int stripe) {
		ArrayList<Child> c = new ArrayList<Child>(children.length);
		for (Child child : children) {
			c.add(child);
		}
		Collections.shuffle(c);

		for (Child child : c) {
			Address addr = child.getChildAddress().getMaintenance();
			MaintenanceMessage msg = new RequestChild(stripe);
			try {
				MaintenanceResponse r = this.maintenanceNode.send(addr, msg);
				if (r != null
						&& r instanceof lso.net.response.maintenance.Child) {
					ChildAddress ca = ((lso.net.response.maintenance.Child) r)
							.getChildAddress();
					return new Child(stripe, ca);
				}
			} catch (IllegalArgumentException e) {
			} catch (IOException e) {
			} catch (UnknownMessageException e) {
			} catch (NotFoundException e) {
			}
		}
		return null;
	}

	/**
	 * (1) message <br>
	 * Sends a ChangeParent message to the given child.
	 * 
	 * @param newChild
	 *            child to connect to
	 * @param stripe
	 *            stripe to connect in
	 * @param used
	 *            currently used bandwidth
	 * @param available
	 *            currently available bandwidth
	 * @return true, if the connection could be established; false otherwise
	 */
	protected boolean connectToNewChild(Child newChild, int stripe, int used,
			int available) {
		Address addr = newChild.getChildAddress().getMaintenance();
		MaintenanceMessage msg = new ChangeParent(newChild.getStripe(),
				this.source.getMaintenanceAddress());
		try {
			MaintenanceResponse r = this.maintenanceNode.send(addr, msg);
			if (r != null && r.getReturnCode() == ReturnCode.OK) {
				System.out.println("connected to new child '" + addr
						+ "' in stripe " + stripe + " (" + used + "/"
						+ available + ")");
				this.source.addChild(newChild.getStripe(),
						newChild.getChildAddress());
				return true;
			}
		} catch (IllegalArgumentException e) {
		} catch (IOException e) {
		} catch (UnknownMessageException e) {
		} catch (NotFoundException e) {
		}
		return false;
	}

	/**
	 * (2) messages <br>
	 * Forwards child c[0] to child c[1] in stripe $stripe to move it down the
	 * tree and removes child c[0] from the list of children in stripe $stripe
	 * 
	 * @param c
	 * @param stripe
	 * @param used
	 * @param available
	 * @return true if the forwarding was successful; false otherwise
	 */
	protected boolean forwardChild(Child[] c, int stripe, int used,
			int available) {
		Address forwardM = c[0].getChildAddress().getMaintenance();
		Address forwardD = c[0].getChildAddress().getData();
		Address forwardTo = c[1].getChildAddress().getMaintenance();
		ForwardChild forwardChildMessage = new ForwardChild(stripe, forwardM,
				forwardD);
		try {
			MaintenanceResponse r = this.maintenanceNode.send(forwardTo,
					forwardChildMessage);
			if (r != null && r.getReturnCode() == ReturnCode.OK) {
				System.out.println("forwarding child: " + forwardM + " to "
						+ forwardTo + " because of insufficient bandwidth ("
						+ used + "/" + available + ")");
				this.source.removeChildInStripe(c[0]);

				MaintenanceMessage cp = new ChangeParent(stripe, forwardTo);
				this.maintenanceNode.send(forwardM, cp);

				return true;
			}
		} catch (IllegalArgumentException e) {
		} catch (IOException e) {
		} catch (UnknownMessageException e) {
		} catch (NotFoundException e) {
		}
		return false;
	}
}
