package lso.streaming;

import java.net.InetSocketAddress;
import java.util.HashSet;
import java.util.Set;

import lso.Address;
import lso.Child;
import de.tudarmstadt.kom.betterproxy.StreamEngine;

public class Streamer {

	protected StreamEngine engine;

	public Streamer(StreamEngine engine) {
		this.engine = engine;
	}

	public boolean updateStripe(int stripe, Child[] children) {
		Set<InetSocketAddress> addresses = new HashSet<InetSocketAddress>();
		for (Child child : children) {
			Address da = child.getChildAddress().getData();
			String h = da.getHostname();
			int p = da.getPort();
			InetSocketAddress addr = new InetSocketAddress(h, p);
			addresses.add(addr);
		}

		this.engine.updateClients(stripe, addresses);

		return true;
	}

	public StreamEngine getStreamEngine() {
		return this.engine;
	}

}