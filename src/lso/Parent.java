package lso;

public class Parent {
	private int stripe;
	private Address address;

	public int getStripe() {
		return stripe;
	}

	public Address getAddress() {
		return this.address;
	}

	public Parent(int stripe, Address address) {
		this.stripe = stripe;
		this.address = address;
	}

	public String toString() {
		return "Parent(" + this.stripe + ") " + this.address;
	}
}
