#!/bin/bash

source addresses.ini

stream='6.Six.Stripes'
stripe='6'
video_rate='100'
bandwidth='200'
peer_bandwidth='200'

cd ../start/
./source.sh $source_host $source_port_maintenance $source_port_data $tracker $stream $stripe $video_rate $bandwidth 4 Full &

for i in {0..3}; do
	sleep 1
	./peer.sh $peer_host $peer_port_maintenance $peer_port_data $tracker $stream $peer_bandwidth 4 'Full' &
done
