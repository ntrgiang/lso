#!/bin/bash

if [ "$#" -gt "0" ]; then
	peers=$1
else
	peers='1'
fi


source addresses.ini

stream='For.The.Birds'
peer_bandwidth='200'

echo "starting peer for stream '${stream}'"

cd ../start/
for i in $(seq 1 ${peers}); do
	./peer.sh $peer_host $peer_port_maintenance $peer_port_data $tracker $stream $peer_bandwidth 4 'Full' &
done
