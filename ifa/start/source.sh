#!/bin/bash
# 1 = host
# 2 = port_maintenance
# 3 = port_data
# 4 = tracker address
# 5 = stream name
# 6 = stripes
# 7 = video rate
# 8 = bandwidth
# 9 = number of data ports
# 10 = mode

if [ ! -e "ports" ]; then
	mkdir ports
fi

if [ ! -e "log" ]; then
	mkdir log
fi

host=$1
tracker=$4
stream=$5
stripes=$6
video_rate=$7
bandwidth=$8
data_ports=$9
mode=${10}

### check for a free TCP port
port_maintenance=$2
while [ -e ports/tcp-${port_maintenance} ]; do
	port_maintenance=$(expr $port_maintenance + 1)
done

### reserve TCP port
touch ports/tcp-${port_maintenance}


### check for first free UDP port
port_data_start=$3
while [ -e ports/udp-${port_data_start} ]; do
	port_data_start=$(expr $port_data_start + 1)
done

### reserve UDP ports
for index in $(seq 0 $(expr $data_ports - 1)); do
	port_data=$(expr $port_data_start + $index)
	touch ports/udp-${port_data}
done


### build addresses
address_maintenance="${host}:${port_maintenance}"
address_data="${host}:${port_data}"
name="${stream}-src-${port_maintenance}"

### start source
java -jar ../../build/source.jar \
			-b $address_maintenance -d $address_data -t $tracker \
			-s $stream -x $stripes -v $video_rate \
			-w $bandwidth -n $name -m $mode


### remove lock on TCP port
#rm ports/tcp-${port_maintenance}

### remove lock on UDP ports
#for index in $(seq 0 $(expr $data_ports - 1)); do
#	port_data=$(expr $port_data_start + $index)
#	rm ports/udp-${port_data}
#done
