#!/bin/bash
# 1 = monitor address
# 2 = tracker address
# 3 = default peer bandwidth

java -jar ../../build/monitor.jar -b $1 -t $2 -p $3 -r 1000
