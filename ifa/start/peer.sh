#!/bin/bash
# 1 = hostname
# 2 = port maintenance
# 3 = first data port
# 4 = tracker address
# 5 = stream name
# 6 = upload bandwidth
# 7 = number of data ports
# 8 = mode

if [ ! -e "ports" ]; then
	mkdir ports
fi

if [ ! -e "log" ]; then
	mkdir log
fi

host=$1
tracker=$4
stream=$5
bandwidth=$6
data_ports=$7
mode=$8


### check for a free TCP port
port_maintenance=$2
while [ -e ports/tcp-${port_maintenance} ]; do
	port_maintenance=$(expr $port_maintenance + 1)
done

### reserve TCP port
touch ports/tcp-${port_maintenance}


### check for first free UDP port
port_data_start=$3
while [ -e ports/udp-${port_data_start} ]; do
	port_data_start=$(expr $port_data_start + 1)
done

### reserve UDP ports
for index in $(seq 0 $(expr $data_ports - 1)); do
	port_data=$(expr $port_data_start + $index)
	touch ports/udp-${port_data}
done


### build addresses
address_maintenance="${host}:${port_maintenance}"
address_data="${host}:${port_data_start}"
name="${stream}-peer-${port_maintenance}"

### start peer
java -jar ../../build/peer.jar \
			-b $address_maintenance -d $address_data -t $tracker \
			-s $stream -w $bandwidth -n $name -m $mode


### remove lock on TCP port
#rm ports/tcp-${port_maintenance}

### remove lock on UDP ports
#for index in $(seq 0 $(expr $data_ports - 1)); do
#	port_data=$(expr $port_data_start + $index)
#	rm ports/udp-${port_data}
#done
