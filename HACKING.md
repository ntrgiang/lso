A Guide to get LSO and add Functionality to LSO
===============================================

Package Layout
--------------

There's a package for every entity (peer, monitor, source, tracker).  These
packages contain a class that holds the state and one that implements a runner.

The `lso.net` package contains everything network related.  It is split in
packages for messages and responses.  All maintenance messages and responses are
in `los.net.message.maintenance` and `lso.net.response.maintenance`.


Message Flow
------------

Every entity that receives messages has a MaintenanceNode.  A MaintenanceNode
has a MaintenanceMessageSender and a MaintenanceMessageReceiver.  The Sender and
Receiver handle the wire-coding using factories.  The
ProtocolBufferMaintenanceMessageSender uses the ProtocolBufferMessageFactory and
the MaintenanceResponseFactory, the ProtocolBufferMaintenanceMessageReceiver the
MaintenanceMessageFactory and the ProtocolBufferResponseFactory.

Every Entity that receives messages needs a MaintenanceMessageHandler, that
implements the behaviour of message handling.

A message flow can look like this:

  1. PeerRunner sends Alive to Tracker
  2. PeerRunner uses the ProtocolBufferMaintenanceMessageSender in it's
     MaintenanceNode
  3. The ProtocolBufferMaintenanceMessageSender changes the Alive Object to a
     ProtocolBuffer Alive message
  4. The ProtocolBufferMaintenanceMessageSender writes the serialized
     ProtocolBuffer message to the tracker's open socket
  5. The Tracker's ProtocolBufferMaintenanceMessageReceiver creates a Alive
     Object from the recieved ProtocolBuffer message
  6. The Tracker's MaintenanceMessageHandler reacts to the message an creates a
     Null Object
  7. The Tracker's ProtocolBufferMaintenanceMessageReceiver creates a Protocol
     Buffer Null Respose
  8. The ProtocolBufferMaintenanceMessageReceiver writes the serialized Protocol
     Buffer response to the socket
  9. The Peer's ProtocolBufferMaintenanceMessageSender creates a Null Response
     Object from the received Protocol Buffer Null response


Adding new Messages
-------------------

Implementing a new message involves seven steps:

  1. Add a message to res/protocolbuffers/messages.proto
  2. Build protocolbuffer messages "protoc --java_out=src -I res/protocolbuffers res/protocolbuffers/*.proto"
  3. Add message type to `lso.net.message.maintenance.Type`
  4. Define a class in `lso.net.message.maintenance` that extends `lso.net.message.maintenance.MaintenanceMessage`
  5. Add handling of message to `lso.net.message.maintenance.MaintenanceMessageHandler`
  6. Add generation of message to `lso.net.message.maintenance.MaintenanceMessageFactory`
  7. Add handling in each component's MaintenanceMessageHandler
  8. Add generation of ProtocolBufferMessage to `lso.net.message.ProtocolBufferMessageFactory`

DON'T FORGET THE 'break' STATEMENT!

Adding new Responses
-------------------

Implementing a new response involves five steps:

  1. Add a response to res/protocolbuffers/responses.proto
  2. Build protocolbuffer messages "protoc --java_out=src -I res/protocolbuffers res/protocolbuffers/*.proto"
  3. Add response type to `lso.net.response.maintenance.Type`
  4. Define a class in `lso.net.response.maintenance` that extends `lso.net.response.maintenance.MaintenanceMessage`
  5. Add generation of message to `lso.net.response.maintenance.MaintenanceResponseFactory`
  6. Add generation of message to `lso.net.response.ProtocolBufferResponseFactory`

DON'T FORGET THE 'break' STATEMENT!
