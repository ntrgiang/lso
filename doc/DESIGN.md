Live Streaming Overlay Design
=============================

This is a document that describes lso's design.  For more detail please

  a) consult the referenced documents
  b) read the inline code documentation


Entities
--------

lso consists of the following entities:

  - trackers
  - sources
  - peers
  - monitoring servers (to be renamed)


Communication
-------------
Currently we only focus on maintenance communication.  This includes maintenance
of the overlay structure, registering streams, etc.  Maintenance communication
is TCP based and does not include any payload data like RTP streams.


### Maintenance Communication Implementation
The maintenance communication is highly decoupled.  It is implemented in the
`lso.net` package.


#### Messages
[Protocol Buffers](http://code.google.com/apis/protocolbuffers) are used as the
wire protocol for maintenance communication.  The protocol buffer definitions
are located in `/res/protocolbuffers`.

Every Message is a abstract class and inherits from `MaintenanceMessage`.

For protocol buffer messages there's an interface `ProtocolBufferMessage` which
is implemented by the class (e.g. `AliveProtocolBufferMessage`) that is used for
communication, which extends the abstract message type class.


#### Responses
Responses follow the messages pattern.


#### Communication
Maintenance communication is encapsulated in a `MaintenanceNode` and should run
in its own thread.  A `MaintenanceNode` manages a `MaintenanceMessageSender` and
`MaintenanceMessageReceiver` -- which binds to a socket and handles messages
using a given `MaintenanceMessageHandler`.

The current implementation uses a `ProtocolBufferMaintenanceMessageReceiver` to
handle protocol buffer messages over TCP.  Switching to a custom serialization
format over UDP would only require a re-implementation of the
`MaintenanceMessageReceiver` and the message implementations.

